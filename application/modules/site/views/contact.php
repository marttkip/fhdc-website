<?php 
	if(count($contacts) > 0)
	{
		$email = $contacts['email'];
		$phone = $contacts['phone'];
		$facebook = $contacts['facebook'];
		$twitter = $contacts['twitter'];
		$linkedin = $contacts['linkedin'];
        $instagram = $contacts['instagram'];
		$logo = $contacts['logo'];
		$company_name = $contacts['company_name'];
		$address = $contacts['address'];
		$city = $contacts['city'];
		$post_code = $contacts['post_code'];
		$building = $contacts['building'];
		$floor = $contacts['floor'];
		$location = $contacts['location'];
		$working_weekend = $contacts['working_weekend'];
		$working_weekday = $contacts['working_weekday'];
		
		// if(!empty($email))
		// {
		// 	$mail = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$facebook = '<a href="'.$facebook.'" target="_blank">Facebook</a>';
		// }
		
		// if(!empty($twitter))
		// {
		// 	$twitter = '<a href="'.$twitter.'" target="_blank">Twitter</a>';
		// }
		
		// if(!empty($linkedin))
		// {
		// 	$linkedin = '<a href="'.$linkedin.'" target="_blank"></a>';
		// }
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
        $instagram = '';
		$logo = '';
		$company_name = '';
	}

   $branches_rs = $this->site_model->get_all_branches();

    $branches_list = '';
    $branch_dropdownList = '';
    if($branches_rs->num_rows() > 0)
    {
        foreach ($branches_rs->result() as $key => $value) {
            # code...
            $branch_name = $value->branch_name;
            $branch_email = $value->branch_email;
            $branch_phone = $value->branch_phone;
            $branch_address = $value->branch_address;
            // $branch_postal_code = $value->branch_postal_code;
            $branch_location = $value->branch_location;
            $branch_building = $value->branch_building;
            $branch_floor = $value->branch_floor;
            $location_link = $value->location_link;
            $branches_list .= '
                            <li style="margin-bottom:10px !important;">
                                <i class="fa fa-map-marker"></i> <b>'.$branch_name.'</b><br />
                                <p> '.$branch_building.' '.$branch_floor.' '.$branch_location.'</p>
                                <p> '.$branch_phone.'</p>
                            </li>';

            $branch_dropdownList .= '<option value="'.$branch_email.'">'.$branch_name.'</option>';
        }
    }

?>


<!-- Content -->
    
        <!--section-->
        <!-- <div class="section mt-0">
            
            <div class="contact-map" id="googleMapContact"></div> -->
            <!--  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.770230837087!2d36.81454321475408!3d-1.3133175990421269!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f1156882675e5%3A0x5506815c046c9add!2sFamily%20Health%20Dental%20Clinic!5e0!3m2!1sen!2ske!4v1600164010191!5m2!1sen!2ske" class="contact-map" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
        <!-- </div> -->
        <!--//section-->
        <!--section-->
        <!-- <div class="section mt-10 bg-grey">
            <div class="container">
                <div class="row">
                    <div class="col-md">
                        <div class="title-wrap">
                            <h5>Clinic Location</h5>
                            <div class="h-decor"></div>
                        </div>
                        <ul class="icn-list-lg">
                            <li><i class="fa fa-map"></i> <?php echo $location?>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md mt-3 mt-md-0">
                        <div class="title-wrap">
                            <h5>Contact Info</h5>
                            <div class="h-decor"></div>
                        </div>
                        <ul class="icn-list-lg">
                            <li><i class="fa fa-phone"></i>Phone: <span class="theme-color"><span class="text-nowrap"> <?php echo $phone;?></span> 
                                </span>
                                
                            </li>
                            <li><i class="fa fa-envelope"></i>Email: <a href="mailto:<?php echo $email?>"> <?php echo $email?></a></li>
                        </ul>
                    </div>
                    <div class="col-md mt-3 mt-md-0">
                        <div class="title-wrap">
                            <h5>Working Time</h5>
                            <div class="h-decor"></div>
                        </div>
                        <ul class="icn-list-lg">
                            <li><i class="fa fa-clock"></i>
                                <div>
                                    <div class="d-flex"><span>Mon-Fri</span><span class="theme-color"> <?php echo $working_weekday?></span></div>
                                    <div class="d-flex"><span>Weekends</span><span class="theme-color"> <?php echo $working_weekend?></span></div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div> -->
        <!--//section-->
<div class="page-content">
    <div class="section mt-0">
            <div class="breadcrumbs-wrap">
                <div class="container">
                    <div class="breadcrumbs">
                        <a href="<?php echo site_url().'home'?>">Home</a>
                        <span>Contact Us</span>
                    </div>
                </div>
            </div>
        </div>
        <!--section-->
        <div class="section " style="margin-top: 20px !important;margin-bottom: 20px;">
            <div class="container">
                <div class="row">
                    <div class="col-md col-lg-5">
                        <div class="pr-0 pr-lg-8">
                            <div class="title-wrap">
                                <h2>Get In Touch With Us</h2>
                                <div class="h-decor"></div>
                            </div>
                            <div class="mt-2 mt-lg-4">
                                <p>For general questions, please send us a message and we’ll get right back to you. You can also call us directly to speak with a member of our service team or insurance expert.</p>
                                <p class="p-sm">Fields marked with a * are required.</p>
                            </div>
                            <div class="mt-2 mt-md-5"></div>
                            <h5>Stay Connected</h5>
                            <div class="content-social mt-15">
                                <a href="<?php echo $facebook?>" target="blank" class="hovicon"><i class="fa fa-facebook"></i></a>
                                <a href="<?php echo $twitter?>" target="blank" class="hovicon"><i class="fa fa-twitter"></i></a>
                                <a href="" target="blank" class="hovicon"><i class="fa fa-google"></i></a>
                                <a href="<?php echo $instagram?>" target="blank" class="hovicon"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md col-lg-6 mt-4 mt-md-0">

                        <?php echo form_open($this->uri->uri_string(), array("class" => "contact-form","novalidate"=>"novalidate"));?>
                            <div class="successform">
                                <p>Your message was sent successfully!</p>
                            </div>
                            <div class="errorform">
                                <p>Something went wrong, try refreshing and submitting the form again.</p>
                            </div>
                            <div>
                                <input type="text" class="form-control" name="sender_name" placeholder="Your name*">
                            </div>
                            <div class="mt-15">
                                <input type="text" class="form-control" name="sender_email" placeholder="Email*">
                            </div>
                            <div class="mt-15">
                                <input type="text" class="form-control" name="contact_no" placeholder="Your Phone">
                            </div>
                            <div class="mt-15">
                                <input type="text" class="form-control" name="subject" placeholder="Subject*">
                            </div>
                            <div class="mt-15">
                                <!-- <?php echo $branch_dropdownList;  ?> -->
                                <div class="selectWrapper input-group mt-1">
                                <span>
                                <i class="fa fa-list"></i>
                            </span>
                                <select name="branch" class="form-control">
                                    <option selected="selected" disabled="disabled">Branch*</option>
                                    <?php echo $branch_dropdownList; ?>
                                </select>
                            </div>
                            </div>
                            <div class="mt-15">
                                <textarea class="form-control" name="message" placeholder="Message"></textarea>
                            </div>
                            <div class="mt-3">
                                <button type="submit" class="btn btn-hover-fill"><i class="fa fa-right-arrow"></i><span>Send message</span><i class="fa fa-right-arrow"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!--//section-->

<?php echo $this->load->view("site/our_partners", '');?>  
<?php //echo $this->load->view("site/tag_line", '');?>  
</div>
