<?php
    $company_details = $this->site_model->get_contacts();
    
    if(count($company_details) > 0)
    {
        $email = $company_details['email'];
        $email2 = $company_details['email'];
        $facebook = $company_details['facebook'];
        $twitter = $company_details['twitter'];
        $linkedin = '';// $company_details['linkedin'];
        $logo = $company_details['logo'];
        $company_name = $company_details['company_name'];
        $phone = $company_details['phone']; 
        $mission = $company_details['mission']; 
        $about = $company_details['about'];     
    
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
        $mission = '';
    }


?>


<?php
// var_dump($title);die();
  $about_query = $this->site_model->get_active_content_items($title,1);
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $gallery_rs = $this->site_model->get_post_gallery($post_id);

      $gallery = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
          $post_gallery_image_thumb = $value->post_gallery_image_thumb;

          $gallery .= ' <li>
                              <div class="portfolio-thumb">
                                  <div class="gc_filter_cont_overlay_wrapper port_uper_div">
                                      <img src="'.$post_gallery_image_name.'" class="zoom image img-responsive" alt="service_img" />
                                      <div class="gc_filter_cont_overlay zoom_popup">
                                          <div class="gc_filter_text"><a href="'.$post_gallery_image_name.'"><i class="fa fa-plus"></i></a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </li>';
        }
      }

    }
  }
   
 

  ?>

<?php echo $this->load->view("site/quick_links", '');?>  

<div class="page-content">
    <!--section-->
    <div class="section mt-0">
      <div class="breadcrumbs-wrap">
        <div class="container">
          <div class="breadcrumbs">
            <a href="<?php echo site_url().'home'?>">Home</a>
            <a href="<?php echo site_url().'services'?>">Services</a>
            <a href="<?php echo site_url().'services'?>"><?php echo $blog_category_name;?></a>
            <span><?php echo $post_title;?></span>
          </div>
        </div>
      </div>
    </div>
    <!--//section-->
    <!--section-->
    <div class="section page-content-first">
      <div class="container mt-6">
        <div class="row">
          
          <div class="col-md-8 col-lg-9 mt-4 mt-md-0">
            <div class="title-wrap">
              <h1><?php echo $post_title?></h1></div>
            <div class="service-img">
              <img src="<?php echo $image_about?>" class="img-fluid" alt="">
            </div>
            <div class="pt-2 pt-md-4">
             
              <?php echo $description;?>


            </div>
            
            <!-- </div> -->
          </div>
          <div class="col-md">


            <?php


            $blog_category_id = $this->site_model->get_category_id('Company Services');

            $category_items = $this->site_model->get_active_post_content_by_category($blog_category_id);

            $main_services = '';
            if($category_items->num_rows() > 0)
            {
                $x= 1;
                foreach ($category_items->result() as $key => $row) 
                {
                    $post_title_view = $row->post_title;
                    $post_id = $row->post_id;
                    $blog_category_id = $row->blog_category_id;
                    $image_about = base_url().'assets/images/posts/'.$row->post_image;
                    $blog_web_name = $this->site_model->create_web_name($post_title);
                    


                    // $blog_web_name = $this->site_model->create_web_name($blog_category_name);
                    $blog_category_id = $this->site_model->get_category_id($post_title_view);

                    $category_items = $this->site_model->get_active_post_content_by_category($blog_category_id);
                    $services_sub_menu_services = '';
                    $x++;
                    $item='';
                    if($x== 1)
                    {
                      $item = 'show';
                    }
                    // var_dump($category_items);die();
                     
                    if($category_items->num_rows() > 0)
                    {
                      $services_sub_menu_services .= '<div class="collapse '.$item.'" id="submenu'.$x.'">
                                                      <ul class="flex-column nav">';
                     
                      foreach ($category_items->result() as $key => $value) {
                        # code...

                        $post_title = $value->post_title;
                        $web_name = $this->site_model->create_web_name($post_title);

                        $services_sub_menu_services .= '<li class="nav-item"><a  class="nav-link" href="'.site_url().'view-service/'.$web_name.'">  '.$post_title.' </a></li>';
                      }
                     
                      // var_dump($services_sub_menu_services);die();
                       $services_sub_menu_services .= ' </ul>
                                                  </div>';
                    }
                    else
                    {
                      // put that category here
                    }

                   
                                                 // <h1><a href="'.site_url().'view-service-category/'.$blog_web_name.'"> Read More <i class="fa fa-long-arrow-right"></i></a></h1>

                     $main_services .= '
                                        <li class="nav-item">
                                          <a class="nav-link" href="#submenu'.$x.'" data-toggle="collapse" data-target="#submenu'.$x.'">'.$post_title_view.'</a>
                                           '.$services_sub_menu_services.'
                                        </li>
                                      ';

                  }
                }
                
              ?>
            <ul class="services-nav flex-column flex-nowrap">

              <?php echo $main_services;?>
              
            </ul>
            <div class="row d-flex flex-column flex-sm-row flex-md-column mt-3">
              <div class="col-auto col-sm col-md-auto">
                <div class="contact-box contact-box-1">
                  <h5 class="contact-box-title">Working Time</h5>
                  <ul class="icn-list">
                    <li><i class="icon-clock"></i>Mon-Sat <?php echo $weekday?>
                      <br>Sunday <?php echo $weekend?>
                      <br>Public Holidays Closed</li>
                  </ul>
                </div>
              </div>
              <div class="col-auto col-sm col-md-auto">
                <div class="contact-box contact-box-2">
                  <h5 class="contact-box-title">Contact Info</h5>
                  <ul class="icn-list">
                    <li><i class="icon-telephone"></i>
                      <div class="d-flex flex-wrap">
                        <span>Phone:&nbsp;&nbsp;</span>
                        <span><?php echo $phone?></div>
                    </li>
                    <li><i class="icon-black-envelope"></i><a href="mailto:<?php echo $email?>"><?php echo $email?></a></li>
                  </ul>
                </div>
              </div>
            </div>
            <!-- <div class="question-box mt-3">
              <h5 class="question-box-title">Ask the Experts</h5>
              <form id="questionForm" method="post" novalidate>
                <div class="successform">
                  <p>Your message was sent successfully!</p>
                </div>
                <div class="errorform">
                  <p>Something went wrong, try refreshing and submitting the form again.</p>
                </div>
                <input type="text" class="form-control" name="name" placeholder="Your name*">
                <input type="text" class="form-control" name="email" placeholder="E-mail*">
                <input type="text" class="form-control" name="phone" placeholder="Phone">
                <textarea class="form-control" name="message" placeholder="Question*"></textarea>
                <button type="submit" class="btn btn-sm btn-hover-fill mt-15"><i class="icon-right-arrow"></i><span>Ask Now</span><i class="icon-right-arrow"></i></button>
              </form>
            </div> -->
          </div>
        </div>
      </div>
    </div>
    <!--//section-->
  </div>
<!-- Content -->

      
                 
                  
  





    