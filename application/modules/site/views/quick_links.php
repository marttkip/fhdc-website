<?php
    $company_details = $this->site_model->get_contacts();
    
    if(count($company_details) > 0)
    {
        $email = $company_details['email'];
        $email2 = $company_details['email'];
        $facebook = $company_details['facebook'];
        $twitter = $company_details['twitter'];
        $linkedin = '';// $company_details['linkedin'];
        $logo = $company_details['logo'];
        $company_name = $company_details['company_name'];
        $phone = $company_details['phone']; 
        $mission = $company_details['mission']; 
        $about = $company_details['about'];  
        $working_weekday = $company_details['working_weekday'];  
        $working_weekend = $company_details['working_weekend'];     
    
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
        $mission = '';
        $working_weekend = '';
        $working_weekday = ''; 
    }

$branches_rs = $this->site_model->get_all_branches();
$branches_list = '';
if($branches_rs->num_rows() > 0)
{
    foreach ($branches_rs->result() as $key => $value) {
        # code...
        $branch_name = $value->branch_name;
        $branch_id = $value->branch_id;
        $branch_email = $value->branch_email;
        $branch_phone = $value->branch_phone;
        $branch_address = $value->branch_address;
        $branch_code = $value->branch_code;
        $branch_location = $value->branch_location;
        $branch_building = $value->branch_building;
        $branch_floor = $value->branch_floor;
        $location_link = $value->location_link;

        $testimonials_image_about = base_url().'assets/logo/'.$value->branch_image_name;
        $gallery_rs = $this->site_model->get_branch_gallery_list($branch_code);
         // var_dump($gallery_rs);die();
        $gallery = '';
        if($gallery_rs->num_rows() > 0)
        {
            foreach ($gallery_rs->result() as $key => $value) {
              # code...
              $gallery_image_name = base_url().'assets/gallery/'.$value->gallery_image_name;
              $gallery_image_thumb = $value->gallery_image_thumb;

              $gallery .= ' <div class="col"><span class="gallery-popover-link" data-full="'.$gallery_image_name.'"><img src="'.$gallery_image_name.'" alt="" class="img-fluid"></span></div>';


            }

             // var_dump($gallery_image_name);die();
        }

        $branches_list .='<h4 class="p-1 mx-auto">'.$branch_name.'</h4>
        <iframe src="'.$location_link.'" class="google-map" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe><hr>
        ';
            $branch_dropdownList .= '<option value="'.$branch_name.'">'.$branch_name.'</option>';
    }
}


?>
<div class="quickLinks-wrap js-quickLinks-wrap-d d-none d-lg-flex">
    <div class="quickLinks js-quickLinks">
        <div class="container">
            <div class="row no-gutters">
                <div class="col">
                    <a href="#" class="link">
                        <i class="fa fa-map-marker" style="font-size: 30px;"></i><span>Location</span></a>
                    <div class="link-drop p-0">
                        <!-- <div id="googleMapDrop" class="google-map"></div> -->
                        <div>
        <?php echo $branches_list;?>

                            <!--  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.770230837087!2d36.81454321475408!3d-1.3133175990421269!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f1156882675e5%3A0x5506815c046c9add!2sFamily%20Health%20Dental%20Clinic!5e0!3m2!1sen!2ske!4v1600164010191!5m2!1sen!2ske" class="google-map" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe> -->
                        </div>
                        <hr>
                        <!-- <div>
                             <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3988.770230837087!2d36.81454321475408!3d-1.3133175990421269!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x182f1156882675e5%3A0x5506815c046c9add!2sFamily%20Health%20Dental%20Clinic!5e0!3m2!1sen!2ske!4v1600164010191!5m2!1sen!2ske" class="google-map" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div> -->
                       
                    </div>
                </div>
                <div class="col">
                    <a href="#" class="link">
                        <i class="fa fa-clock-o" style="font-size: 30px;"></i><span> Working Time</span>
                    </a>
                    <div class="link-drop">
                        <h5 class="link-drop-title"><i class="fa fa-clock-o"></i> Working Time</h5>
                        <table class="row-table">
                            <tr>
                                <td><i>Mon-Saturday</i></td>
                                <td><?php echo $working_weekday?></td>
                            </tr>
                           
                            <tr>
                                <td><i>Sunday</i></td>
                                <td><?php echo $working_weekend?></td>
                            </tr>
                            <tr>
                                <td><i>Public Holidays</i></td>
                                <td>Closed</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- <div class="col">
                    <a href="#" class="link">
                        <i class="fa fa-calendar" style="font-size: 30px;"></i><span> Appointment</span>
                    </a>
                    <div class="link-drop">
                        <h5 class="link-drop-title"><i class="fa fa-calendar"></i> Appointment</h5>
                      
                        <?php echo form_open('quick-appointment', array("novalidate"=>"novalidate"));?>

                            <div class="successform">
                                <p>Your message was sent successfully!</p>
                            </div>
                            <div class="errorform">
                                <p>Something went wrong, try refreshing and submitting the form again.</p>
                            </div>
                            <div class="input-group">
                                <span>
                                <i class="fa fa-user"></i>
                            </span>
                                <input name="name" type="text" class="form-control" placeholder="Your Name" />
                            </div>
                            <div class="row row-sm-space mt-1">
                                <div class="col">
                                    <div class="input-group">
                                        <span>
                                        <i class="fa fa-envelope"></i>
                                    </span>
                                        <input name="email" type="text" class="form-control" placeholder="Your Email" />
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="input-group">
                                        <span>
                                        <i class="fa fa-phone"></i>
                                    </span>
                                        <input name="phone" type="text" class="form-control" placeholder="Your Phone" />
                                    </div>
                                </div>
                            </div>
                            <div class="selectWrapper input-group mt-1">
                                <span>
                                <i class="fa fa-list"></i>
                            </span>
                                <select name="branch" class="form-control">
                                    <option selected="selected" disabled="disabled">Select Branch</option>
                                    <?php echo $branch_dropdownList ;  ?>
                                </select>
                            </div>
                            <div class="row row-sm-space mt-1">
                                <div class="col-sm-6">
                                    <div class="input-group flex-nowrap">
                                        <span>
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        <div class="datepicker-wrap">
                                            <input name="date" type="text" class="form-control datetimepicker" placeholder="Date" readonly>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6 mt-1 mt-sm-0">
                                    <div class="input-group flex-nowrap">
                                        <span>
                                                <i class="icon-clock-o"></i>
                                        </span>
                                        <div class="datepicker-wrap">
                                            <input name="time" type="text" class="form-control timepicker" placeholder="Time" readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-2">
                                <button type="submit" class="btn btn-sm btn-hover-fill">Request</button>
                            </div>
                        </form>
                    </div>
                </div> -->
               <!--  <div class="col">
                    <a href="#" class="link">
                        <i class="fa fa-calendar" style="font-size: 30px;"></i><span>Doctors Timetable</span>
                    </a>
                    <div class="link-drop">
                        <h5 class="link-drop-title"><i class="icon-calendar"></i>Doctors Timetable</h5>
                        <p>This simply works as a guide and helps you to connect with dentists of your choice. Please confirm the doctor’s availability before leaving your premises.</p>
                        <p class="text-right"><a href="schedule.html" class="btn btn-sm btn-hover-fill">Details</a></p>
                    </div>
                </div>
                -->
                <div class="col">
                    <a href="#" class="link">
                        <i class="fa fa-ambulance" style="font-size: 30px;"></i><span> Emergency Case</span></a>
                    <div class="link-drop">
                        <h5 class="link-drop-title"><i class="fa fa-ambulance"></i> Emergency Case</h5>
                        <p>Emergency dental care may be needed if you have had a blow to the face, lost a filling, or cracked a tooth. </p>
                        <ul class="icn-list">
                            <li><i class="fa fa-phone"></i><span class="phone"> <?php echo $phone;?></span>
                            </li>
                            <li><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $email;?>"> <?php echo $email;?></a></li>
                        </ul>
                        <p class="text-right mt-2"><a href="<?php echo site_url().'contact-us'?>" class="btn btn-sm btn-hover-fill">Our Contacts</a></p>
                    </div>
                </div>
                <div class="col col-close"><a href="index.html#" class="js-quickLinks-close"><i class="fa fa-angle-up" data-toggle="tooltip" data-placement="top" title="Close panel"></i></a></div>
            </div>
        </div>
        <div class="quickLinks-open js-quickLinks-open"><span data-toggle="tooltip" data-placement="left" title="Open panel">+</span></div>
    </div>
</div>