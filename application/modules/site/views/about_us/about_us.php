<?php
$company_details = $company_details;

$mission = $company_details['mission'];
$vision = $company_details['vision'];
$core_values = $company_details['core_values'];



?>

<?php
  $about_query = $this->site_model->get_active_content_items('FHDC Info',1);
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $gallery_rs = $this->site_model->get_post_gallery($post_id);

      $gallery = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
          $post_gallery_image_thumb = $value->post_gallery_image_thumb;

          $gallery .= '<div class="col-6">
                          <img src="'.$post_gallery_image_name.'" class="w-100" alt="">
                        </div>';
        }
      }

    }
  }
  ?>


 
 <?php echo $this->load->view("site/quick_links", '');?>  


  <!--//quick links-->
  <div class="page-content">
    <!--section-->
    <div class="section mt-0">
      <div class="breadcrumbs-wrap">
        <div class="container">
          <div class="breadcrumbs">
            <a href="<?php echo site_url().'home'?>">Home</a>
            <span>About Us</span>
          </div>
        </div>
      </div>
    </div>
    <!--//section-->
    <!--section-->
    <div class="section page-content-first">
      <div class="container">
        <div class="text-center mb-2  mb-md-3 mb-lg-4">
          <!-- <div class="h-sub theme-color">Our dentists working to your smile</div> -->
          <h1>About Family Health Dental Clinic</h1>
          <p>Being a <strong>reliable and patient-centric dental clinic in Nairobi</strong>, we always ensure our patients have thye best treatment outcomes possible. </p>
          <div class="h-decor"></div>
        </div>
      </div>
      <div class="container">
        <div class="row">
          <div class="col-lg-6 text-center text-lg-left pr-md-4">
            <img src="<?php echo $image_about?>" class="w-100" alt="">
            <div class="row mt-3">
              <?php echo $gallery;?>
            </div>
          </div>
          <div class="col-lg-6 mt-3 mt-lg-0">
            <?php echo $description?>
            <div class="mt-3 mt-md-7"></div>
            <h2>Mission Statement</h2>
            <div class="mt-0 mt-md-4"></div>
            <?php echo $mission?>
            <div class="mt-0 mt-md-4"></div>
            <h2> Vision Statement</h2>
            <div class="mt-0 mt-md-4"></div>
            <?php echo $vision?>
            
             <div class="facilities">
                <h2>Our Facilities</h2>
                <img src="https://scontent.fnbo9-1.fna.fbcdn.net/v/t39.30808-6/271495937_513885113434495_5126665595317169740_n.jpg?stp=dst-jpg_p526x296&_nc_cat=104&ccb=1-5&_nc_sid=730e14&_nc_eui2=AeGHPlR_a0k4-87bk7MyjFhoNCGzJMh1Lw80IbMkyHUvD6EmCEs9VEli6YvZXKhkP2WjoNnai-qPe2p99EJHX9jn&_nc_ohc=SB_dXFhT524AX-i18UC&_nc_pt=5&_nc_zt=23&_nc_ht=scontent.fnbo9-1.fna&oh=00_AT-5OrfxLN4tpbxOAQb4W0aUvJy5APJky0UtTwjTcNPupg&oe=624C575B" alt="a Family Health Dental Clinic dentist viewing a patient's x-ray in the X-ray room" />
            
            <p style="margin-top:30px;">We have modern equipment that make it possible to achieve positive treatment outcomes which motivate our patients to refer more patients to us.</p>
            
            <p>Some examples of the instruments we use include:</p>
            
            <ol>
                <li><strong>Endodontic rotary instruments</strong> which are essential during endodontic procedures such as <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/Tooth-Fillings">root canal treatment</a>. These instruments are used to shape teeth during endodontic procedures.</li>
                <li><strong>Dental loupes</strong> which make it easy for our dentists to visualize what cannot be seen by the naked eye. Rest assured, we are not missing anything critical during a procedure.</li>
                <li><strong>Pluggers</strong> which are essential during a <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/Tooth-Fillings">filling procedure</a>. We use these to pack, condense and compact a filling material into a tooth cavity. </li>
                <li><strong>Burnishers</strong> which we use to contour and polish <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/Tooth-Fillings">amalgam fillings</a> and to polish <a href="https://familyhealthdentalclinic.com/Tooth-Fillings">composite fillings</a></li>
                <li><strong>Sickle probes</strong> which we use to locate cavities, pits and fissures on teeth, and to remove <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/blog/view-single/discolored-teeth">tartar</a>.</li>
            </ol>
            </div>
            
            <div style="margin-top:30px; padding-bottom: 20px;" class="experience">
                <h2>15+ Years of Dental Excellence</h2>
                <p>For over 15 years, <strong>we have served thousands of patients in Nairobi</strong> and achieved excellent treatment outcomes. That's the reason why we continue getting positive reviews.</p>
                <p>Take an example of Linda Jenna, a patient who visited our clinic for full mouth scaling. <strong>Here is a video showing the treatment outcome of Linda Jenna, one of our many happy patients</strong> </p>
                <iframe src="https://www.facebook.com/plugins/video.php?height=476&href=https%3A%2F%2Fweb.facebook.com%2FFamilyHealthDentalClinic%2Fvideos%2F353102646410991%2F&show_text=false&width=269&t=0" width="269" height="476" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true" title="A happy patient who just underwent a full mouth scaling procedure in Family Health Dental Clinic"></iframe>
                <p>Clearly, excellence is at the core of our practice as dental professionals and we keep seeking to improve; we never settle. You can trust that you are in good hands when you visit <strong>Family Health Dental Clinic</strong>.</p>
            </div>
            
            </div>
          </div>
          
        </div>
      </div>
    </div>
    <!--//section-->
    <!--section-->
    <?php
    $about_query = $this->site_model->get_active_content_items('Core Values',1);
     $about_company = '';
     $image_about_values  ='';
    if($about_query->num_rows() == 1)
    {
      $x=0;
      foreach($about_query->result() as $row)
      {
        $about_title = $row->post_title;
        $post_id = $row->post_id;
        $blog_category_name = $row->blog_category_name;
        $blog_category_id = $row->blog_category_id;
        $post_title = $row->post_title;
        $web_name = $this->site_model->create_web_name($post_title);
        $post_status = $row->post_status;
        $post_views = $row->post_views;
        $image_about_values = base_url().'assets/images/posts/'.$row->post_image;
        $created_by = $row->created_by;
        $modified_by = $row->modified_by;
        $post_target = $row->post_target;
        $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
        $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
        $description = $row->post_content;
        $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
        $created = $row->created;
        $day = date('j',strtotime($created));
        $month = date('M',strtotime($created));
        $year = date('Y',strtotime($created));
        $created_on = date('jS M Y',strtotime($row->created));
        $x++;
        if($x < 9)
        {
          $x = '0'.$x;
        }

       
      }
    }
    ?>
    <!--//section-->
    <!--section-->
    <div class="section bg-grey">
      <div class="container">
        <div class="title-wrap text-center">
          <h2 class="h1">Our Core Values</h2>
          <div class="h-decor"></div>
        </div>
        <div class="row">
          <div class="col-md">
              <div class="row">
                <div class="col-lg-3">
                </div>
                <div class="col-lg-6">
                  <img src="<?php echo $image_about_values?>" class="img-fluid">
                </div>
                <div class="col-lg-3">
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
    
  
    <!--//section-->
    <!--section-->
 
    <br></br>
      
    <!--section-->
    <!--section-->
    
    <?php //echo $this->load->view("site/tag_line", '');?> 
    <!--//section-->
    
    
    <div>
    <h2 style="text-align:center;">Testimonials</h2>
    <?php echo $this->load->view("site/testimonials", '');?> 
    </div>
         
  </div>
   <?php echo $this->load->view("site/our_partners", '');?> 
   
   <div style="margin-left:300px; margin-bottom:40px;" class="quick-oral-health-tips">
                <h2>Gum Disease Tips</h2>
                <img src="https://scontent.fnbo9-1.fna.fbcdn.net/v/t39.30808-6/274995810_548573646632308_696096433515640421_n.jpg?stp=dst-jpg_p526x296&_nc_cat=100&ccb=1-5&_nc_sid=730e14&_nc_eui2=AeH3HKEFidHh6OZQAWRCIpb7tF0x_ccZpfu0XTH9xxml-1qZd5d1yyla9HrpQhYzAQ1GWPLxATo9gpQxz8aTktuI&_nc_ohc=OK7Vhzh6_h4AX-KM7pg&tn=6kVcwiHq4S32d5fn&_nc_pt=5&_nc_zt=23&_nc_ht=scontent.fnbo9-1.fna&oh=00_AT_1vly7jwRrhLJTGcW9L_cj3LPlfrWNsr8Yg95Um_LJaA&oe=624BBABB" alt="a picture showing gum disease tips. People who visit this page will find these tips useful" />
            </div>

       <?php //echo $this->load->view("site/why_choose_us", '');?>  
    <!-- top service wrapper end-->
      <?php //echo $this->load->view("site/team_list", '');?>  

     <?php //echo $this->load->view("site/our_partners", '');?>  
    <!--news wrapper start-->
    

     <?php //echo $this->load->view("site/tag_line", '');?>  