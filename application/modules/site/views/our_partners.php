<?php
$about_query = $this->site_model->get_active_items('Our Partners');
 $our_partners = '';

 // var_dump($about_query);die();
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = strip_tags($row->post_content);
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $x++;
    if($x < 9)
    {
      $x = '0'.$x;
    }
    $our_partners .= '<div class="col-md-2">
                          <a href="#"><img src="'.$image_about.'" class="img-fluid" alt=""></a> 
                      </div>';
  }
}
?>

<div class="section bg-grey">
    <div class="container">
        <div class="title-wrap text-center">
            <h2 class="h1">Our Partners</h2>
            <p>One of the reasons why we are an <strong>affordable dental clinic in Nairobi</strong> is because we have a partnership with a great number of insurance companies, NHIF and payment providers who make it very easy for our patients to make payments in the most convenient way possible. </p>
            <div class="h-decor"></div>
        </div>
        <div class="row specialist-carousel js-specialist-carousel">
            <?php echo $our_partners?>
        </div>
    </div>
  </div>
  <br>