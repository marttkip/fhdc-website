<?php
$about_query = $this->site_model->get_active_items('faqs');
 $faqs_list = '';

 // var_dump($about_query);die();
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $x++;
    // if($x < 9)
    // {
    //   $x = '0'.$x;
    // }
    $faqs_list .= '
					<div class="faq-item">
                            <a data-toggle="collapse" data-parent="#faqAccordion1" href="#" aria-expanded="true"><span>'.$x.'.</span><span>'.$post_title.'</span></a>
                            <div id="faqItem1" class="collapse show faq-item-content" role="tabpanel">
                                <div>
                                   '.$description.'
                                </div>
                            </div>
                        </div>';
  }
}
?>

<div class="section bg-grey py-0">
    <div class="container-fluid px-0">
        <div class="row no-gutters">
            <div class="col-xl-6 banner-left bg-fullheight" style="background-image: url(<?php echo base_url().'assets/themes/dentco/'?>images/content/banner-left.jpg)"></div>
            <div class="col-xl-6">
                <div class="faq-wrap">
                    <div class="title-wrap">
                        <h2 class="h1">Patient <span class="theme-color">Information</span></h2>
                    </div>
                
                    <div id="tab-content" class="tab-content mt-sm-1">
                        <div id="tab-A" class="tab-pane fade show active" role="tabpanel">
                            <div id="faqAccordion1" class="faq-accordion" data-children=".faq-item">
                                <?php echo $faqs_list;?>
                              
                            </div>
                        </div>
                       
                    </div>
                    <a href="#" class="btn mt-15 mt-md-3" data-toggle="modal" data-target="#modalQuestionForm"> <span>Ask question</span> <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>