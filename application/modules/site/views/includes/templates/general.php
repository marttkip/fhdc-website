<!DOCTYPE html>
<html lang="en">
	<?php

    if(isset($post_target))
    {
        $contacts['post_target'] = $post_target;
    }
    else
    {
        $contacts['post_target'] = NULL;
    }
    ?>
	<?php echo $this->load->view('includes/header', $contacts, TRUE);?>
	
<body style="padding:0 10px; font-size:">
    <input type="hidden" name="config_url" id="config_url" value="<?php echo site_url()?>">
    <input type="hidden" name="base_url" id="base_url" value="<?php echo site_url()?>">
    <?php echo $this->load->view('includes/navigation', '', TRUE);?>


  
    <?php echo $content; ?>

	     

    <?php echo $this->load->view('includes/footer', '', TRUE);?>
    	
     <script type="text/javascript">
        
        $(document).on("submit","form#bookingFormId",function(e)
        {
            e.preventDefault();
            
            var form_data = new FormData(this);

            // alert(form_data);
            
            var config_url = $('#config_url').val();    

             var url = config_url+"site/book_appointment";
             // var values = $('#visit_type_id').val();
             alert(url);
               $.ajax({
               type:'POST',
               url: url,
               data:form_data,
               dataType: 'text',
               processData: false,
               contentType: false,
               success:function(data){
                  var data = jQuery.parseJSON(data);
                     // alert(data);
                  if(data.message == "success")
                    {
                        alert('You have successfully placed you appointment booking');
                        window.location.href = config_url+"home";
                        
                    }
                    else
                    {
                        alert('Please ensure you have added included all the items');
                    }
               
               },
               error: function(xhr, status, error) {
               alert("XMLHttpRequest=" + xhr.responseText + "\ntextStatus=" + status + "\nerrorThrown=" + error);
               
               }
               });
             
            
           
            
        });
    </script>
         <!--footer wrapper end-->
	    <!--main js files-->

	    <!-- JavaScript  files ========================================= -->
	<!--	 <script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/jquery/jquery-3.2.1.min.js"></script> -->
	<!--<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/jquery-migrate/jquery-migrate-3.0.1.min.js"></script>-->
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/cookie/jquery.cookie.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/bootstrap-datetimepicker/moment.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/popper/popper.min.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/bootstrap/bootstrap.min.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/waypoints/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/waypoints/sticky.min.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/slick/slick.min.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/scroll-with-ease/jquery.scroll-with-ease.min.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/countTo/jquery.countTo.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/form-validation/jquery.form.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>vendor/form-validation/jquery.validate.min.js"></script>
	<!-- Custom Scripts -->
	<script src="<?php echo base_url().'assets/themes/dentco/'?>js/app.js"></script>
    <script src="<?php echo base_url().'assets/themes/dentco/'?>color/color.js"></script>

	<script src="<?php echo base_url().'assets/themes/dentco/'?>js/app-shop.js"></script>
	<script src="<?php echo base_url().'assets/themes/dentco/'?>form/forms.js"></script>


		
      
    
	</body>
</html>