<?php
	$company_details = $this->site_model->get_contacts();
	
	if(count($company_details) > 0)
	{
		$email = $company_details['email'];
		$email2 = $company_details['email'];
		$facebook = $company_details['facebook'];
		$twitter = $company_details['twitter'];
		$instagram =  $company_details['instagram'];
		$logo = $company_details['logo'];
		$company_name = $company_details['company_name'];
		$phone = $company_details['phone'];
        $google = '';
		
		// if(!empty($email))
		// {
		// 	$email = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$twitter = '<li class="pm_tip_static_bottom" title="Twitter"><a href="#" class="fa fa-twitter" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$linkedin = '<li class="pm_tip_static_bottom" title="Linkedin"><a href="#" class="fa fa-linkedin" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		// if(!empty($facebook))
		// {
		// 	$instagram = '<li class="pm_tip_static_bottom" title="Instagram"><a href="#" class="fa fa-instagram" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$google = '<li class="pm_tip_static_bottom" title="Google Plus"><a href="#" class="fa fa-google-plus" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
		
		// if(!empty($facebook))
		// {
		// 	$facebook = '<li class="pm_tip_static_bottom" title="Facebook"><a href="#" class="fa fa-facebook" style="border: 3px solid #B22222; color: #B22222; " target="_blank"></a></li>';
		// }
	}
	else
	{
		$email = '';
		$facebook = '';
		$twitter = '';
		$linkedin = '';
		$logo = '';
		$company_name = '';
		$google = '';
	}
    // var_dump($logo);die()
$branches_rs = $this->site_model->get_all_branches();

    $branches_list = '';
    if($branches_rs->num_rows() > 0)
    {
        foreach ($branches_rs->result() as $key => $value) {
            # code...
            $branch_name = $value->branch_name;
            $branch_email = $value->branch_email;
            $branch_phone = $value->branch_phone;
            $branch_address = $value->branch_address;
            // $branch_postal_code = $value->branch_postal_code;
            $branch_location = $value->branch_location;
            $branch_building = $value->branch_building;
            $branch_floor = $value->branch_floor;
            $location_link = $value->location_link;
            
          $branches_list .= ' <div class="header-phone"> <i class="fa fa-phone"></i> '.$branch_name.' - '.$branch_phone.' </div>';
    }
  }

?>

<header class="header">
        <div class="header-quickLinks js-header-quickLinks d-lg-none">
            <div class="quickLinks-top js-quickLinks-top"></div>
            <div class="js-quickLinks-wrap-m">
            </div>
        </div>
        <div class="header-topline d-none d-lg-flex">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-auto d-flex align-items-center">
                        <?php echo $branches_list;?>
                    </div>
                    <div class="col-auto ml-auto d-flex align-items-center">
                         <span class="header-social">
                            <a href="<?php echo $facebook;?>" class="hovicon"><i class="fa fa-facebook"></i></a>
                            <a href="<?php echo $twitter;?>" class="hovicon"><i class="fa fa-twitter"></i></a>
                            <a href="<?php echo $instagram;?>" class="hovicon"><i class="fa fa-instagram"></i></a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-content">
            <div class="container">
                <div class="row align-items-lg-center">
                    <button class="navbar-toggler collapsed" data-toggle="collapse" data-target="#navbarNavDropdown">
                        <span style="color:#31569b;" class="fa fa-list" ></span>
                    </button>
                    <div class="col-lg-auto col-lg-2 d-flex align-items-lg-center">
                        <a href="" class="header-logo"><img src="<?php echo base_url().'assets/logo/'.$logo?>" alt="" class="img-fluid"></a>
                    </div>
                    <div class="col-lg ml-auto header-nav-wrap">
                        <div class="header-nav js-header-nav">
                            <nav class="navbar navbar-expand-lg btco-hover-menu">
                                <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                                    <ul class="navbar-nav">
                                        <?php echo $this->site_model->get_navigation();?>
                                    </ul>
                                </div>
                            </nav>
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </header>
<!--header-->
