    <?php
    // $mini_desc = '';
    
    // var_dump($meta_tags);die();
  
    if(!isset($meta_tags) OR empty($meta_tags))
    {
        $contacts = $this->site_model->get_contacts();
        $post_meta = strip_tags($contacts['post_meta']);
        $mini_desc = strip_tags($contacts['vision']);
        
    }
    else
    {
        $post_meta = strip_tags($meta_tags);
        $post_meta = strip_tags($meta_tags);
    }
    ?>
  	<head>        
        <title><?php echo $title;?></title>
        <!--meta-->
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <meta name="author" content="Admin" />
        <meta name="MobileOptimized" content="320" />
        <meta name="HandheldFriendly" content="true">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />
        <meta name="description" content="<?php echo $mini_desc;?>" />
        <meta name="keywords" content="<?php echo $post_meta;?>" />
        <meta property="og:locale" content="en_US" />
	    <meta property="og:type" content="website" />
        <meta property="og:title" content="<?php echo $title;?>" />
        <meta property="og:description" content="<?php echo $mini_desc;?>" />
        <meta property="og:url" content="https://familyhealthdentalclinic.com/" />
        <meta property="og:site_name" content="Family Health Dental Clinic" />
        <meta property="article:publisher" content="https://www.facebook.com/FamilyHealthDentalClinic/" />
        <meta property="og:image" content="https://scontent.fnbo9-1.fna.fbcdn.net/v/t1.6435-9/82864633_127759232047087_397959016673705984_n.jpg?_nc_cat=103&ccb=1-5&_nc_sid=e3f864&_nc_ohc=7ZB6Bro0ut8AX8nP9S2&_nc_pt=5&_nc_ht=scontent.fnbo9-1.fna&oh=00_AT8ZVMRy9wxLij_gZA1z4lAHFDjoNgHzC4lZS31PyLd_rQ&oe=62622FAA" />
	    <meta property="og:image:width" content="390" />
	    <meta property="og:image:height" content="108" />
        <meta name="format-detection" content="telephone=no">
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:title" content="An Affordable Dental Clinic in Nairobi - Family Health Dental Clinic" />
	    <meta name="twitter:description" content="An Affordable, fully equipped and Modern Dental Clinic in Nairobi offering Specialized Dental Treatment. Call 0733219208/ 0718501426." />
	    <meta name="twitter:image" content="https://pbs.twimg.com/media/Ez5IOfqXsAAE2x7?format=jpg&name=small" />
	    <meta name="twitter:site" content="@fh_dentalclinic" />
        
        
        
        <!-- FAVICONS ICON -->
        <!--<link href="<?php echo base_url().'assets/themes/dentco/'?>vendor/slick/slick.css" rel="stylesheet">-->
        <!--<link href="<?php echo base_url().'assets/themes/dentco/'?>vendor/animate/animate.min.css" rel="stylesheet">-->

        <script src="<?php echo base_url().'assets/themes/bootstrap/'?>css/bootstrap.css"></script>
        <link href="<?php echo base_url().'assets/themes/dentco/'?>icons/style.css" rel="stylesheet">
        <link href="<?php echo base_url().'assets/themes/dentco/'?>vendor/bootstrap-datetimepicker/bootstrap-datetimepicker.css" rel="stylesheet">
        <link href="<?php echo base_url().'assets/themes/dentco/'?>css/style.css" rel="stylesheet">
         <!-- <link href="<?php echo base_url().'assets/themes/dentco/'?>css/style-O.css" rel="stylesheet"> -->
        <link href="<?php echo base_url().'assets/themes/dentco/'?>color/color.css" rel="stylesheet">
    <!--Favicon-->
        <link rel="icon" href="<?php echo base_url().'assets/themes/dentco/'?>images/favicon.png" type="image/x-icon">
        <link rel="canonical" href="<?php echo site_url()?><?php echo(uri_string())?>"/>
        
        
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
        <!-- Google map -->
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

            
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/";?>fontawesome/css/font-awesome.css" type="text/css">

        <script src="<?php echo base_url().'assets/themes/theme/'?>js/jquery.min.js"></script><!-- JQUERY.MIN JS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.js"></script>
    
        
        <!-- Google Tag Manager -->
        <script async>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-TCWM2FP');</script>
        <!-- End Google Tag Manager -->
        
        <!-- JSON-LD markup generated by Google Structured Data Markup Helper. -->
<script type="application/ld+json">
[ {
  "@context" : "http://schema.org",
  "@type" : "LocalBusiness",
  "name" : "Family Health Dental Clinic",
  "image" : "https://pbs.twimg.com/media/EnQi7_JXEAIUWOV.jpg",
  "telephone" : "+254718501426",
  "email" : "info@familyhealthdentalclinic.com",
  "address" : {
    "@type" : "PostalAddress",
    "streetAddress" : "Langata - Mbagathi Roundabout TMALL 3rd Floor.",
    "addressLocality" : "Nairobi",
    "addressRegion" : "Nairobi",
    "addressCountry" : "Kenya"
  },
  "url" : "https://familyhealthdentalclinic.com/",
  "priceRange": "Depends on Procedure",
  "aggregateRating" : {
    "@type" : "AggregateRating",
    "ratingValue" : "4.9",
    "bestRating" : "5.0",
    "ratingCount" : "70"
  },
  "review" : {
    "@type" : "Review",
    "author" : {
      "@type" : "Person",
      "name" : "Betty Mutwiri"
    },
    "reviewRating" : {
      "@type" : "Rating",
      "ratingValue" : "5.0"
    },
    "reviewBody" : "We loved our visit today. Exceptional ambience, consideration to have a kids play section was a plus for me. The dentists wow was very patient and professional. I would recommend this to parents and guardians."
  }
}, {
  "@context" : "http://schema.org",
  "@type" : "LocalBusiness",
  "name" : "Family Health Dental Clinic",
  "image" : "https://pbs.twimg.com/media/EnQi7_JXEAIUWOV.jpg",
  "email" : "familyhealthdentalclinic@gmail.com",
  "telephone" : "+254718501426",
  
  "address" : {
    "@type" : "PostalAddress",
    "streetAddress" : "Rabai RD,Buruburu The Point Mall Suite C90",
    "addressLocality" : "Nairobi",
    "addressRegion" : "Nairobi",
    "addressCountry" : "Kenya"
  },
  "url" : "https://familyhealthdentalclinic.com/",
  "priceRange": "Depends on Procedure",
  "aggregateRating" : {
    "@type" : "AggregateRating",
    "ratingValue" : "4.9",
    "bestRating" : "5.0",
    "ratingCount" : "70"
  },
  "review" : {
    "@type" : "Review",
    "author" : {
      "@type" : "Person",
      "name" : "Betty Mutwiri"
    },
    "reviewRating" : {
      "@type" : "Rating",
      "ratingValue" : "5.0"
    },
    "reviewBody" : "We loved our visit today. Exceptional ambience, consideration to have a kids play section was a plus for me. The dentists wow was very patient and professional. I would recommend this to parents and guardians."
  }
} ]
</script>

<script type="application/ld+json">
{
  "@context": "https://schema.org/", 
  "@type": "BreadcrumbList", 
  "itemListElement": [{
    "@type": "ListItem", 
    "position": 1, 
    "name": "Home",
    "item": "https://familyhealthdentalclinic.com/"  
  },{
    "@type": "ListItem", 
    "position": 2, 
    "name": "About Us",
    "item": "https://familyhealthdentalclinic.com/about-us"  
  },{
    "@type": "ListItem", 
    "position": 3, 
    "name": "Services",
    "item": "https://familyhealthdentalclinic.com/services"  
  },{
    "@type": "ListItem", 
    "position": 4, 
    "name": "Dental consultation and examination",
    "item": "https://familyhealthdentalclinic.com/view-service-category/Dental-consultation-and-examination"  
  },{
    "@type": "ListItem", 
    "position": 5, 
    "name": "Dental Xrays",
    "item": "https://familyhealthdentalclinic.com/view-service-category/Dental-Xrays"  
  },{
    "@type": "ListItem", 
    "position": 6, 
    "name": "Full Mouth Scaling gum treatment",
    "item": "https://familyhealthdentalclinic.com/view-service-category/Full-Mouth-Scaling-gum-treatment"  
  },{
    "@type": "ListItem", 
    "position": 7, 
    "name": "GUM DISEASES",
    "item": "https://familyhealthdentalclinic.com/view-service-category/GUM-DISEASES"  
  },{
    "@type": "ListItem", 
    "position": 8, 
    "name": "Minor Oral Surgery",
    "item": "https://familyhealthdentalclinic.com/view-service-category/Minor-Oral-Surgery"  
  },{
    "@type": "ListItem", 
    "position": 9, 
    "name": "Paediatric Dentistry",
    "item": "https://familyhealthdentalclinic.com/view-service-category/Paediatric-Dentistry"  
  },{
    "@type": "ListItem", 
    "position": 10, 
    "name": "Root Canal Treatment",
    "item": "https://familyhealthdentalclinic.com/view-service-category/Root-Canal-Treatment"  
  },{
    "@type": "ListItem", 
    "position": 11, 
    "name": "Tooth Alignment Braces",
    "item": "https://familyhealthdentalclinic.com/view-service-category/Tooth-Alignment-Braces"  
  },{
    "@type": "ListItem", 
    "position": 12, 
    "name": "Tooth Extractions",
    "item": "https://familyhealthdentalclinic.com/view-service-category/Tooth-Extractions"  
  },{
    "@type": "ListItem", 
    "position": 13, 
    "name": "Tooth Fillings",
    "item": "https://familyhealthdentalclinic.com/view-service-category/Tooth-Fillings"  
  },{
    "@type": "ListItem", 
    "position": 14, 
    "name": "Tooth replacements Implants crowns and bridge",
    "item": "https://familyhealthdentalclinic.com/view-service-category/Tooth-replacements-Implants-crowns-and-bridge"  
  },{
    "@type": "ListItem", 
    "position": 15, 
    "name": "Tooth Whitening",
    "item": "https://familyhealthdentalclinic.com/view-service-category/Tooth-Whitening"  
  },{
    "@type": "ListItem", 
    "position": 16, 
    "name": "Our Branches",
    "item": "https://familyhealthdentalclinic.com/our-branches"  
  },{
    "@type": "ListItem", 
    "position": 17, 
    "name": "Our Team",
    "item": "https://familyhealthdentalclinic.com/our-team"  
  },{
    "@type": "ListItem", 
    "position": 18, 
    "name": "CSR",
    "item": "https://familyhealthdentalclinic.com/csr"  
  },{
    "@type": "ListItem", 
    "position": 19, 
    "name": "Our Blog",
    "item": "https://familyhealthdentalclinic.com/whats-new"  
  },{
    "@type": "ListItem", 
    "position": 20, 
    "name": "Contact Us",
    "item": "https://familyhealthdentalclinic.com/contact-us"  
  },{
    "@type": "ListItem", 
    "position": 21, 
    "name": "Privacy Policy",
    "item": "https://familyhealthdentalclinic.com/privacy-policy"  
  },{
    "@type": "ListItem", 
    "position": 22, 
    "name": "TOOTH DISCOLORATION CAUSES, PREVENTION, TREATMENT",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/discolored-teeth"  
  },{
    "@type": "ListItem", 
    "position": 23, 
    "name": "HOW TO FLOSS TEETH",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/how-to-floss-teeth"  
  },{
    "@type": "ListItem", 
    "position": 24, 
    "name": "BABY TEETH: WHEN THEY ERUPT, WHEN THEY ARE SHED & HOW TO TAKE CARE OF THEM",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/baby-teeth"  
  },{
    "@type": "ListItem", 
    "position": 25, 
    "name": "TEETH BRACES: 10 TIPS TO PROTECTING YOUR CHILD'S BRACES",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/10-tips-to-protecting-your-childs-braces"  
  },{
    "@type": "ListItem", 
    "position": 26, 
    "name": "TOOTH EXTRACTION IN KENYA",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/tooth-extraction-in-kenya"  
  },{
    "@type": "ListItem", 
    "position": 27, 
    "name": "DENTAL FILLINGS VS ROOT CANAL TREATMENT FOR TOOTH DECAY",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/dental-fillings-vs-root-canal"  
  },{
    "@type": "ListItem", 
    "position": 28, 
    "name": "TOOTH LOSS AND REPLACEMENT OPTIONS",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/tooth-replacement-options"  
  },{
    "@type": "ListItem", 
    "position": 29, 
    "name": "TOOTH BRUSHING DOs AND DON'Ts",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/brushing-teeth-guide"  
  },{
    "@type": "ListItem", 
    "position": 30, 
    "name": "DENTAL FLUOROSIS: FACTS AND MYTHS",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/dental-fluorosis-facts-and-myths"  
  },{
    "@type": "ListItem", 
    "position": 31, 
    "name": "HOW TO USE DENTAL BRIDGES PROPERLY & AVOID PROBLEMS",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/how-to-use-dental-bridges-properly"  
  },{
    "@type": "ListItem", 
    "position": 32, 
    "name": "DENTAL BRIDGES Vs. DENTAL IMPLANTS FOR TOOTH REPLACEMENT",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/dental-bridges-vs-dental-implants"  
  },{
    "@type": "ListItem", 
    "position": 33, 
    "name": "DENTAL BRIDGES: WHO NEEDS THEM, TYPES, PROCEDURE, BENEFITS & DANGERS/COMPLICATIONS",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/dental-bridges"  
  },{
    "@type": "ListItem", 
    "position": 34, 
    "name": "TOOTH DECAY & BAD BREATH: CAUSES & SOLUTIONS",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/tooth-decay-bad-breath-causes-solutions"  
  },{
    "@type": "ListItem", 
    "position": 35, 
    "name": "HOW WE FIX TEETH ALIGNMENT PROBLEMS: TEETH BRACES",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/teeth-braces"  
  },{
    "@type": "ListItem", 
    "position": 36, 
    "name": "DENTAL IMPLANTS PROCEDURE, BENEFITS, RISKS AND COSTS IN NAIROBI",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/dental-implants-procedure-benefits-risks-costs"  
  },{
    "@type": "ListItem", 
    "position": 37, 
    "name": "DENTAL IMPLANTS VS DENTURES, A DETAILED COMPARISON",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/dental-implants-vs-dentures"  
  },{
    "@type": "ListItem", 
    "position": 38, 
    "name": "DENTAL IMPLANTS CARE TIPS",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/dental-implants-care-tips"  
  },{
    "@type": "ListItem", 
    "position": 39, 
    "name": "DENTAL FILLINGS: TYPES, PROCEDURE, BENEFITS & COST IN NAIROBI",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/dental-fillings"  
  },{
    "@type": "ListItem", 
    "position": 40, 
    "name": "10 TIPS TO PROTECTING YOUR CHILD'S BRACES",
    "item": "https://familyhealthdentalclinic.com/blog/view-single/10-tips-to-protecting-your-childs-braces"  
  }]
}
</script>
<script type="application/ld+json" async>
{
  "@context": "https://schema.org",
  "@type": "FAQPage",
  "mainEntity": [{
    "@type": "Question",
    "name": "What are the types of dental fillings?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "We have temporary and permanent fillings. Temporary fillings are used in between dental procedures like root canal treatment. Zinc oxide eugenol cement is usually used.
Permanent fillings last longer although they need to be replaced after some time. They include amalgam, composite and glass ionomer cement.
 Amalgam filling contains alloys of silver and mercury. They are long-lasting and cheaper compared to other filling material. The grey color of amalgam does not make it aesthetically pleasing therefore it is not used in visible teeth.
Composite material is very popular because they are available in different shades therefore can match the color of your teeth. They can last up to 5-10 years. Do not be alarmed when you see the dentist flash a blue light into your mouth when they use composite filling. Composite sets ‘hardens’ when exposed to this light.
Glass ionomer cements, just like composite, are available in different shades. They release fluoride therefore help to prevent caries. The only setback is that they are weaker than composite and can break or wear out."
    }
  },{
    "@type": "Question",
    "name": "How long do dental fillings last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "This depends on the type of filling material used.  Glass ionomer cements are weak and lasts the shortest amount of time. Composite fillings can last from 5-12 years whereas amalgam fillings can last for more than 15 years.
Proper dental hygiene is essential to maintain fillings for longer periods. Brush and floss twice daily. Visit your dentist for checkups and cleaning. During the visit they can monitor the state of the fillings."
    }
  },{
    "@type": "Question",
    "name": "Which is the best filling?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The choice of filling depends on the tooth on which the filling is going to be placed. Amalgam is a good filling material and it’s cheaper, but it cannot be placed on your front teeth because it’s grey in color.
Most dentists prefer using composite material because they are strong, can last for years and are tooth colored. They are also relatively affordable."
    }
  },{
    "@type": "Question",
    "name": "How much do fillings cost in Kenya?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The cost of dental fillings in Kenya ranges from 3,000 – 10,000 depending on the material to be filled and the tooth to be filled."
    }
  },{
    "@type": "Question",
    "name": "Is a filling better than a crown?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Crowns are used to reinforce fillings. Fillings are used to cover holes left after decayed tooth parts. If the hole was big, a big filling Crowns are used to reinforce fillings. Fillings are used to cover holes left after decayed tooth parts. If the hole was big, a big filling would be required. Fillings done on molars might require the reinforcement of a crown because the region experiences a lot of force from chewing. The crown protects the filling from fracture or getting dislodged.
Depending on the condition of the teeth the dentist may either choose a filling or a crown. For a cracked tooth, a crown is more appropriate. The dentist will choose what works for your case."
    }
  },{
    "@type": "Question",
    "name": "Do I really need a filling?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Fillings are a very popular and effective method for caries management. If you have cavities, you may need a filling. A proper dental examination is done to check whether you need the filling. 
Once cavities have been discovered, their sizes will also affect the choice of treatment. Very small cavities may not need a filling, the doctor may recommend a fluoride rinse and proper dental hygiene. Very large fillings on the hand will fail after some time therefore fillings are not recommended in deep cavities."
    }
  },{
    "@type": "Question",
    "name": "Can cavities spread to other teeth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No. Teeth cannot spread cavities to adjacent teeth. The bacteria that cause the cavities, however, can spread to other teeth and cause carries in other teeth."
    }
  },{
    "@type": "Question",
    "name": "Can cavities get worse after a filling?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ideally no. Before a filling is done, all the bacteria and decayed tooth are removed. If the bacteria is removed the cavities cannot progress. However, if the cleaning was not done properly the cavities will progress and you will still feel pain. Visit your dentist if you still have a toothache after a filling has been done."
    }
  },{
    "@type": "Question",
    "name": "Does salty water help cavities?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Salty water has been found to reduce the number of bacteria. This however does not get rid of cavities. To get rid of cavities you need to visit the dentist. Being an antibacterial agent, salty water should not replace brushing and flossing teeth."
    }
  },{
    "@type": "Question",
    "name": "What are the disadvantages of tooth filling?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Tooth fillings have a lifespan and require to be replaced after some time.
Amalgam is a metal compound and can expand or contract when exposed to extreme temperature changes. These changes can lead to tooth fracture and sensitivity 
Fillings can sustain damage and break.
Fillings made of amalgam are very noticeable even on back teeth."
    }
  },{
    "@type": "Question",
    "name": "How do dentists remove old fillings?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There are major concerns on how amalgam fillings are removed. People are more aware of the dangers of mercury. The removal of the filling can release mercury vapor into the air. There are steps used to minimize exposure to the vapor.
 During the removal, rubber dams are used to isolate the tooth from the rest of the mouth. The filling material does not come into contact with your mouth. The dentist breaks the amalgam into pieces rather than trimming it down. Trimming the filling down will release a lot of mercury vapor into the air thereby increasing the risk of inhalation.

A suction pump is used to create negative pressure around the area of work. It sucks in air which contains the mercury vapor therefore the vapor is not released into the air.  Disposable hair nets and gowns can be provided to the patient and the staff to prevent their contact with the vapor."
    }
  },{
    "@type": "Question",
    "name": "Do fillings make your teeth weaker?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Ideally fillings are used to support teeth. They protect the tooth from damage by fractures and seal the cavities. However, if a filling is placed on a big cavity, it might be a source of weakness. When a lot of force is applied on the filling, it can break and cause tooth fracture. The strength of a filling depends on the integrity of the remaining part of the tooth. A crown may be required to reinforce the filling. Therefore, fillings alone are not suitable for all occasions."
    }
  },{
    "@type": "Question",
    "name": "What do dentists do for cavities?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "When you get to the clinic with cavities, the dentist will send you for x-rays. The x-rays will show how many layers of the teeth have been affected by the cavity. Teeth have three layers of enamel, dentine, and pulp. If the infection has not spread to the pulp and is contained in the first two layers, a filling will be recommended.
If the cavity has spread to the pulp, root canal treatment or an extraction are recommended. After the treatment you will be advised on how to take care of your teeth to prevent development of more cavities."
    }
  },{
    "@type": "Question",
    "name": "Why do composite fillings fail?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Composite is a very good filling material. Failure can be due to secondary caries development, fracture of the tooth and wear of the filling. Failure by caries can be avoided if you practice good dental hygiene practices. Visit your dentist twice a year to check at the status of the filling to detect whether you need a replacement. Doing the replacement on time can prevent tooth fracture."
    }
  },{
    "@type": "Question",
    "name": "What happens if you ignore cavities?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Cavities result from acids released by bacteria in the mouth. The acids are produced when the bacteria break down food particles, in the process some sulfur compounds are released which cause bad breath. 
If you ignore the cavity these bacteria continue to multiply because it is hard to clean inside the cavity. Acids released by the bacteria dissolve the minerals that form teeth leading to the spread of the cavity. You will start having bad breath because of the sulfur compounds released. 
When the infection reaches deeper layers of the tooth, you might start experiencing toothache which will force you to go to the dentist.  If you notice you have cavities you should go to the dentist before you get to the toothache stage. We all know it is not the most pleasant experience."
    }
  },{
    "@type": "Question",
    "name": "Can you put fillings in front teeth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. There are types of filling materials that can be used in the front teeth. They can be shade matched and look like your natural teeth. When done well, nobody can notice you have a filling."
    }
  },{
    "@type": "Question",
    "name": "Should I remove my amalgam filling?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If the filling is in perfect condition, there is no need to remove it. Amalgam is one of the best filling materials for use in the molar region. It can withstand the forces of chewing. 
However, if you are bothered by the grey color there are other filling materials like composite which can perform as good as amalgam. Composite has different shades, and you can choose with the guide of your dentist, the color that matches your teeth."
    }
  },{
    "@type": "Question",
    "name": "How does baking soda get rid of cavities?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Baking soda cannot get rid of cavities, but it can reduce chances of getting cavities. Cavities are formed when acids (etch) dissolve teeth. Baking soda is alkaline; it neutralizes the acids therefore no cavities are formed. It can also prevent progression of the cavities but can’t get rid of the initial problem."
    }
  },{
    "@type": "Question",
    "name": "How many cavities is normal?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Cavities are not normal. Your molars and premolars are not flat, they have ridges and grooves. These ridges and grooves are important in chewing and allow the teeth to lock into each other.
Cavities on the other hand are as a result of tooth decay. If you have cavities visit your dentist so they can get rid of them."
    }
  },{
    "@type": "Question",
    "name": "Do fillings make teeth stronger?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Fillings restore the strength of teeth damaged by cavities. The functions and appearance of the tooth are restored. The filling also protects the tooth from further damage by either decay or cracking."
    }
  },{
    "@type": "Question",
    "name": "Is decay a cavity?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Tooth decay is the damage caused on the layers of the teeth by acids produced by bacteria. Cavities develop from tooth decay. Cavities are holes formed when the acid etches (removes) the layers of the teeth. Cavities are one of the consequences of tooth decay."
    }
  },{
    "@type": "Question",
    "name": "What can I do instead of a root canal?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The only alternative to a root canal treatment is an extraction. For a root canal treatment to be indicated, it means that an infection has spread to the pulp. The infection can spread from the pulp to the bone and may infect the whole jawbone. We can prevent the spread of infection by removing the infected pulp (by root canal treatment) or removing the infected tooth."
    }
  },{
    "@type": "Question",
    "name": "Can root canal fail?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. There are some instances where the treatment can fail. Root canal treatment failure manifests as: pain, swelling, tooth discoloration, tenderness in the gums, tooth sensitivity, and swelling of the gums.
Some of the causes are:
Missing canals. There are anatomical variations in some patients such that they have extra roots or root canal. The dentist might miss these variations on the x-ray and fail to seal the extra canal leaving the infection in the root. 
Cracked tooth. After the root canal a filling is done. In some instances, the filling might break due to forces of mastication in the mouth leading to a cracked tooth. A crown is recommended after a root canal treatment to prevent cracking of the teeth.
Over instrumentation. During the procedure, the dentist may go deeper than they should and injure the underlying tissue. Injury to underlying nerves will cause pain during and after the procedure. With the use of x-rays and a device known as apex locator, this can be prevented.
Incomplete disinfection and sealing. Root canals are quite narrow, and some are curved making it difficult to clean them. If some bacteria is left in the canal, it can cause further tooth decay and the infection can spread to the bone.
 Dislodged filling. When the filling is not done well, it might dislodge or start to leak. The patient experiences pain and or sensitivity.
A root canal retreatment can be done following root canal failure. The dentist goes back and performs another root canal. The other option is a procedure known as apicoectomy. Apicoectomy entails accessing the root canal via the gums and cleaning the area directly without removing the filling or crown."
    }
  },{
    "@type": "Question",
    "name": "What are the symptoms of an infected root canal?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Pain and discomfort. If you experience pain for more than one week after the procedure you should visit your dentist.
Pus discharge from the gums
Tenderness of the gums
A bad taste in your mouth or bad breath
Swelling of the area around the teeth
Tooth discoloration
Tooth sensitivity"
    }
  },{
    "@type": "Question",
    "name": "Do you get stitches after a root canal treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No. root canal treatment does not involve cutting through the gums. The dentist drills a hole through your tooth, removes the pulp contents, seals the root, and puts in a filling."
    }
  },{
    "@type": "Question",
    "name": "Is a crown needed after root canal?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It depends on the condition of the tooth after the root canal treatment and the position of the tooth. If the tooth is in good condition and can support the filling material adequately, the dentist might opt to fill it and leave it.
Anterior teeth (incisors) do not experience a lot of pressure during chewing and if the tooth is in good condition, a filling might be adequate. In most cases the dentist will recommend crowns for posterior teeth (molars) because they are used for chewing food. Crowns reinforce and strengthen the tooth preventing fractures."
    }
  },{
    "@type": "Question",
    "name": "Do x-rays detect whether I need root canal?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. X-rays are used to visualize how far a tooth decay has spread. Teeth have three layers: enamel, dentine and pulp. If the infection is within the first two layers (enamel and dentine), a filling can be done. If it has spread to the pulp, then a root canal is done. 
Apart from coming up with a diagnosis, x-rays are used during root canal treatment to check for the depth and number of roots, and if the treatment has been successful."
    }
  },{
    "@type": "Question",
    "name": "Are antibiotics necessary before root canal?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, if you have an active infection. The dentist prescribes antibiotics and sends you home till the infection has cleared up, which takes about a week. If you have an infection, the bacteria might spread into the bloodstream and infect other organs like the brain. Infected areas are also difficult to numb with local anesthesia. The patient experiences a lot of pain if the region has not been properly numbed.
Your medical history may also necessitate antibiotic prophylaxis. Patients with a history of heart diseases require antibiotic prophylaxis to prevent infective endocarditis. Infective endocarditis is an infection of heart valves or the interior surface of the chambers of the heart. 
It is important to share your medical history with your dentist, so that they can assess your overall and oral health."
    }
  },{
    "@type": "Question",
    "name": "Can you avoid root canal with antibiotics?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No. Taking antibiotics can clear active infection but cannot clear the initial infection. Once you are done with your course of antibiotics, the bacteria continue to multiply causes further decay and pain. 
You might be tempted to get another dose because the infection is still present, this might eventually lead to drug resistance. To remove the initial infection and any bacteria reservoir you need root canal therapy."
    }
  },{
    "@type": "Question",
    "name": "What happens if a root canal goes untreated?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The initial symptoms are sensitivity and toothache. The toothache becomes worse with time and might affect your ability to carry out day to day activities. The infection might spread further and cause:
Abscess- infection of the jawbone
Irreversible pulpitis- inflammation of the pulp
Tooth loss
Sepsis- a life threatening condition due to the spread of Infection to the body through blood.
Avoiding treatment is not worth all the complications that come with it. If you are afraid of the procedure, inform your dentist so that they can be gentle with you."
    }
  },{
    "@type": "Question",
    "name": "How painful is a root canal?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Root canal is done under local anesthesia. If adequate anesthesia has been administered, most patients do not complain of pain. Challenges to achieve adequate anesthesia may be due to anatomical differences in the patient and if the patient has an active infection. You might be sent home with a dose of antibiotics to fight off the infection or the dentist can opt to increase the dose of anesthesia.
 What you may feel is some discomfort because there are some instruments going into your mouth and the fact you are opening your mouth for a long time. If you feel any pain inform your dentists. After the procedure you might be given painkillers to manage the pain."
    }
  },{
    "@type": "Question",
    "name": "How many visits does root canal entail?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Usually, root canal treatment takes two or three visits. On the first visit the tooth is cleaned and all the contents of the root canals removed. After the cleaning, some medication is put in the canals to stop any bleeding and kill any bacteria that may be present. A temporary filling is done, and you are discharged.
During the second visit, the temporary filling is removed, the canals are cleaned and shaped. An x-ray can be done to check if the entire length of the root has been cleaned. The dentist will then decide if they will close up the canal to complete the treatment or will put a temporary filling.
There are factors that will determine the number of visits that you may make for the treatment. If you are experiencing any pain or bleeding it means that there is still some pulp tissue remaining. All pulp tissue has to be removed before the canals are sealed permanently."
    }
  },{
    "@type": "Question",
    "name": "Can I eat after a root canal treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. The dentist puts a temporary filling in between the treatment to allow for the patient's comfort. It is advisable to wait until you stop feeling numb to prevent biting your cheeks and tongue. You should avoid hard and chewy food which puts pressure on the filling."
    }
  },{
    "@type": "Question",
    "name": "Is it better to have a root canal or extraction?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Root canal treatment preserves function and aesthetics of the tooth. Root canal treatment rehabilitates the tooth. The infection is removed, the pulp space is cleaned and sealed preventing further spread of infection.
 Initially extractions might be cheaper as compared to root canal treatment. However, after extraction, you will need to replace the lost tooth with an implant or a dental bridge which are a bit costly."
    }
  },{
    "@type": "Question",
    "name": "What can I do instead of a root canal treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The only alternative to a root canal treatment is extraction. Extractions ensure that the infection does not spread to the jawbone and bloodstream. After the extraction you can replace the missing teeth using dental bridges, dental implants, or partial dentures. The cost for root canal treatment in Kenya ranges from Ksh. 8,000 – Ksh. 25,000 per tooth. This majorly depends on the dental clinic you visit and the type of material they use."
    }
  },{
    "@type": "Question",
    "name": "Can I drive after root canal treatment?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is safe to drive after root canal treatment. The procedure is usually done under local anesthesia to numb the area rather than general anesthesia. Local anesthesia only numbs the area around the teeth and does not affect your level of consciousness."
    }
  },{
    "@type": "Question",
    "name": "Is your tooth dead after root canal?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Technically speaking, yes. During the treatment all the nerves and blood vessels supplying the tooth are removed. You cannot feel any pain or sensitivity because the nerves have been removed. If you experience any pain, it means that there is treatment failure."
    }
  },{
    "@type": "Question",
    "name": "How long does root canal last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The success rate of root canal therapy is very high provided that the procedure was done correctly. The treatment may last for 10-15 years. There are some things you can do to make it last longer. Putting a crown over the restoration reinforces the filling and protects it from breaking. Maintaining good oral hygiene by brushing and flossing daily and visiting the dentist twice annually."
    }
  },{
    "@type": "Question",
    "name": "What is a crown and a bridge?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A dental crown is a tooth-shaped ‘cap’ that is cemented onto teeth. Crowns are made from metals and metal alloys or ceramic material. Dental bridges are prostheses used in tooth replacement. They have two parts: a ‘fake tooth’ known as pontics and an abutment tooth. The fake tooth replaces the lost tooth while the abutment tooth is a crown used to fix the bridge onto adjacent teeth."
    }
  },{
    "@type": "Question",
    "name": "How long does a crown and bridge last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There are permanent and temporary crowns and bridges. Temporary crowns and bridges are made in the clinic and are used in between treatments. Permanent crowns and bridges are made in the laboratory based on impressions taken on the first visit. Permanent crowns and dental bridges can last for 10-15 years. This period depends on the material used and how well you take care of your teeth. Good oral hygiene is important in prolonging the life of these devices."
    }
  },{
    "@type": "Question",
    "name": "Do crowns and bridges hurt?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Their placement does not hurt, you are numbed therefore you don’t experience any pain or discomfort.  Some people experience gum tenderness and swelling after the bridge has been placed. This can be managed with over-the-counter painkillers. A few days after placement you shouldn’t feel any pain."
    }
  },{
    "@type": "Question",
    "name": "How many teeth can go on a bridge?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Usually, a bridge is used to replace one or two teeth.  The condition of the anchoring teeth matters a lot because they offer support to the bridge. They need to be strong and healthy to ensure success of the treatment."
    }
  },{
    "@type": "Question",
    "name": "Do they numb you for a permanent crown?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. The first visit involves tooth preparation (removing part of enamel) and impression taking a temporary crown is placed and you are sent home. Before tooth preparation you will numb to avoid discomfort and pain. The impression is taken to the lab to produce the permanent crown. On the second visit, the temporary crown is removed, and the permanent crown is cemented onto the tooth."
    }
  },{
    "@type": "Question",
    "name": "Which is better: Root canal or a bridge?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Root canal treatment is a restorative procedure done on teeth with infected pulps. It involves removing the contents of the pulp and placing a material to seal the root canals. Bridges on the other hand, are used to replace lost teeth. This means that root canal and dental bridges are used in different situations."
    }
  },{
    "@type": "Question",
    "name": "Can you eat normally with a bridge?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes.  They function like natural teeth. Your oral functions, chewing, speech and aesthetics are restored by the bridge after tooth loss. However, you are advised not to eat hard foods like nuts which require a lot of force to chew. This minimizes the risk of breaking the restorations."
    }
  },{
    "@type": "Question",
    "name": "Can a dental bridge be done in one day?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, it is possible to have a dental bridge on your first visit. This is thanks to the new technology that allows for fabrication of bridges instantly. Once the impression has been taken, the information is fed onto a computer. Using this data, a bridge is formed from a block of ceramic that fits your teeth perfectly."
    }
  },{
    "@type": "Question",
    "name": "Can you have a bridge in your front teeth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. Maryland bridges are used in the front teeth. This type uses ‘wings’ to bind to adjacent teeth. They are less invasive, and less preparation is done on the adjacent teeth. Initially the wings were made of metal but now we have porcelain options. Porcelain is tooth colored therefore they are less noticeable."
    }
  },{
    "@type": "Question",
    "name": "Is a dental bridge worth it?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dental bridges are a permanent method of replacing lost teeth. They restore the functions of the teeth.  It also prevents movement of the adjacent teeth into the gap left by the lost teeth. Dental bridges are cheaper than dental implants and they do not involve any surgical procedure."
    }
  },{
    "@type": "Question",
    "name": "Which is better; bridging or implants?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It depends on the number of teeth to be replaced. Generally, dental implants are preferred to dental bridges because dental implants transfer the pressure of chewing to the jawbone this prevents its resorption. Implants are long-lasting. Dental bridges are cheaper and do not involve a surgical procedure like implants."
    }
  },{
    "@type": "Question",
    "name": "Why do my gums hurt under my bridge?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Food particles can get under the bridge especially in loose bridges. These food particles can lead to decay or gum disease which might cause pain. Gum diseases can weaken the teeth supporting the bridge and this might create a strain thereby pain.
You should visit your dentist to determine the cause of the pain and for pain management."
    }
  },{
    "@type": "Question",
    "name": "Does a crown require root canal?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Crowns and root canal are different treatment modalities. Root canal is an endodontic treatment that involves the removal of the contents of the pulp. It is usually done in teeth where the infection has spread to the pulp. After the removal, the root canals are sealed, and a filling is done to close the opening on the tooth. 
In some cases, especially where the root canal has been done on the molars, a crown is placed on the filling. The crown will reinforce the filling and prevent the fracture of the tooth."
    }
  },{
    "@type": "Question",
    "name": "Does a bridge look natural?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. The materials used to make dental bridges come in different shades. We choose the shade that matches with your teeth, and nobody can notice whether any dental work has been done on you."
    }
  },{
    "@type": "Question",
    "name": "How do they put a bridge in your mouth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dental bridges use the adjacent sound tooth for support. Some enamel is removed from the adjacent teeth to allow for insertion of the bridge.  This is known as abutment preparation. Impressions are taken and from those impressions a bridge is produced. The bridge is cemented on the prepared teeth using dental cements."
    }
  },{
    "@type": "Question",
    "name": "Are crowns on front teeth noticeable?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Most crowns are made from ceramic or porcelain material. These materials are tooth colored and are undetectable. They match with your natural teeth."
    }
  },{
    "@type": "Question",
    "name": "Is a filling better than a crown?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The choice between a crown and filling depends on the condition of the teeth. They can also be used together like in root canal treatment. The following factors may affect the choice
The size of the cavity. For small cavities, a filling is adequate, however if the cavity is very deep a crown might be necessary. If a filling is done on a big cavity, the filling might break and cause fracturing of the tooth.
 Crowns are recommended for cracked teeth. Fillings rely on the tooth’s strength therefore if that is compromised the chances of failure rate are high. The crown will protect the tooth from further fractures.
Fillings are cheaper compared to crowns.
Your dentist will discuss with you on which of the two treatment modalities is best for you."
    }
  },{
    "@type": "Question",
    "name": "Can I use mouthwash with a temporary crown?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. It is safe to use mouthwash with a temporary crown. Do not rinse too vigorously to avoid removing the temporary filling."
    }
  },{
    "@type": "Question",
    "name": "Can a tooth rot under a crown?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. The main cause of tooth decay under a crown is poor dental hygiene. You need to treat the crown like your natural teeth. Brush and floss twice daily and go for your check ups twice a year. 
Other causes of tooth decay are ill-fitting crowns and cracked crowns. They are difficult to clean and provide good homes for bacteria to thrive."
    }
  },{
    "@type": "Question",
    "name": "Why is my tooth black under my crown?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It can be an indication of tooth decay. Consult your dentist to find out the cause. The black color can also be seen in porcelain fused metal crowns. These are crowns that are made of metal and porcelain material. The porcelain covers the metal portion of the crown. If this covering was not done adequately, the metal part can be seen."
    }
  },{
    "@type": "Question",
    "name": "Are crowns as strong as natural teeth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No, crowns are not as strong as teeth. Enamel is one of the hardest substances. The materials that are used to make crowns have adequate strength to perform like natural teeth. The strength of the crown is dependent on the individual material used to make it."
    }
  },{
    "@type": "Question",
    "name": "Is crown removal painful?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The removal of temporary crowns is straightforward since temporary adhesives are used. The crown is moved gently to make it loose. The procedure of removing permanent crowns is not painful however it can be uncomfortable"
    }
  },{
    "@type": "Question",
    "name": "Can I chew gum with a bridge?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If it is a temporary bridge, you should refrain from chewing gum since it can be dislodged in the process. It is okay to chew gum if the bridge is permanent. The bridge restores chewing."
    }
  },{
    "@type": "Question",
    "name": "Are dental implants bad?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No
Let’s first understand what dental implants are. Dental implants are artificial tooth roots used to replace lost teeth. It is a ‘screw’ placed in your jawbone that offers a strong foundation to support a crown, bridge or partial denture. The implant material (titanium) integrates the implant and bone to function as a single unit. This integration makes dental implants permanent. Titanium is also biocompatible meaning that it does not produce undesirable effects in the body making it safe. As a matter of fact, it stimulates bone growth making it beneficial to the jawbone."
    }
  },{
    "@type": "Question",
    "name": "What happens if you have a tooth pulled and didn’t replace it?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Why is it necessary to replace missing teeth? When a tooth is lost or removed, the adjacent teeth move in an attempt to fill the space. This movement might lead to malocclusion (misalignment of teeth) thus interfering with aesthetics and costly to fix.
The space may be difficult to clean, especially between teeth predisposing you to caries development in adjacent teeth and other dental problems like gum disease and tooth sensitivity which require treatment.
Your speech may also be affected. We all know that teeth are important in the pronunciation of some letters. Difficulty in pronouncing words may affect your confidence and also lead to miscommunication.
Loss of anterior teeth (incisors and canines) may affect your confidence. You may avoid smiling as much as you would like because you are self-conscious about the gap.
Teeth play a huge role in forming our facial profile. They prevent the checks from collapsing, seen after losing a lot of posterior teeth (molars and premolars). Teeth support the checks, which make you look young and vibrant.
Missing teeth can be replaced by using dental implants, bridges, or using partial dentures."
    }
  },{
    "@type": "Question",
    "name": "Can a dental implant procedure have any complications?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. Placement of the dental implant involves a surgical procedure. Risks of complication accompany any surgical procedure. They include:
Bleeding
Pain
Swelling
Infection
Bone fracture in areas where the jaw is thin
Failure to lock in (failure of the implant to integrate with the surrounding bone)
Damage to adjacent teeth’s roots.’
Damage to nerves near the surgical site
These complications may be avoided by administering drugs before, during and after treatment. Planning and adequate preparation are key. Proper imaging studies, usually of x-rays, are necessary to understand the surrounding surgical site and to determine the bone density; this prevents bone fractures, damage to the roots and nerves. 
Other complications might arise depending on medical history. It is important to share with the dentist proper medical history, existing conditions, medications that you might be on and your family history. The dentist will understand what they are working with and also might give you alternative treatment options if the risk of complication is high."
    }
  },{
    "@type": "Question",
    "name": "How are dental implants inserted?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "After settling on dental implant as the most suitable treatment option and having a treatment plan that works for you, the process begins. The procedure is a minor procedure that takes 1-2 hours on the dental chair. 
Once you are numb, an incision (cut) is made across the missing tooth area. Then the bone is prepared for the implant; preparation involves drilling through the bone. The implant is placed within the bone and a temporary abutment and crown are placed on the implant. You are sent home to allow for healing. After healing, a permanent abutment and crown are placed on the implant."
    }
  },{
    "@type": "Question",
    "name": "How long after implantation can I get a crown?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "After implant placement, we have to wait for the bone to heal and for the integration of the implant with bone which might take 3-4 months."
    }
  },{
    "@type": "Question",
    "name": "Why dental implants?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "When we eat, the pressure to crush the food is transferred to the jawbone. This pressure is necessary to maintain the jawbone and the gums. Dental implants allow for the transfer of this pressure to the bone, therefore, preserving the bone and gums. Other methods of replacing teeth like bridges supported by adjacent teeth and partial dentures, don’t transfer the pressure to the underlying tissue. With time the bone underlying the missing teeth weakens and the gum recedes and eventually, there will be a space between the artificial teeth and the gums."
    }
  },{
    "@type": "Question",
    "name": "What are some of the alternatives to dental implants?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dental bridges
These are fixed dental prostheses that replace one or more missing teeth by permanently joining an artificial tooth to adjacent teeth or dental implants. The (pontics) artificial tooth resembles natural teeth in function, shape and appearance. To support the pontics, the adjacent teeth are prepared to accommodate a crown then the bridge is attached to them via dental cements. The adjacent tooth must be sound (free of cavities and other dental problems) for you to have a dental bridge.  
Partial dentures 
Partial dentures allow you to replace multiple missing teeth with one appliance. They utilize the support of other healthy teeth and structures in your mouth like the palate to stay in place. Partial dentures are removable; this allows for better cleaning, decreasing the chances of cavity development.
The dentist will advise you on which method of replacing missing teeth is best suited for you, putting into consideration your medical history, your preferences and your finances."
    }
  },{
    "@type": "Question",
    "name": "How long do dental implants last?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dental implants are permanent prostheses. They integrate with your jawbone and act as a single unit meaning that they are not replaced and last a lifetime.
 The crown, however, is replaced after 10-15 years due to wear and tear. For dentures supported by implants, their durability depends on the material used to make the denture and patients’ comfort. Their replacement varies from patient to patient and the dentist advises you accordingly."
    }
  },{
    "@type": "Question",
    "name": "How much do dental implants cost?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A single implant tooth cost ranges from Ksh. 80,000 to Ksh. 350,000. The average cost in most dental clinics is Ksh. 150,000. The cost largely depends on the types of procedure to be done in preparation to receive the implant."
    }
  },{
    "@type": "Question",
    "name": "Advantages of dental implants",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "They are comfortable because they become part of your jawbone, therefore, eliminating the discomfort of removable dentures.
Dental implants are durable since they are permanent restoration. This saves the patients time and money.
They do not interfere with the adjacent teeth because they can stand on their own. Other methods of replacing lost teeth depend on adjacent teeth, straining them.
The patient has improved self-esteem because they don’t have any missing teeth. You get your smile back, and you feel more confident about yourself.
You have improved dental functions. Dental implants function like your teeth, allowing you to eat your favorite food and speak without worrying that teeth might slip as in the case of ill-fitting dentures.
Dental implants prevent bone loss by preserving and protecting existing bone. They also stimulate natural bone growth."
    }
  },{
    "@type": "Question",
    "name": "Disadvantages of dental implants",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Although dental implants have a high success rate as a method of replacing missing teeth, they have some disadvantages:

Their placement involves a surgical procedure. Although it is a minor surgical procedure, there are risks involved; these include:
infections, bleeding, pain and damage to the surrounding structures. With proper planning before, during and after the procedure, these risks can be minimized

The procedure takes a long time because it involves bone healing. It might take up to several months; therefore, they are not suitable for patients who want instant solutions to missing teeth

Dental implants are quite costly. The cost varies with the type of implant to be placed, the number of implants, and the condition of the surrounding area"
    }
  },{
    "@type": "Question",
    "name": "How to take care of dental implants",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Treat the implants like your natural teeth; brush and floss twice daily and visit your dentist twice a year for your dental checkup.
The dentist might also recommend antibacterial mouthwash to use."
    }
  },{
    "@type": "Question",
    "name": "Who is a good candidate for a dental implant?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Most individuals are qualified for dental implant treatment. Having healthy gums and enough bone density is necessary for dental implant treatment. If you suffer from gum disease and bone loss, the dentist might recommend other teeth replacement methods. You are also required to have good dental hygiene habits. 
Chronic conditions such as diabetes, osteoporosis (a condition in which bones become weak and break easily) and heart diseases may also affect the treatment choice. Your dentist and physician will advise which treatment option is best for you."
    }
  },{
    "@type": "Question",
    "name": "From what age can one get a dental implant?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dental implants are placed in secondary dentition (after shedding all milk teeth). For most Africans, at 25 years, all teeth of the secondary dentition have erupted. People who are 25 years and above are suitable candidates for implant treatment. You should wait for all teeth to erupt before starting the treatment to avoid interfering with the development of the jawbone and the teeth that have not erupted."
    }
  },{
    "@type": "Question",
    "name": "Is 70 too old for dental implants?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No age is too old for dental implant treatment. Provided you have healthy gums and adequate bone to support your implant; you can have dental implants. Aging is not a risk factor for treatment failure."
    }
  },{
    "@type": "Question",
    "name": "What type of dentist puts in dental implants?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Periodontists – they specialize in treating the supporting structures of teeth
Prosthodontics- they specialize in restoration and replacement of teeth
Oral maxillofacial surgeons- perform surgeries involving the face and oral cavity"
    }
  },{
    "@type": "Question",
    "name": "Do gums grow around dental implants?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. While you are healing from the surgery, the gums grow around the implant to provide support. Temporary abutment and crown are placed on the implant to ensure that the gum doesn’t grow over the implant during the healing process. Good dental hygiene is required to prevent gum recession. The dentist might also recommend an antibacterial mouthwash to keep off bacteria that may cause gum recession."
    }
  },{
    "@type": "Question",
    "name": "How soon after extraction can I have an implant?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Tooth extraction is a surgical procedure and requires healing. It is recommended that you wait for 2-3 months after an extraction to allow for the gums and bone to heal. In some instances, however, you can have an extraction and an implant placed during the same visit. This is only possible if you are free of gum disease and have a healthy and dense jawbone.
If you wait for too long to get an implant, you might require bone grafting treatment. After an extraction, the bone that supports the tooth starts to resorb (loss of bone tissue)."
    }
  },{
    "@type": "Question",
    "name": "Can I have all my teeth pulled and get implants?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. It is, however, important to note that dentistry utilizes preventive and conservative treatment methods. For the dentist to extract all your teeth, they need to have justified reasons like severe decay affecting all your teeth.
Dentures replace missing teeth in whole dentition or upper and lower sets of teeth. Some dentures are supported by dental implants whereas others are supported by the structures in the mouth. Implant-supported dentures are recommended because they maintain the health of the gums and bone.
There are different techniques used in implant-supported dentures; the dentist will advise you on which technique is suitable for you."
    }
  },{
    "@type": "Question",
    "name": "What is the percentage failure of dental implants?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The failure rate of dental implants is estimated to be 5-10% which is fairly low.  The causes of failure may be due to:
Poor positioning of the dental implant can make the crown look unnatural and cause gum recession
Infections in the area of the implant
Nerve damage when the implant is placed too close to a nerve. It is rare, however.
Allergic reaction to the implant material
Failed osseointegration (integration of the bone and the implant). This occurs if there is low bone density making the implant loose.
Some of the signs of implant failure are severe pain and discomfort, swelling, and the implant becoming loose. Please visit the dentist when you experience these symptoms."
    }
  },{
    "@type": "Question",
    "name": "Are dental implants painful?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The procedure itself is not painful since you are under anesthesia. You might feel some pain after the anesthesia wears off. The dentist usually prescribes painkillers to help relieve the pain. The pain may persist for 10 days. If the pain lasts more than ten days or gets worse, you should see the dentist because it might signify treatment failure."
    }
  },{
    "@type": "Question",
    "name": "Can your mouth reject a dental implant?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, the body can reject any foreign material. However, titanium, the material used to make dental implants, is biocompatible. It does not evoke unwanted reactions in the body, making it have a success rate of 95-98%. 
There have been cases of allergies to titanium metal. If you have any metal allergies, it is important to tell your dentist because there are high chances that you may also be allergic to titanium. Implant rejection leads to treatment failure.
Signs that you may have titanium allergies:
Pain in the implant area
Erythema (redness) in the tissue around the implant.
Tissue death around the implant
Bone loss
Eczema (Itchy inflammation) of the gum and the skin around the implant"
    }
  },{
    "@type": "Question",
    "name": "Dos and don’ts after tooth extraction",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dos 
Bite on the gauze for 30 minutes.
Consume soft foods and drinks
Take medication as prescribed.
Rinse with warm salty water gently after 24 hours
Get some rest after the extraction.
Brush gently and avoid brushing the extraction site.
Don’ts
Smoking for at least 24hours after the extraction.
 Take alcohol and do not use alcohol-based mouthwash.
Use straws to drink.
Take hot foods or drinks
Touch the extraction site
Spit saliva.
Perform exercises or any strenuous activities"
    }
  },{
    "@type": "Question",
    "name": "How can I make my tooth heal faster?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Resist from touching the area even with your tongue. I know there will be great temptation to feel the newly created gap but don’t do it. Touching the area might dislodge the blood clot and delay recovery.
Take painkillers prescribed by the dentist to manage the pain.
Sip drinks especially cold drinks. Hot drinks can dissolve the clot delaying the healing. Avoid using straws when taking your drinks.
Use an icepack on your cheek to reduce the swelling and provide some relief.
Bite firmly on the gauze placed by the dentist. The pressure helps to form a blood clot.
Take some rest for at least 24hours after the extraction."
    }
  },{
    "@type": "Question",
    "name": "Do I need sedation during an extraction?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No.  A local anesthesia is usually used to numb the region around the tooth. For most patients, local anesthesia is adequate to numb the pain and they experience minimal discomfort. You will feel mild pressures, as the dentist is working in your mouth.
Some of the reasons why you might need sedation are:
Having a very strong gag reflex
Extreme restlessness 
You have extreme dental anxieties
In cases where multiple oral surgeries have to be done."
    }
  },{
    "@type": "Question",
    "name": "How painful is a tooth extraction?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Extractions are done under a local anesthesia. You only feel slight pressures as the dentist is working in your mouth.  If you feel any pain during the procedure alert the dentist so that they may increase the dose of anesthesia. 
After the procedure, the dentist gives you painkillers to manage the pain after the anesthesia has worn off."
    }
  },{
    "@type": "Question",
    "name": "What is a dry socket?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A dry socket results when the blood clot on your extraction site is dislodged. The bone and nerve endings around the site are exposed to air, food and drinks leading to pain and inflammation.  You can also get an infection because the bone and nerve ending have been exposed to all kinds of bacteria found in the mouth.
A dry socket usually occurs 3-4 days after an extraction. It is a very painful experience. The pain increases with time and is very persistent. Over the counter painkillers do not relieve the pain usually. You might also experience an unpleasant taste and have bad breath. 
Visit your dentist if any of these happen after an extraction."
    }
  },{
    "@type": "Question",
    "name": "How do I prevent a dry socket?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Prevent any activity that will change the pressure in your mouth. Therefore, do not smoke, spit, use straws or do any strenuous activity. You should also avoid poking the area to minimize the risk of dislodging the clot.
Some forms of birth control, especially those containing high levels of estrogen may interfere with the healing process. It is important to share with your doctor if you are on any birth control method so that they can advise you accordingly."
    }
  },{
    "@type": "Question",
    "name": "When can I stop worrying about a dry socket?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If a week has lapsed since your extraction, you should stop worrying. Dry socket usually occurs 3-4 days after an extraction."
    }
  },{
    "@type": "Question",
    "name": "What is the treatment for a dry socket?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "If you suspect you have a dry socket, visit your dentist. At the clinic they will numb you, clean the area and place a medicated dressing. You might be required to visit the clinic again to change the dressing. The dentist will be prescribed pain killers to manage the pain. Antibiotics may be prescribed to prevent infections."
    }
  },{
    "@type": "Question",
    "name": "Can a dry socket heal on its own?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dry socket can heal on its own. However, you may need professional help to clean the area to prevent infection, manage the pain and discomfort, and to speed the healing process.
flushing the area with saltwater to remove any food particles and debris can prevent infection. Use of painkillers and ice packs can help manage the pain.
It is best you see the dentist so that they can assess the situation and manage it properly."
    }
  },{
    "@type": "Question",
    "name": "What should my extraction site look like after 5 days?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "You might notice that the site looks yellow or greyish a few days after the procedure. This is normal and it is an indication that healing is in progress. There should be no more bleeding and the swelling should be minimal."
    }
  },{
    "@type": "Question",
    "name": "How long does it take to recover from a tooth extraction?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "This depends on whether the tooth extracted was a deciduous tooth or a permanent tooth. The recovery following extraction of a deciduous tooth is faster compared to permanent teeth.
After 7-10 day the opening left after extraction should be closed or almost completely closed. The swelling and tenderness of the gums should have subsided at this time. Healing continues and may last months as the bone continues to develop to fill the space left by the tooth’s roots."
    }
  },{
    "@type": "Question",
    "name": "What are the side effects of removing a tooth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The adjacent teeth move in an attempt to fill the space left by the extracted tooth. The movement may cause malocclusion (misalignment of teeth). Naturally, teeth need to be in contact with other teeth. The tooth opposite the gap will grow longer in an attempt to find this contact whereas the teeth on the other side of the gap will grow towards each other. This happens gradually and you might not notice it until some years have passed.
It is difficult to clean the gap left especially if it is between teeth. Food particles that lodge into that space provide nutrition to bacteria allowing them to thrive. These bacteria cause tooth decay to the adjacent teeth and may also cause gum disease.
There are treatment options to replace lost teeth. They include dental implants, partial dentures, and dental bridges. Consult the dentist on which of the treatment options is suitable for you."
    }
  },{
    "@type": "Question",
    "name": "Do you need root canal treatment if you pull your tooth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No. Root canal treatment is an alternative to an extraction. Root canal rehabilitates the tooth instead of pulling it out. During root canal treatment the contents of the root canal are removed and replaced by a material which seals the canals preventing spread of infection into the jawbone. A filling is then put on the tooth to make it look as natural as possible. 
Root canal treatment is generally preferred to extraction because it preserves function and aesthetic of the tooth. It saves the tooth and relieves the pain."
    }
  },{
    "@type": "Question",
    "name": "Which tooth has the longest roots?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The maxillary (upper) canines have the longest roots. The roots are long and curved and may pose a challenge in extracting them. The maxillary canines in deciduous dentition are slender than those in permanent dentition and are easier to extract.
Most dentists prefer to preserve the permanent maxillary canines. If their extraction is necessary, there are different techniques they can use to prevent fracturing the jawbone."
    }
  },{
    "@type": "Question",
    "name": "Do I need antibiotics after extraction?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "In most cases antibiotics are not prescribed after or before an extraction. However, if you have a weak immune system or heart condition, the dentist may prescribe antibiotics to prevent spread of bacteria into the bloodstream. It is important to share your history with the dentist to prevent complications."
    }
  },{
    "@type": "Question",
    "name": "How long does it take to extract a molar?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Once you are numb, extracting the tooth can take between ten and thirty minutes. Time taken to complete the extraction varies from patient to patient."
    }
  },{
    "@type": "Question",
    "name": "What is the best painkiller for tooth extraction?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Take the painkillers prescribed by the dentist. Over the counter painkillers like paracetamol and ibuprofen are used for pain relief after extractions."
    }
  },{
    "@type": "Question",
    "name": "Should I feel pain 5 days after an extraction?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Feeling pain after 5 days is not normal and may be an indication of dry socket. The pain should subside around day 3 post extraction.  Visit the dentist if you are feeling pain 5 days after extraction."
    }
  },{
    "@type": "Question",
    "name": "Can a dentist pull an infected tooth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. The dentist can settle on extracting the tooth if it has severe decay and cannot be saved. Other reasons why the dentist can remove teeth are: orthodontic treatment in cases of overcrowding, impaction and following dental trauma."
    }
  },{
    "@type": "Question",
    "name": "Will the dentist pull a tooth the same day?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Dental extractions are done on a single visit. The procedure takes anywhere from 10 to 45 minutes but may be longer depending on the number of teeth to be extracted."
    }
  },{
    "@type": "Question",
    "name": "Is it better to pull a tooth or crown it?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The only alternative to extracting a tooth is root canal treatment. For a tooth to be a good candidate for extraction, it needs to have severe caries or has an infection that has spread to the pulp. A crown serves to reinforce a filling or for aesthetics. Placing a crown will not get rid of the infection and will cover up the infection allowing it to spread farther."
    }
  },{
    "@type": "Question",
    "name": "Does pulling teeth change your face?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It depends on the number and the tooth that is extracted. The roots of teeth form part of the facial structure. When a tooth is extracted the jaw remodels to accommodate the space left. Remodeling may change the facial profile. These changes are very hard to detect especially if only one tooth has been extracted.
Removing teeth for an orthodontic treatment can improve the face in some patients. For instance, if the patient’s tips don’t touch when at rest due to crowding, extraction of some teeth may be recommended. After the orthodontic treatment, the patient can close their mouth comfortably and can boost their confidence."
    }
  },{
    "@type": "Question",
    "name": "How do I know my baby is teething?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Your baby may experience the following when teething
They are fussy and clingy
Drooling 
The gums are red and swollen
Lack of sleep
Lack of appetite
You might see the tooth buds
They bite everything they can get into their mouth.
These signs are not the same for every child. They occur before the teeth erupt and may become worse as the process proceeds. 
Diarrhea and fever are not normal. If your child is experiencing these symptoms, they might have eaten something that is dirty in an attempt to soothe their gums. You may have to see your pediatrician."
    }
  },{
    "@type": "Question",
    "name": "What do I do if my baby is teething?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Sore gums are common during teething. You can gently rub your clean fingers on their gums. Alternatively, you can use a clean cold gauze pad and apply gentle pressure to soothe their gums. 
Babies find comfort in chewing and biting onto things. You can give them teething rings. The teething ring should be large to prevent swallowing and clean to prevent infections. Some parents prefer using teething necklaces because they don’t fall hence, they don’t get dirt. There are big concerns that the necklace can strangle the baby. If you choose to use the teething necklace, always ensure that the baby is under supervision.
Your fingers can become your baby’s chew toy. Ensure that you fingers are clean and for added comfort you can dip them in cold water.
Be patient with your child. Teething is a normal process, and it will be over after some time. Wipe them when they drool and give them your attention when they require it. If the drooling is too much you can place a bib around them.
If they are experiencing a lot of pain, you can give them painkillers. Consult with your dentist on the painkillers and dosage that you should give to your baby."
    }
  },{
    "@type": "Question",
    "name": "Do you lose all your baby teeth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "In most cases yes. There are some people who don’t shed all their baby teeth. These teeth are known retained teeth. It is usually the second molars. The most common reason for teeth retention is lack of permanent teeth to replace them.
If you have retained teeth, visit the dentist to see whether they will result in any dental problems."
    }
  },{
    "@type": "Question",
    "name": "Do teething babies cry a lot?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Pain is very subjective. Different babies have different experiences when teething. However, teething is very uncomfortable and can make the baby very fussy. Be patient with your baby while they are teething."
    }
  },{
    "@type": "Question",
    "name": "Do milk teeth have roots?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. When the baby teeth erupt, they have complete roots. As they grow older, the roots resorb to accommodate for the eruption of the permanent teeth. This resorption causes tooth loss and that is what enables kids to shed their teeth."
    }
  },{
    "@type": "Question",
    "name": "How can I get my one-year-old to brush their teeth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Getting your child into the habit of brushing their teeth might be difficult. Here are some few tips to help you in this journey
Let your child pick out their toothbrush. Kids toothbrushes come in different shapes and have different characters. Picking out a toothbrush that has their favorite character can get them excited about tooth brushing.
Take turns when brushing. Instead of brushing their teeth, alternate with them and let them brush their own teeth. This makes the child more involved in the process. It is also the basis for the growth of the habit.
Make brushing time a fun time. You can play a song or sing along to a special tooth brushing song. Incorporate something the child loves in the tooth brushing routine to make the child excited about it.
Let your child practice brushing teeth with their toys. You can buy a toy brush and encourage the child to brush their toys without toothpaste. This should be done under supervision. It creates a sense of responsibility in your child
Lastly, be patient with your child. They will get there eventually."
    }
  },{
    "@type": "Question",
    "name": "Can adults have baby teeth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. These are known as retained teeth. If the tooth is structurally, functionally, and aesthetically sound they pose no problem. Usually, it is the second molars that are retained but if it is an incisor or first molar that is retained orthodontic treatment might be required. This is because they may lead to dental complications to adjacent teeth."
    }
  },{
    "@type": "Question",
    "name": "Is it bad if baby teeth erupt out of order?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "No. In some instances some teeth erupt before others. Children are different and not all children follow the sequence of eruption. This is nothing to cause alarm. All the teeth will erupt in due time."
    }
  },{
    "@type": "Question",
    "name": "Is it normal for a 1 year old to have no teeth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. Some children take a long time before their first tooth erupts and some teeth erupt as early as 3 months. The time frame for the first tooth to erupt can range from 3 months to 15 months. Every child is different.
 In most cases the first tooth erupts at 6-8 months. If your child is 18 months and does not have any teeth you should see your dentist for further investigations."
    }
  },{
    "@type": "Question",
    "name": "Why have my baby’s teeth not erupted?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Some of reasons for late teething are
Hereditary factors. Sometimes late teething runs in the family. Ask your parents and grandparents if they had such issues with their children.
Poor nutrition. If the baby is not having adequate breast milk or their formula is not nutritious enough, there might be a delay in their eruption.
Hypothyroidism. This is a condition where the thyroid gland is not producing thyroid hormone. Thyroid hormone is very important in controlling your metabolism rate. Hypothyroidism causes abnormal metabolism and causes lethargy. It also causes a delay in hitting milestones.
Medical conditions. Some conditions such as Down’s syndrome which can delay milestones.
Premature birth or low birth weight. These factors can delay tooth eruption and can also cause enamel defects. The teeth develop during pregnancy and their development can be affected by any problem encountered during this period."
    }
  },{
    "@type": "Question",
    "name": "What are the risks associated with late teething?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Late teething can interfere with weaning. Teeth are important when introducing solid food to the baby. The baby might have difficulty in chewing solid food.
The baby can have issues with speech development. As we all know, teeth are very important in pronouncing some letters. The child may have difficulty communicating. This might also affect their self-esteem.
Late teething can result in malocclusion on permanent teeth. The permanent teeth can erupt before the deciduous teeth are shed leading to crowding and crooked permanent teeth."
    }
  },{
    "@type": "Question",
    "name": "Is teething worse at night?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "You may notice that your baby is fussier at night. During the day the baby is distracted and does not concentrate on the discomfort caused by teething. At night they are exhausted and are more aware of the discomfort making them very fussy."
    }
  },{
    "@type": "Question",
    "name": "How does thumbsucking affect the dental development of my child?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It is normal for babies to suck their thumbs; it provides a soothing and calming effects on them. Between the ages of two and four, most children stop sucking on their thumbs. Some persist with the behavior and may have effects on their developing teeth and may need orthodontic treatment eventually.
Prolonged thumb sucking can cause misalignment of teeth. The thumb applies pressure on the upper front teeth and pushes outwards leading to protrusion of the teeth. The lower front teeth on the other hand can be pushed inwards towards the tongue by the force applied by the thumb. 
Depending on how the child is sucking the thumb can press on the palate (roof of the mouth). The pressure can make the palate become narrower and deeper. This can lead to posterior crossbite. whereby, the back upper teeth bite into the back lower teeth instead of the other way round. Crowding might also result due to the narrow upper arch."
    }
  },{
    "@type": "Question",
    "name": "How can I make my child stop sucking their thumb?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "First of all, don’t scold your child for sucking their thumb. Thumbsucking has been shown to have a soothing and calming effect. If you scold and criticize your child, they may get anxious or get stressed out. The problem may get worse because the child will suck their thumb to get some relief from the stress.
Instead of scolding the child, try some positive reinforcement. You can set goals with your child and once they meet the goal you give them a reward. For instance, you can set a given period of time in which they are not allowed to suck their thumb. If they don’t suck their thumb during that time frame give them what you had promised them. Increase the duration of time gradually and with time they will stop.
If the child is old enough, explain to them how thumb sucking can affect their teeth and facial development. If possible, use images to show the severity of the condition and better understanding. This will motivate them to cease the habit.
See if there are any underlying conditions that may make the child to suck their thumb. Some people suck their thumb when under stress while others when they have difficulty in sleeping. You can address these issues and the habit may cease.
Be patient with the child. It will take some time before they stop this habit."
    }
  },{
    "@type": "Question",
    "name": "Can you fix tooth discoloration?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes. Let's first understand tooth discoloration. Tooth discoloration can be categorized into three different categories depending on the part affected and the cause: extrinsic, intrinsic, and age-related.
Extrinsic. With extrinsic tooth discoloration,  the stains only affect the tooth enamel (outer layer of the tooth). The most common causes of extrinsic tooth discoloration  include: food, beverages, tobacco.  These stains can be managed by observing proper dental hygiene and cleaning at the dental clinic. 
Intrinsic. The stain is located within the tooth and often appears grey. They are difficult to remove with over-the-counter whitening products. Examples of causes of intrinsic stains include: certain medications, trauma or injury to a tooth, tooth decay, excessive fluoride use. 
Age-related. When you age, the enamel on your teeth begins to wear off and dentine becomes more visible. Dentine is yellow in color hence the  yellow appearance becomes more intense with age. Age-related discoloration may be caused by both extrinsic and intrinsic factors.
When  the cause of the stains has been identified, tooth discoloration can be fixed. In other cases like discoloration due to age it might be difficult to fix and other treatment options like veneers and crowns may be used."
    }
  },{
    "@type": "Question",
    "name": "What causes discoloration?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "There are several causes of tooth discoloration, including:
Foods/drinks. Coffee, tea, wines, and certain fruits and vegetables can stain your teeth.
Tobacco use. Smoking or chewing tobacco causes brown stains on teeth .
Poor dental hygiene. Failure to brushing and  flossing can cause plaque and stain-producing substance accumulation.
Aging. As you age, the outer layer of enamel on your teeth wears away, revealing the yellow color of dentine.
Medication. Some drugs such as tetracycline, an antibiotic, when administered to children can cause tooth discoloration. Chemotherapy drugs and some antipsychotic drugs can also cause tooth discoloration in adults and children.
Genetics. Some people have naturally brighter or thicker enamel than others. Genetic conditions such as dentinogenesis imperfecta can give your teeth a grey color."
    }
  },{
    "@type": "Question",
    "name": "How do you get rid of tooth discoloration?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Keep your teeth healthy and looking great by maintaining a good dental hygiene routine which  includes  brushing and flossing twice daily and visiting your dentist twice annually for cleaning. You should limit your consumption of teeth-staining foods and beverages. If you have to consume these products, ensure that you maintain good dental hygiene practices. 
You can use over the counter teeth whitening products or those prescribed by the dentist to whiten the teeth. It is important to note that these products can cause tooth sensitivity, the sensitivity goes after some time. If you experience any gum irritation, visit the dentist for a different treatment plan.
 Veneers can also be used to cover badly stained teeth that cannot be treated by bleaching. They are made of material that are tooth colored and are hard to detect."
    }
  },{
    "@type": "Question",
    "name": "What does a dead tooth look like?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A dead tooth has a different color when compared to the other teeth, it may appear yellow, grey, brown or black. The discoloration gets worse with time as the tooth continues to decay.  Tooth discoloration is one of the first signs of a dying tooth. Dead teeth do not have a blood supply. They result after spread of decay into the pulp or following dental trauma."
    }
  },{
    "@type": "Question",
    "name": "Are brown stains on teeth permanent?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The ability to remove brown stains will depend on the cause of the stains.  Causes of brown stains include; nicotine found in tobacco, certain foods and beverages, dental calculus, fluorosis, medications and  trauma to the tooth. 
Brown stains as a result of severe dental fluorosis are difficult to remove. Whereas brown stains from food and beverages can be removed by observing good dental hygiene practices. Visit your dentist if you want to get rid of brown stains so that they can advise you properly."
    }
  },{
    "@type": "Question",
    "name": "Why is half my tooth yellow?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The yellow color near your gum line is a sign that you have tartar. Tartar or dental calculus, has a rough texture that feels quite different from the smooth surface of tooth enamel. Tartar usually accumulates at the gum line on the front and back surfaces of teeth. Tartar can damage tooth enamel and cause tooth decay. Tartar can also accumulate below the gum line, which can cause  gum disease. Other signs of tartar accumulation include bleeding gums, inflamed gums, and sensitive gums"
    }
  },{
    "@type": "Question",
    "name": "Can badly stained teeth be whitened?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The cause of the stains will affect the choice of  treatment to be offered. Some stains can be removed by change of lifestyle choices where others require professional interventions like veneers and professional whitening.
In most cases badly stained teeth require professional intervention. In-office whitening involves the use of bleaches to whiten the teeth. The results can be seen after the first or second visit and they last longer. In some cases where bleaching is not effective, veneers and porcelain crowns are recommended."
    }
  },{
    "@type": "Question",
    "name": "Will grey teeth fall?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "It depends on the cause of the grey discoloration. Not all grey teeth are as a result of tooth decay or dead teeth. Tooth decay interferes with the integrity of the tooth and the tooth may fall due to weakness. Careful examination of the tooth is necessary to find the cause of discoloration. Grey teeth that are not dying or decaying  can be whitened using whitening treatments."
    }
  },{
    "@type": "Question",
    "name": "Can you leave a dead tooth in your mouth?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "A dead tooth is a tooth that does not have a blood supply. For many people, discoloration may be one of the first signs of a dying tooth. The color may be yellow, brown, grey or black. You may also experience pain in the tooth or gums if you have a dead tooth.
It’s important to treat a dying or dead tooth as soon as possible. If left untreated, the bacteria from the dead tooth can spread and lead to more problems such as loss of additional teeth and abscesses. It could also affect your jawbone and gums."
    }
  },{
    "@type": "Question",
    "name": "Why are my teeth suddenly staining?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Tooth discoloration is usually a gradual process. Something is introduced to your teeth and over time, it slowly darkens your teeth. Tooth discoloration due to dental trauma, is an exception to the gradual process and can cause discoloration over a very short period of time.
This is why daily brushing and flossing is recommended  to prevent tooth discoloration. A proper dental hygiene routine helps remove bacteria and plaque buildup that could lead to decay and discoloration. 
If you are experiencing sudden tooth discoloration, you should see the dentist to find out the cause of the discoloration. If it is due to dental trauma, necessary measures can be taken to prevent loss of the tooth."
    }
  },{
    "@type": "Question",
    "name": "Why are my teeth yellow yet I brush them every day?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "As you age, the white enamel surface of your teeth may wear down. Dentine, which is yellow in color, becomes more visible."
    }
  },{
    "@type": "Question",
    "name": "How can I rebuild my enamel naturally?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Once tooth enamel is damaged, it cannot be brought back. However, weakened enamel can be restored to some degree by improving its mineral content. Although toothpastes and mouthwashes can never “rebuild” teeth, they can contribute to the remineralization of enamel.
Remineralization is the introduction of minerals, especially calcium and fluoride to the teeth. These minerals bond to the surface of the teeth and are drawn to weak points in the enamel. This is especially effective in cases of dental erosion, where the tooth surfaces might be weakened without being cracked or chipped.
Enamel’s chief ingredient is calcium phosphate, also known as hydroxyapatite. Products with high concentrations of calcium phosphate or with fluoride, a common additive, are best at helping teeth to remineralize before damage exceeds the point of no return."
    }
  },{
    "@type": "Question",
    "name": "Why are my child’s teeth grey?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Baby teeth are typically whiter than adult permanent teeth because they are more calcified. Baby teeth can become discolored for many reasons, including:
Inadequate brushing. If baby teeth aren't brushed properly, dental calculus can build up on the teeth. If left untreated the calculus can cause tooth decay which appears as a grey-black color.
Medication use. Taking the antibiotic tetracycline during pregnancy or breast-feeding or giving it to a baby can make their teeth grey.
Trauma to the teeth. Injuries to teeth can make them dark. The dark color could be the result of bleeding within the tooth. The discoloration is usually sudden and follows an accident.
Genetics . Children suffering from dentinogenesis imperfecta have a grey hue in their teeth. This is a genetic condition that affects the normal development of dentine."
    }
  },{
    "@type": "Question",
    "name": "Are dark spots always cavities?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "The biggest difference is that black stains are a buildup on your teeth, but cavities are a hole in the tooth. Use your tongue or a tool that’s safe for your tooth (such as a toothpick or flosser) to feel the dark area. If it’s built up from the tooth, it’s a black stain. If it’s a hole, it’s a cavity.  A  black stain that is neither built up nor a hole is likely discoloration. Usually, discoloration spreads all over your teeth. Localized discoloration could be caused by habits that stain teeth in a particular area, like smoking. Or staining might collect because a tooth is damaged: chipped or cracked enamel collects stains."
    }
  },{
    "@type": "Question",
    "name": "Does a dead tooth smell?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Yes, a dead or decaying tooth may emit a foul odor due to the bacteria growing in and around the tooth. Halitosis, also known as bad breath, is usually the result of excessive bacteria in the mouth. While bacteria mainly cause tooth decay, they release sulfur compounds that cause the bad breath. Bad breath and a bad taste are common signs of tooth decay and a dead tooth."
    }
  },{
    "@type": "Question",
    "name": "How long does it take for a dead tooth to change color?",
    "acceptedAnswer": {
      "@type": "Answer",
      "text": "Change of color is one of the first signs of a dying tooth. It occurs almost immediately after the tooth starts to die. The color change can be yellow, brown, grey or black. The tooth usually stands out from the rest of the teeth. The discoloration gets worse as the death of the tooth progresses."
    }
  }]
}
</script>
<style>
             .slick-slider {
	position: relative;
	display: block;
	-moz-box-sizing: border-box;
	box-sizing: border-box;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
	-webkit-touch-callout: none;
	-khtml-user-select: none;
	-ms-touch-action: pan-y;
	touch-action: pan-y;
	-webkit-tap-highlight-color: transparent;
}

.slick-list {
	position: relative;
	display: block;
	overflow: hidden;
	margin: 0;
	padding: 0;
}

.slick-list:focus {
	outline: none;
}

.slick-list.dragging {
	cursor: pointer;
	cursor: hand;
}

.slick-slider .slick-track,
.slick-slider .slick-list {
	-webkit-transform: translate3d(0, 0, 0);
	-moz-transform: translate3d(0, 0, 0);
	-ms-transform: translate3d(0, 0, 0);
	-o-transform: translate3d(0, 0, 0);
	transform: translate3d(0, 0, 0);
}

.slick-track {
	position: relative;
	top: 0;
	left: 0;
	display: block;
}

.slick-track:before,
.slick-track:after {
	display: table;
	content: '';
}

.slick-track:after {
	clear: both;
}

.slick-loading .slick-track {
	visibility: hidden;
}

.slick-slide {
	display: none;
	float: left;
	height: 100%;
	min-height: 1px;
}

[dir='rtl'] .slick-slide {
	float: right;
}

.slick-slide img {
	display: block;
	width: 100%;
}

.slick-slide.slick-loading img {
	display: none;
}

.slick-slide.dragging img {
	pointer-events: none;
}

.slick-initialized .slick-slide {
	display: block;
}

.slick-loading .slick-slide {
	visibility: hidden;
}

.slick-vertical .slick-slide {
	display: block;
	height: auto;
	border: 1px solid transparent;
}


.top-homepage-iframe{ width:"560px"}
    
@media(max-width: 480px){
.top-homepage-iframe{
    max-width:100%;
}
}
    
@media(min-width:480px){
.top-homepage-iframe{
    max-width:100%;
}
}


</style>
        


  </head>
 
