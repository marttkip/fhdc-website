<?php

$contacts = $this->site_model->get_contacts();

  if(count($contacts) > 0)
  {
    $email = $contacts['email'];
    $phone = $contacts['phone'];
    $facebook = $contacts['facebook'];
    $twitter = $contacts['twitter'];
    $linkedin = $contacts['linkedin'];
    $instagram = $contacts['instagram'];
    $logo = $contacts['logo'];
    $company_name = $contacts['company_name'];
    $about = $contacts['about'];
    $address = $contacts['address'];
    $city = $contacts['city'];
    $post_code = $contacts['post_code'];
    $building = $contacts['building'];
    $floor = $contacts['floor'];
    $location = $contacts['location'];
    $working_weekend = $contacts['working_weekend'];
    $working_weekday = $contacts['working_weekday'];
    
    if(!empty($email))
    {
      $mail = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
    }
    
    // if(!empty($facebook))
    // {
    //  $facebook = '<li><a class="social_icon facebook" href="'.$facebook.'" target="_blank"></a></li>';
    // }
    
    // if(!empty($twitter))
    // {
    //  $twitter = '<li><a class="social_icon twitter" href="'.$twitter.'" target="_blank"></li>';
    // }
    
    // if(!empty($linkedin))
    // {
    //  $linkedin = '<li><a class="social_icon googleplus" href="'.$linkedin.'" target="_blank"></li>';
    // }
  }
  else
  {
    $email = '';
    $facebook = '';
    $twitter = '';
    $linkedin = '';
    $logo = '';
    $instagram = '';
    $company_name = '';
  }
$popular_query = $this->blog_model->get_popular_posts();

if($popular_query->num_rows() > 0)
{
    $popular_posts = '';
    $count = 0;
    foreach ($popular_query->result() as $row)
    {
        $count++;
        
        if($count < 3)
        {
            $post_id = $row->post_id;
            $post_title = $row->post_title;
            $image = base_url().'assets/images/posts/thumbnail_'.$row->post_image;
            $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
            $description = $row->post_content;
            $mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $description), 0, 10)));
            $created = date('jS M Y',strtotime($row->created));
            
            $popular_posts .= '

                                <div class="footer-post d-flex">
                                    <div class="footer-post-photo"><img src="'.$image.'" alt="" class="img-fluid"></div>
                                    <div class="footer-post-text">
                                        <div class="footer-post-title"><a href="'.site_url().'blog/view-single/'.$post_id.'">'.$mini_desc.'.</a>
                                        </div>
                                        <p>'.$created.'</p>
                                    </div>
                                </div>
                
            ';
        }
    }
}

else
{
    $popular_posts = 'There are no posts yet';
}


  $branches_rs = $this->site_model->get_all_branches();

    $branches_list = '';
    if($branches_rs->num_rows() > 0)
    {
        foreach ($branches_rs->result() as $key => $value) {
            # code...
            $branch_name = $value->branch_name;
            $branch_email = $value->branch_email;
            $branch_phone = $value->branch_phone;
            $branch_address = $value->branch_address;
            // $branch_postal_code = $value->branch_postal_code;
            $branch_location = $value->branch_location;
            $branch_building = $value->branch_building;
            $branch_floor = $value->branch_floor;
            $location_link = $value->location_link;
            
          $branches_list .= ' <div class="col-md-4">
                                    <h5>'.$branch_name.'</h5>
                                    <p style="line-height: 10px !important;margin-bottom: 24px !important;"><i class="fa fa-map-marker"></i> '.$branch_location.' '.$branch_building.' '.$branch_floor.'
                                    </p>
                                    <p style="line-height: 10px !important;margin-bottom: 24px !important;">
                                        <a href="'.site_url().'our-branches" class="btn btn-xs btn-gradient"><i class="fa fa-map"></i><span>Get directions on the map</span> <i class="fa fa-right-arrow"></i></a>
                                    </p>
                                    <p style="line-height: 10px !important;margin-bottom: 24px !important;"><i class="fa fa-phone"></i><b><span class="phone"><span class="text-nowrap"> '.$branch_phone.'</span></b></p>
                                    <p style="line-height: 10px !important;margin-bottom: 24px !important;"><i class="fa fa-envelope"></i><a href="mailto:<?php echo $email?>"> '.$branch_email.'</a></p>
                                    
                                </div>';
    }
  }



?>

<div class="footer mt-0">
        <div class="container">
            <div class="row py-1 py-md-2 px-lg-0">
                <div class="col-md-4 footer-col1">
                    <div class="row flex-column flex-md-row flex-lg-column">
                        <div class="col-md col-lg-auto">
                            <div class="footer-logo">
                                <img src="<?php echo base_url().'assets/logo/'.$logo?>" alt="" class="img-fluid">
                            </div>
                            <div class="mt-2 mt-lg-0"></div>
                            <div class="footer-social d-none d-md-block d-lg-none">
                                <a href="<?php echo $facebook?>" target="blank" class="hovicon"><i class="fa fa-facebook"></i></a>
                                <a href="<?php echo $twitter?>" target="blank" class="hovicon"><i class="fa fa-twitter"></i></a>
                                <a href="<?php echo $instagram?>" target="blank" class="hovicon"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                        <div class="col-md">
                           <!--  <div class="footer-text mt-1 mt-lg-2">
                                <p>To receive email releases, simply provide
                                    <br>us with your email below</p>
                                <form action="#" class="footer-subscribe">
                                    <div class="input-group">
                                        <input name="subscribe_mail" type="text" class="form-control" placeholder="Your Email" />
                                        <span><i class="icon-black-envelope"></i></span>
                                    </div>
                                </form>
                            </div> -->
                            <div class="footer-social d-md-none d-lg-block">
                                <a href="<?php echo $facebook?>" target="blank" class="hovicon"><i class="fa fa-facebook"></i></a>
                                <a href="<?php echo $twitter?>" target="blank" class="hovicon"><i class="fa fa-twitter"></i></a>
                                <a href="<?php echo $instagram?>" target="blank" class="hovicon"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php echo $branches_list?>
               
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row text-center text-md-left">
                    <div class="col-sm">Copyright © <?php echo date('Y')?> <a href="<?php echo site_url().'home'?>"><?php echo $company_name?></a><span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</span>
                        <a href="<?php echo site_url().'privacy-policy'?>">Privacy Policy</a></div>
                    <div class="col-sm-auto ml-auto"><span class="d-none d-sm-inline">For emergency cases&nbsp;&nbsp;&nbsp;</span><i class="fa fa-phone"></i>&nbsp;&nbsp;<b><?php echo $phone?></b></div>
                    
            </div>
        </div>
    </div>
    <!--//footer-->

    <div class="backToTop js-backToTop">
        <i class="fa fa-arrow-up"></i>                             
    </div>
    <div class="modal modal-form modal-form-sm fade" id="modalQuestionForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <button aria-label='Close' class='close' data-dismiss='modal'>
                    <i class="icon-error"></i>
                </button>
                <div class="modal-body">
                    <div class="modal-form">
                        <h3>Ask a Question</h3>
                        <form class="mt-15" id="questionForm" method="post" action-xhr="" novalidate>
                            <div class="successform">
                                <p>Your message was sent successfully!</p>
                            </div>
                            <div class="errorform">
                                <p>Something went wrong, try refreshing and submitting the form again.</p>
                            </div>
                            <div class="input-group">
                                <span>
                                <i class="icon-user"></i>
                            </span>
                                <input type="text" name="name" class="form-control" autocomplete="off" placeholder="Your Name*" />
                            </div>
                            <div class="input-group">
                                <span>
                                    <i class="icon-email2"></i>
                                </span>
                                <input type="text" name="email" class="form-control" autocomplete="off" placeholder="Your Email*" />
                            </div>
                            <div class="input-group">
                                <span>
                                    <i class="icon-smartphone"></i>
                                </span>
                                <input type="text" name="phone" class="form-control" autocomplete="off" placeholder="Your Phone" />
                            </div>
                            <textarea name="message" class="form-control" placeholder="Your comment*"></textarea>
                            <div class="text-right mt-2">
                                <button type="submit" class="btn btn-sm btn-hover-fill">Ask Now</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal modal-form fade" id="modalBookingForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <button aria-label='Close' class='close' data-dismiss='modal'>
                    <i class="icon-error"></i>
                </button>
                <div class="modal-body">
                    <div class="modal-form">
                        <h3>Book an Appointment</h3>
                        <form class="mt-15" id="bookingForm" method="post" action-xhr="" novalidate>
                            <div class="successform">
                                <p>Your message was sent successfully!</p>
                            </div>
                            <div class="errorform">
                                <p>Something went wrong, try refreshing and submitting the form again.</p>
                            </div>
                            <div class="input-group">
                                <span>
                                <i class="icon-user"></i>
                            </span>
                                <input type="text" name="bookingname" class="form-control" autocomplete="off" placeholder="Your Name*" />
                            </div>
                            <div class="row row-xs-space mt-1">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <span>
                                            <i class="icon-email2"></i>
                                        </span>
                                        <input type="text" name="bookingemail" class="form-control" autocomplete="off" placeholder="Your Email*" />
                                    </div>
                                </div>
                                <div class="col-sm-6 mt-1 mt-sm-0">
                                    <div class="input-group">
                                        <span>
                                            <i class="icon-smartphone"></i>
                                        </span>
                                        <input type="text" name="bookingphone" class="form-control" autocomplete="off" placeholder="Your Phone" />
                                    </div>
                                </div>
                            </div>
                            <div class="row row-xs-space mt-1">
                                <div class="col-sm-6">
                                    <div class="input-group">
                                        <span>
                                            <i class="icon-birthday"></i>
                                        </span>
                                        <input type="text" name="bookingage" class="form-control" autocomplete="off" placeholder="Your age" />
                                    </div>
                                </div>
                            </div>
                            <div class="selectWrapper input-group mt-1">
                                <span>
                                    <i class="icon-tooth"></i>
                                </span>
                                <select name="bookingservice" class="form-control">
                                    <option selected="selected" disabled="disabled">Select Service</option>
                                    <option value="Cosmetic Dentistry">Cosmetic Dentistry</option>
                                    <option value="General Dentistry">General Dentistry</option>
                                    <option value="Orthodontics">Orthodontics</option>
                                    <option value="Children`s Dentistry">Children`s Dentistry</option>
                                    <option value="Dental Implants">Dental Implants</option>
                                    <option value="Dental Emergency">Dental Emergency</option>
                                </select>
                            </div>
                            <div class="input-group flex-nowrap mt-1">
                                <span>
                                    <i class="icon-calendar2"></i>
                                </span>
                                <div class="datepicker-wrap">
                                    <input name="bookingdate" type="text" class="form-control datetimepicker" placeholder="Date" readonly>
                                </div>
                            </div>
                            <div class="input-group flex-nowrap mt-1">
                                <span>
                                    <i class="icon-clock"></i>
                                </span>
                                <div class="datepicker-wrap">
                                    <input name="bookingtime" type="text" class="form-control timepicker" placeholder="Time">
                                </div>
                            </div>
                            <textarea name="bookingmessage" class="form-control" placeholder="Your comment"></textarea>
                            <div class="text-right mt-2">
                                <button type="submit" class="btn btn-sm btn-hover-fill">Book now</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    