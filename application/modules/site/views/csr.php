<?php
  $about_query = $this->site_model->get_active_items('Corporate Social Responsibility');

  // var_dump($about_query);die();
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $gallery_rs = $this->site_model->get_post_gallery($post_id);

      $gallery = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
          $post_gallery_image_thumb = $value->post_gallery_image_thumb;

          // $gallery .= '<li><img src="'.$post_gallery_image_name.'" alt=""></li>';
          $gallery .= ' <div class="col"><span class="gallery-popover-link" data-full="'.$post_gallery_image_name.'"><img src="'.$post_gallery_image_name.'" alt="" class="img-fluid"></span></div>';
        }
      }

      $about_company .= '<div class="container">
					        <div class="row">
					          <div class="col-lg-4">
					            <div class="title-wrap">
					              <h2 class="h1">'.$post_title.' </h2>
					              <div class="h-decor"></div>
					            </div>

					            

					            '.$description.'
					          </div>
					          <div class="col-lg-8 mt-4 mt-lg-0">
					            	<div class="sm-gallery-row row no-gutters mx-lg-15">
                                            '.$gallery.'
                                    </div>
					          </div>
					        </div>
					      </div>';

    }
  }
  ?>

<?php echo $this->load->view("site/quick_links", '');?>  

<div class="page-content mb-20">

        <!--section-->
    <div class="section mt-0">
      <div class="breadcrumbs-wrap">
        <div class="container">
          <div class="breadcrumbs">
            <a href="<?php echo site_url().'home'?>">Home</a>
            <span>CSR</span>
          </div>
        </div>
      </div>
    </div>

    <div class="section mb-100 mt-5">
        <div class="container">
        <div class="text-center mb-2  mb-md-3 mb-lg-4">
          <!-- <div class="h-sub theme-color">Where to find us</div> -->
          <h1>OUR IMPACT IN SOCIETY</h1>
          <div class="h-decor"></div>
        </div>
      </div>
      
      <?php echo $about_company;?>

      
    </div>

    <?php echo $this->load->view("site/our_partners", '');?>  
    <?php //echo $this->load->view("site/tag_line", '');?>  
</div>
