<?php

$blog_category_id = $this->site_model->get_category_id('Company Services');

$category_items = $this->site_model->get_active_post_content_by_category($blog_category_id,4);

$main_services = '';
$mobile =='';
if($category_items->num_rows() > 0)
{
    $x= 0;
    foreach ($category_items->result() as $key => $row) 
    {
        $post_title = $row->post_title;
        $post_content = $row->post_content;
        $post_id = $row->post_id;
        $web_name = $this->site_model->create_web_name($post_title);
        $post_header = base_url().'assets/images/posts/'.$row->post_header;
        $mini_desc = implode(' ', array_slice(explode(' ', $post_content), 0, 50));
        
        $link = 'col-4';
        $link_other = '';

         $x++;
        if ($x == 1 OR $x == 4) {
            $link = 'col-4';
            $content = '<div class="service-box-rotator js-service-box-rotator">
                            <div class="service-box service-box-greybg service-box--hiddenbtn">
                                <div class="service-box-caption text-center">
                                    <div class="service-box-icon"><i class="fa fa-home"></i></div>
                                    <div class="service-box-icon-bg"><i class="fa fa-home"></i></div>
                                    <h3 class="service-box-title">'.$post_title.'</h3>
                                    <p>'.$mini_desc.'</p>
                                    <div class="btn-wrap"><a href="'.site_url().'view-service-category/'.$web_name.'" class="btn"><i class="fa fa-angle-right"></i><span>Know more</span><i class="fa fa-angle-right"></i></a></div>
                                </div>
                            </div>
                           
                        </div>';
        }
        else if ($x == 2) {

            $link = 'col-8';
            $content = '<div class="service-box">
                            <div class="service-box-image" data-bg="'.$post_header.'"></div>
                            <div class="service-box-caption text-center w-50 ml-auto">
                                <h3 class="service-box-title">'.$post_title.'</h3>
                                <p>'.$mini_desc.'</p>
                                <div class="btn-wrap"><a href="'.site_url().'view-service-category/'.$web_name.'" class="btn"><i class="fa fa-angle-right"></i><span>Know more</span><i class="fa fa-angle-right"></i></a></div>
                            </div>
                        </div>';
        }
        else if ($x == 3) {

            $link = 'col-8';
            $link_other = 'order-lg-4 order-xl-3';
            $content = '<div class="service-box">
                            <div class="service-box-image" data-bg="'.$post_header.'"></div>
                            <div class="service-box-caption text-center w-50 ml-auto">
                                <h3 class="service-box-title">'.$post_title.'</h3>
                                <p>'.$mini_desc.'</p>
                                <div class="btn-wrap"><a href="'.site_url().'view-service-category/'.$web_name.'" class="btn"><i class="fa fa-angle-right"></i><span>Know more</span><i class="fa fa-angle-right"></i></a></div>
                            </div>
                        </div>';
         }
         else
         {
            $content = '<div class="service-box-rotator js-service-box-rotator">
                            <div class="service-box service-box-greybg service-box--hiddenbtn">
                                <div class="service-box-caption text-center">
                                    <div class="service-box-icon"><i class="fa fa-home"></i></div>
                                    <div class="service-box-icon-bg"><i class="fa fa-home"></i></div>
                                    <h3 class="service-box-title">'.$post_title.'</h3>
                                    <p>'.$mini_desc.'</p>
                                    <div class="btn-wrap"><a href="'.site_url().'view-service-category/'.$web_name.'" class="btn"><i class="fa fa-angle-right"></i><span>Know more</span><i class="fa fa-angle-right"></i></a></div>
                                </div>
                            </div>
                           
                        </div>';
         }

         $mobile .= '<div class="service-box service-box-greybg service-box--hiddenbtn">
                        <div class="service-box-caption text-center">
                            <div class="service-box-icon"><i class="fa fa-home"></i></div>
                            <div class="service-box-icon-bg"><i class="fa fa-home"></i></div>
                            <h3 class="service-box-title">'.$post_title.'</h3>
                            <<p>'.$mini_desc.'</p>
                            <div class="btn-wrap"><a href="'.site_url().'view-service-category/'.$web_name.'" class="btn"><i class="fa fa-angle-right"></i><span>Know more</span><i class="fa fa-angle-right"></i></a></div>
                        </div>
                    </div>';

        $main_services .= '<div class="'.$link.' order-'.$x.' '.$link_other.'">
                               '.$content.' 
                            </div>';
       
    }
}

?>
<div class="section">
    <div class="container-fluid px-0">
        <div class="title-wrap text-center">
            <div class="h-sub theme-color">What we offer</div>
            <h2 class="h1">General Services</h2>
            <div class="h-decor"></div>
        </div>
        <div class="row no-gutters services-box-wrap services-box-wrap-desktop">
            <?php echo $main_services;?>
            
        </div>

        <div class="row no-gutters services-box-wrap services-box-wrap-mobile">
            <div class="service-box-rotator js-service-box-rotator">
                <?php echo $mobile;?>
            </div>
        </div>
    </div>
</div>