<?php

$about_query = $this->site_model->get_active_items('Privacy Policy');
 $faqs_list = '';

 // var_dump($about_query);die();
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = $row->post_content;
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $x++;
    // if($x < 9)
    // {
    //   $x = '0'.$x;
    // }
    // $faqs_list .= '
				// 	<div class="faq-item">
    //                         <a data-toggle="collapse" data-parent="#faqAccordion1" href="#" aria-expanded="true"><span>'.$x.'.</span><span>'.$post_title.'</span></a>
    //                         <div id="faqItem1" class="collapse show faq-item-content" role="tabpanel">
    //                             <div>
    //                                '.$description.'
    //                             </div>
    //                         </div>
    //                     </div>';
  }
}
?>


<div class="page-content">
		<!--section-->
		<div class="section mt-0">
			<div class="breadcrumbs-wrap">
				<div class="container">
					<div class="breadcrumbs">
						<a href="<?php echo site_url().'home'?>">Home</a>
						<span>Privacy Policy</span>
				
					</div>
				</div>
			</div>
		</div>
		<!--//section-->
		<!--section-->
		<div class="section page-content-first">
			<div class="container mt-6">
				<div class="row">
					
					<div class="col-md-12 col-lg-12 mt-4 mt-md-0">
						<div class="title-wrap">
						<h1><?php echo $title;?></h1></div>
					
						<div class="pt-2 pt-md-4">
							
							<?php echo $description?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--//section-->
	</div>