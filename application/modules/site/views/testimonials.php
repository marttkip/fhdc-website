<?php
$about_query = $this->site_model->get_active_items('Company Testimonials');
 $testimonials_list = '';

 // var_dump($about_query);die();
if($about_query->num_rows() > 0)
{
  $x=0;
  foreach($about_query->result() as $row)
  {
    $about_title = $row->post_title;
    $post_id = $row->post_id;
    $blog_category_name = $row->blog_category_name;
    $blog_category_id = $row->blog_category_id;
    $post_title = $row->post_title;
    $web_name = $this->site_model->create_web_name($post_title);
    $post_status = $row->post_status;
    $post_views = $row->post_views;
    $image_about = base_url().'assets/images/posts/'.$row->post_image;
    $created_by = $row->created_by;
    $modified_by = $row->modified_by;
    $post_target = $row->post_target;
    $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
    $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
    $description = strip_tags($row->post_content);
    $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
    $created = $row->created;
    $day = date('j',strtotime($created));
    $month = date('M',strtotime($created));
    $year = date('Y',strtotime($created));
    $created_on = date('jS M Y',strtotime($row->created));
    $x++;
    if($x < 9)
    {
      $x = '0'.$x;
    }
    $testimonials_list .= '
                        <div class="col-sm-6 col-md-4" style="text-align:"center";">
                            <div class="doctor-box text-center" style="height: 300px;">
                                <div class="doctor-box-photo">
                                   
                                </div>
                                <h5 class="doctor-box-name"></h5>
                                <div class="doctor-box-text" style="font-style: italic !important;">
                                    <p style="font-size:19px;">"'.$description.'" </p>
                                    <p> <strong>'.$post_title.' </strong> </p>
                                </div>
                                
                            </div>
                        </div>';
  }
}
?>

<div class="section">
    <div class="container">
        <div class="title-wrap text-center">
           
            <div class="h-decor"></div>
        </div>
        <div class="row specialist-carousel js-specialist-carousel">
            <?php echo $testimonials_list?>
            
        </div>
    </div>
</div>
