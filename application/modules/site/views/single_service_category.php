<?php
    $company_details = $this->site_model->get_contacts();
    
    if(count($company_details) > 0)
    {
        $email = $company_details['email'];
        $email2 = $company_details['email'];
        $facebook = $company_details['facebook'];
        $twitter = $company_details['twitter'];
        $linkedin = '';// $company_details['linkedin'];
        $logo = $company_details['logo'];
        $company_name = $company_details['company_name'];
        $phone = $company_details['phone']; 
        $mission = $company_details['mission']; 
        $about = $company_details['about'];  
        $weekday = $company_details['weekday'];  
        $weekend = $company_details['weekend'];     
    
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
        $mission = '';
        $weekend = '';
        $weekday ='';
    }

// var_dump($email);die();
?>
<?php
  $about_query = $this->site_model->get_active_content_items($title,1);
   $about_company = '';
  if($about_query->num_rows() == 1)
  {
    $x=0;
    foreach($about_query->result() as $row)
    {
      $about_title = $row->post_title;
      $post_id = $row->post_id;
      $blog_category_name = $row->blog_category_name;
      // $blog_category_id = $row->blog_category_id;
      $post_title = $row->post_title;
      $web_name = $this->site_model->create_web_name($post_title);
      $post_status = $row->post_status;
      $post_views = $row->post_views;
      $image_about = base_url().'assets/images/posts/'.$row->post_image;
      $created_by = $row->created_by;
      $modified_by = $row->modified_by;
      $post_target = $row->post_target;
      $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
      $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
      $description = $row->post_content;
      $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
      $created = $row->created;
      $day = date('j',strtotime($created));
      $month = date('M',strtotime($created));
      $year = date('Y',strtotime($created));
      $created_on = date('jS M Y',strtotime($row->created));
      $x++;
      if($x < 9)
      {
        $x = '0'.$x;
      }

      $gallery_rs = $this->site_model->get_post_gallery($post_id);

      $gallery = '';
      if($gallery_rs->num_rows() > 0)
      {
        foreach ($gallery_rs->result() as $key => $value) {
          # code...
          $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
          $post_gallery_image_thumb = $value->post_gallery_image_thumb;

          $gallery .= ' <li>
                              <div class="portfolio-thumb">
                                  <div class="gc_filter_cont_overlay_wrapper port_uper_div">
                                      <img src="'.$post_gallery_image_name.'" class="zoom image img-responsive" alt="service_img" />
                                      <div class="gc_filter_cont_overlay zoom_popup">
                                          <div class="gc_filter_text"><a href="'.$post_gallery_image_name.'"><i class="fa fa-plus"></i></a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </li>';
        }
      }

    }
  }
   
  $blog_category_id = $this->site_model->get_category_id($post_title);

  $category_items = $this->site_model->get_active_post_content_by_category($blog_category_id);
  $services_sub_menu_services = '';
  // var_dump($category_items);die();

  if($category_items->num_rows() > 0)
  {
    
   
    foreach ($category_items->result() as $key => $value) {
      # code...

      $post_title_view = $value->post_title;
      $web_name = $this->site_model->create_web_name($post_title_view);
      $image_about_view = base_url().'assets/images/posts/'.$value->post_image;
      $child_description = $value->post_content;
      $mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $child_description), 0, 50)));

      $services_sub_menu_services .= ' <div class="col-md-6 col-lg-6">
                                        <div class="service-card">
                                          <div class="service-card-photo">
                                            <a href="'.site_url().'view-service/'.$web_name.'"><img src="'.$image_about_view.'" class="img-fluid" alt=""></a>
                                          </div>
                                          <h5 class="service-card-name"><a href="'.site_url().'view-service/'.$web_name.'">'.$post_title_view.'</a></h5>
                                          <div class="h-decor"></div>
                                          <p>'.$mini_desc.'</p>
                                          
                                        
                                        </div>
                                      </div>';
    }
   
    // var_dump($services_sub_menu_services);die();
  }
  else
  {
    // put that category here
  }


                                     // <h1><a href="
// var_dump($blog_category_id);die();

  // blog services 

  ?>

<?php echo $this->load->view("site/quick_links", '');?>  

<div class="page-content">
    <!--section-->
    <div class="section mt-0">
      <div class="breadcrumbs-wrap">
        <div class="container">
          <div class="breadcrumbs">
            <a href="<?php echo site_url().'home'?>">Home</a>
            <a href="<?php echo site_url().'services'?>">Services</a>
            <span><?php echo $post_title;?></span>
          </div>
        </div>
      </div>
    </div>
    <!--//section-->
    <!--section-->
    <div class="section page-content-first">
      <div class="container">
        <div class="text-center mb-2  mb-md-3 mb-lg-4">
          <!-- <div class="h-sub theme-color">Our dentists working to your smile</div> -->
          <h1><?php echo $post_title;?></h1>
          <div class="h-decor"></div>
        </div>
      </div>
       <div class="container mt-5 mb-6">
        <div class="row">
           
          <div class="col-lg-6 text-center text-lg-left pr-md-4">
            <div class="service-img">
                  <img src="<?php echo $image_about?>" class="img-fluid" alt="">
                </div>
          </div>
          <div class="col-lg-6 mt-3 mt-lg-0">
            <?php echo $description?>
            
          </div>
        </div>
      </div>
      
    </div>
    <!--//section-->
  </div>
<!-- Content -->

      
                 
                  
  





    