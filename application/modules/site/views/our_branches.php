<?php 

$branches_rs = $this->site_model->get_all_branches();

$branches_list = '';
$count =0;
if($branches_rs->num_rows() > 0)
{
    foreach ($branches_rs->result() as $key => $value) {
        # code...
        $branch_name = $value->branch_name;
        $branch_email = $value->branch_email;
        $branch_phone = $value->branch_phone;
        $branch_address = $value->branch_address;
        $branch_code = $value->branch_code;
        $branch_location = $value->branch_location;
        $branch_building = $value->branch_building;
        $branch_floor = $value->branch_floor;
        $location_link = $value->location_link;

        $testimonials_image_about = base_url().'assets/logo/'.$value->branch_image_name;
        $gallery_rs = $this->site_model->get_branch_gallery_list($branch_code);
         // var_dump($gallery_rs);die();
        $gallery = '';
        if($gallery_rs->num_rows() > 0)
        {
            foreach ($gallery_rs->result() as $key => $value) {
              # code...
              $gallery_image_name = base_url().'assets/gallery/'.$value->gallery_image_name;
              $gallery_image_thumb = $value->gallery_image_thumb;

              $gallery .= ' <div class="col"><span class="gallery-popover-link" data-full="'.$gallery_image_name.'"><img src="'.$gallery_image_name.'" alt="" class="img-fluid"></span></div>';


            }

             // var_dump($gallery_image_name);die();
        }

        $branches_list .='<div class="row">
                              <div class="col-md-4">
                                <div class="doctor-page-photo text-center">
                                  <img src="'.$testimonials_image_about.'" class="img-fluid" alt="">
                                </div>
                                <div class="mt-3 mt-md-5"></div>
                                <table class="table doctor-page-table">
                                  <tr>
                                    <td><i class="fa fa-phone"></i></td>
                                    <td> '.$branch_phone.'</td>
                                  </tr>
                                  <tr>
                                    <td><i class="fa fa-envelope"></i></td>
                                    <td>'.$branch_email.'</td>
                                  </tr>
                                  <tr>
                                    <td><i class="fa fa-home"></i></td>
                                    <td>
                                      <div class="marker-list-md">
                                        <span>Location: '.$branch_location.'</span><br>
                                        <span>Building: '.$branch_building.' '.$branch_floor.' </span>
                                      </div>
                                    </td>
                                  </tr>
                                  
                                </table>
                              </div>
                              <div class="col-md-8 mt-4 mt-lg-0">
                                <div class="doctor-info mb-3 mb-lg-4">
                                  <div class="doctor-info-name">
                                    <h3>'.$branch_name.'</h3>
                                    
                                  </div>
                                  <div class="doctor-info-phone"><i class="fa fa-phone"></i><a href="tel:'.$branch_phone.'"> '.$branch_phone.'</a></div>
                                  <div class="doctor-info-social">
                                    <a href="" target="blank" class="hovicon"><i class="fa fa-facebook"></i></a>
                                    <a href="mailto:'.$branch_email.'" class="hovicon"><i class="fa fa-envelope"></i></a>

                                  </div>
                                </div>
                                <div>
                                    
                                    <iframe src="'.$location_link.'" class="google-map" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" width="100%" height="350px;"></iframe>
                                    
                                </div>
                                
                                <div class="mt-4 mt-lg-6"></div>
                                
                                
                               
                              </div>
                            </div>';
    }
}
?>
<?php echo $this->load->view("site/quick_links", '');?>  
<div class="page-content">
    <!--section-->
    <div class="section mt-0">
      <div class="breadcrumbs-wrap">
        <div class="container">
          <div class="breadcrumbs">
            <a href="<?php echo site_url().'home'?>">Home</a>
            <span>Our Branches</span>
          </div>
        </div>
      </div>
    </div>
    <!--//section-->
    <!--section-->
    <div class="section">
      <div class="container mt-6">
        <?php echo $branches_list;?>
      </div>
    </div>

    <?php echo $this->load->view("site/our_partners", '');?>  
    <?php //echo $this->load->view("site/tag_line", '');?>  
    
  </div>