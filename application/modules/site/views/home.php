<?php
    $company_details = $this->site_model->get_contacts();
    
    if(count($company_details) > 0)
    {
        $email = $company_details['email'];
        $email2 = $company_details['email'];
        $facebook = $company_details['facebook'];
        $twitter = $company_details['twitter'];
        $linkedin = '';// $company_details['linkedin'];
        $logo = $company_details['logo'];
        $company_name = $company_details['company_name'];
        $phone = $company_details['phone']; 
        $mission = $company_details['mission']; 
        $about = $company_details['about'];     
    
    }
    else
    {
        $email = '';
        $facebook = '';
        $twitter = '';
        $linkedin = '';
        $logo = '';
        $company_name = '';
        $google = '';
        $mission = '';
    }


?>

</div>
<div class="page-content">
<!--section slider-->
<div class="section mt-0">
     <?php echo $this->load->view("site/quick_links", '');?>  
     
    
     <!--<?php echo $this->load->view("site/home/slider", '');?>  -->
    
    
</div>
        <div class="section">
            <div class="container pt-lg-2">
                <div class="title-wrap text-center text-md-left d-lg-none mb-lg-2">
                    <div class="h-sub">15+ Years of Dental Excellence</div>
                    <h1 class="h1">An Affordable Dental Clinic in Nairobi- <span class="theme-color">Family Health Dental Clinic</span></h1>
                    <p>Licensed by the <a style="color:blue;" href="http://kmpdc.go.ke/Registers/H-Facilities.php">Kenya Medical Practitioners and Dentists Board</a></p>
                </div>
                <div class="row mt-2 mt-md-3 mt-lg-0">
                    <div class="col-md-6">
                        <div class="title-wrap hidden d-none d-lg-block text-center text-md-left">
                            <div class="h-sub">15+ Years of Dental Excellence</div>
                            <h1 class="h1 theme-color" >An Affordable Dental Clinic in Nairobi</span></h1>
                        </div>
                        <div> 
                        <br>
                            <iframe class="top-homepage-iframe" src="https://www.facebook.com/plugins/video.php?height=314&href=https%3A%2F%2Fweb.facebook.com%2FFamilyHealthDentalClinic%2Fvideos%2F4708635102548874%2F&show_text=false&width=560&t=0" height="314" width="560" style="border:none;overflow:hidden;" scrolling="no" frameborder="0" allowfullscreen="true" allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share" allowFullScreen="true" title="A Visit to Family Health Dental Clinic Showing Facilities, Doctors and Patients"></iframe>
                        <br>
                        <p>
                        We are an <strong>Affordable Dental Clinic in Nairobi with over 15 years of dental excellence offering specialized Dental Care</strong> in the following areas:
                        
                        <ol>
                        
                        <li><a style="font-weight:bold; color:blue; text-decoration: underline;" href="#dental-consultation-and-examination">Dental Consultation and Examination</a></li>
                        
                        <li><a style="font-weight:bold; color:blue; text-decoration: underline;" href="#dental-fillings-and-root-canal">Dental Fillings and Root Canal Treatment</a></li>
                        
                        <li><a style="font-weight:bold; color:blue; text-decoration: underline;" href="#dental-xrays">Dental X-rays</a></li>
                        
                        <li><a style="font-weight:bold; color:blue; text-decoration: underline;" href="#tooth-extraction">Teeth Extractions</a></li>
                        
                        <li><a style="font-weight:bold; color:blue; text-decoration: underline;" href="#full-mouth-scaling">Full Mouth Scaling</a></li>
                        
                        <li><a style="font-weight:bold; color:blue; text-decoration: underline;" href="#minor-oral-surgery">Minor Oral Surgery</a></li>
                        
                        <li><a style="font-weight:bold; color:blue; text-decoration: underline;" href="#paediatric-dentistry">Paediatric Dentistry</a></li>
                        
                        <li><a style="font-weight:bold; color:blue; text-decoration: underline;" href="#teeth-alignment">Teeth Alignment</a></li>
                        
                        <li><a style="font-weight:bold; color:blue; text-decoration: underline;" href="#teeth-replacements">Teeth Replacements: Dental Implants, Crowns and Bridges</a></li>
                        
                        <li><a style="font-weight:bold; color:blue; text-decoration: underline;" href="#teeth-whitening">Teeth Whitening</a></li>
                        
                        </ol>
                        </p>

                            
                            <!--<?php echo $about?>-->
                        </div>
                        <div>
                            <h2 style="color:#DB5C20;">Affordable Dental Services in Nairobi</h2>
                            <p  style="color:"black";"> If you are looking for an <strong>affordable dental clinic in Nairobi</strong>, look no further. <strong>Our dental services are very affordable. We accept insurance and NHIF (subject to confirmation from the <a style="color:blue; text-decoration:underline;" href="https://nhifcare.co.ke/landing/">NHIF website</a>). For procedures that cost above Sh. 30,000, we have flexible payment plans.</strong> </p>
                            <p>We are located on the 3rd Floor of TMall along Langata Road and in the Point Mall along Rabai Road in Buruburu but, <strong>being a Nairobi Dental Clinic, we serve the whole of Nairobi.</strong></p>
                            <h2 style="color:#DB5C20;">Our Services</h2>
                            <br>
                            <div class="fixed-bottom">
                                <a style="margin-bottom:10px; background-color: blue; color: white;" href="#book-appointment" class="btn btn-primary" data-tpggle="button">Book Appointment</a>
                            </div>
                            
                            <P>As a highly sought-after <strong>dental clinic in Nairobi</strong>, we stop at nothing to ensure we give quality dental services to our patients. We have highly-specialized dentists who are well-experienced and continue to deliver outstanding treatment outcomes that enocurage our patients to refer more patients to us. </P>
                            <p>The services we offer include: </p>
                            
                           
                                <p id="dental-consultation-and-examination"><a style="color:#DB5C20; font-weight:bold;" href="https://familyhealthdentalclinic.com/dental-consultation-and-examination">Dental Consultation and Examination</a>
                                <br>
                                    
                                    <img loading="lazy" style="max-width:70%; height:100;"  src="https://familyhealthdentalclinic.com/assets/images/posts/2a071f7d56eaac39dd399b9420805502.JPG" alt="Dr. Kiio, one of the dentists at Family Health Dental Clinic, examining a patient"/>
                                </p>
                                <br>
                                <p>If you are looking for a <strong>dental clinic in Nairobi</strong> that will handle you with a high level of professionalism, you should definitely visit <strong>Family Health Dental Clinic</strong>.</p>
                                <p id="dental-consultation-and-examination">When you walk into Family Health Dental Clinic, you will be received at the reception desk. We will ask for your personal details and request you to sit as you await to see the dentist. You will be ushered to the examination room... <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/dental-consultation-and-examination">Continue Reading</a> </p>
                                <p>
                                <a href="https://familyhealthdentalclinic.com/Tooth-Fillings" style="color:#DB5C20; font-weight:bold;">Dental Fillings and Root Canal Treatment</a>
                                <br>
                                    
                                    <img loading="lazy" style="max-width:70%; height:100;" src="https://familyhealthdentalclinic.com/assets/images/posts/750ee6e84d91a6683d2911271b861e28.png" alt="A diagram showing the tooth filling process, from the cleaning and shaping of a decayed tooth to the placing of the tooth filling."">
                                    </p>
                                    <p>We pride ourselves on being one of the <strong>dental clinics in Nairobi</strong> that provide excellent dental fillings.
                                    <br>
                                    <p>Fillings are tooth-saving materials that are used to repair broken, cracked or tooth that have been worn down. Fillings are commonly used to restore decayed teeth. Root canal is another treatment option that is used to restore decayed teeth.Another option is dental fillings. Let us cover the comparison between dental fillings and root canal so you can understand the use cases of each... <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/Tooth-Fillings">Continue Reading</a> </p>
                                    <p id="dental-xrays">
                                <a style="color:#DB5C20; font-weight:bold;" href="https://familyhealthdentalclinic.com/dental-xrays">Dental Xrays</a>
                                <br>
                                <img loading="lazy" style="max-width:70%; height:100;" src="https://familyhealthdentalclinic.com/assets/images/posts/b44163b5e2aa6865894b4a831125abcf.jpg" alt= "A patient having an Xray of his teeeth taken."/>
                                </p>
                                <br>
                                <p>Dental radiology involves the use of x-rays to take images of the oral structures.  It is important to take these images because we can learn about the state of teeth and the surrounding structures which cannot be seen by just looking in the mouth. We can reduce many mistakes by using radiographic images. For example, if a patient requires root canal treatment, but a filling was done, the patient’s problem will not be solved, they will continue to be in pain, feel sensitivity or even develop a bone infection. ... <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/dental-xrays">Continue Reading</a> </p>
                                <p id="tooth-extraction">
                                <a style="color:#DB5C20; font-weight:bold;" href="https://familyhealthdentalclinic.com/tooth-extractions">Tooth Extraction</a>
                                <br>
                                <img loading="lazy" style="max-width:70%; height:100;" src="https://familyhealthdentalclinic.com/assets/images/posts/af305779ae32b61bdd9d15d9f63c38ba.jpg" alt="A premolar being extracted"/>
                                </p>
                                <br>
                                 <p>Tooth extraction is a procedure where the dentist completely removes a tooth from the mouth where it is attached to the jaw bone. This could be for various reasons. You may have a wisdom tooth that is impacted; a diseased tooth that can't be saved; or overcrowded teeth that need to be removed so more room can be created to facilitate proper alignment during orthodontics. ... <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/tooth-extractions">Continue Reading</a> </p>
                                <p id="minor-oral-surgery">
                                <a style="color:#DB5C20; font-weight:bold;" href="https://familyhealthdentalclinic.com/minor-oral-surgery">Minor Oral Surgery</a>
                                <br>
                                <img loading="lazy" style="max-width:70%; height:100;"  src="https://familyhealthdentalclinic.com/assets/images/posts/a9ec0b7d0cfab3faa76a0ce8b09cf625.jpg" alt="A side image of a person's face showing the inside of his mouth. One of his molars is impacted (stuck in the bone. It didn't fully erupt"/>
                                </p>
                                <br>
                                <p>There are several examples of oral surgeries that are carried out every now and then in a dental office. They include, tooth extraction, dental implants, bone grafting, periodontal surgery, cleft palate and lip repair and corrective jaw surgery. Let's look at these procedures individually. ... <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/minor-oral-surgery">Continue Reading</a> </p>
                                <p id="paediatric-dentistry">
                                <a style="color:#DB5C20; font-weight:bold;" href="https://familyhealthdentalclinic.com/paediatric-dentistry-">Paediatric Dentistry</a>
                                <br>
                                <img loading="lazy" style="max-width:70%; height:100;"  src="https://familyhealthdentalclinic.com/assets/images/posts/689711a21d949167d39e39a57e45a777.jpg" alt="A female child having her teeth examined. She is smiling."/>
                                </p>
                                <br>
                                <p><strong>Family Health Dental Clinic</strong> is one of the <strong>dental clinics in Nairobi</strong> that cater to children's oral health.</p>
                                <p>Paediatric Dentistry is a branch of dentistry that concerns children below 14 years of age (from birth up to adolescence). The development of milk teeth starts even before the birth of a baby and baby teeth begin to appear between 6-8 months of age. Some babies get their teeth early and some get them late. Usually, the first baby teeth that appear are the lower front teeth. ... <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/paediatric-dentistry-">Continue Reading</a> </p>
                                <p id="teeth-alignment">
                                <a style="color:#DB5C20; font-weight:bold;" href="https://familyhealthdentalclinic.com/tooth-alignment-braces">Teeth Alignment</a>
                                <br>
                                <img loading="lazy" style="max-width:70%; height:100;" src="https://familyhealthdentalclinic.com/assets/images/posts/4e3fafc58852a502c70ba42fdd9a482f.jpg" alt="A picture of a patient's mouth showing the dentist putting braces on his teeth"/>
                                </p>
                                <br>
                                <p>Orthodontic treatment is the original smile makeover tool — and you will be happy to know that you're never too old to take advantage of it. But it isn't all about looks. Properly aligned teeth help you  bite, chew and even speak more effectively. They are also easier to clean, which helps keep your mouth free of tooth decay and gum disease. ... <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/tooth-alignment-braces">Continue Reading</a> </p>
                                <p id="teeth-replacements">
                                <a style="color:#DB5C20; font-weight:bold;" href="https://familyhealthdentalclinic.com/tooth-replacements-implants-crowns-and-bridge">Teeth Replacements (Dental Implants, Crowns and Bridges)</a>
                                <br>
                                <img loading="lazy" style="max-width:70%; height:100;"  src="https://familyhealthdentalclinic.com/assets/images/posts/b2b789614adc3c72d38773350201a214.jpg" alt="A model of a person's mouth showing a missing tooth"/>
                                </p>
                                <p>Dental implants are used to replace missing teeth in the jaws. The reasons for losing teeth can be varied, but no matter what the cause, their loss can impact on both appearance and function. A dental bridge, on the other hand, is a fixed dental prosthesis used to replace one or several missing teeth by permanently joining an artificial tooth to adjacent teeth or dental implants. ... <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/tooth-replacements-implants-crowns-and-bridge">Continue Reading</a></p>
                                <br>
                                <p id="teeth-whitening">
                                <a style="color:#DB5C20; font-weight:bold;" href="https://familyhealthdentalclinic.com/tooth-whitening-">Teeth Whitening</a>
                                <br>
                                <img loading="lazy" style="max-width:70%; height:100;"  src="https://familyhealthdentalclinic.com/assets/images/posts/cea598f57159ca6e4fcfc39d4bc4e0cf.jpg" alt="A person's teeth split into two halves with the left half showing stained teeth while the right half shows whitened teeth."/>
                                </p>
                                <p>Being an <strong>affordable dental clinic in Nairobi</strong>, we are always commited to giving our patients the perfect smile at a very affordable cost.</p>
                                <p>If you want to obtain a bright and white smile, teeth whitening may be just what you are looking for. Not only is teeth whitening effective, but it can also drastically change your smile and improve your self-esteem at the same time. The peroxide ingredient in most teeth whitening products is what actually bleaches the enamel and makes your teeth whiter. The strength of the whitening treatment typically depends on the strength of the peroxide contained in the whitening product. The current percentages of peroxide in teeth whitening products are 10%, 16%, and 22%. ... <a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/tooth-whitening-">Continue Reading</a></p>
                                <br>
                                <p id="full-mouth-scaling">
                                <a style="color:#DB5C20; font-weight:bold;" href="https://familyhealthdentalclinic.com/full-mouth-scaling">Full Mouth Scaling</a>
                                <br>
                                <img loading="lazy" style="max-width:70%; height:100;"  src="https://familyhealthdentalclinic.com/assets/images/posts/12d82a13081d2e3c934056714da4069e.jpg" alt="A diagram showing 2 different open mouths side by side. The left picture shows a mouth with gum disease while the right picture shows a mouth with healthy gums."/>
                                <br>
                                <p>This is a non-surgical treatment that aims to remove plaque, sediments and calculus on the surfaces of teeth. Plaque is a sticky coating on the teeth, usually white in color, that is formed by a mixture of food particles and saliva. The layer is removed on brushing and flossing. Plaque hardens if it is not removed and becomes tartar, also known as dental calculus.if the plaque is not removed,  minerals like calcium and sodium found in our diet are deposited on it leading to the formation of dental calculus. ...<a style="color:blue; text-decoration:underline;" href="https://familyhealthdentalclinic.com/full-mouth-scaling">Continue Reading</a></p>
                                </p>
                                <br>
                                <div class="section">
      <div class="container">
        <div class="title-wrap text-center">
          <h2 class="h1">Our Doctors.</h2>
          <div class="h-decor"></div>
        </div>
        
  <div class="row">
  <div class="card" style="width: 18rem;">
  <img loading="lazy" style="width: 80%;" src="https://pbs.twimg.com/media/FOc7pHpXoAgIZmI?format=jpg&name=small" class="card-img-top" alt="Dr. Kiio L.M">
  <!--<img laoding="speed" style="width: 80%;" src="https://familyhealthdentalclinic.com/assets/images/posts/9ed550d0c3016af0326e3024a9e1800d.jpg" class="card-img-top" alt="Dr. Kiio L.M">-->
  <div class="card-body">
  <br>
    <h5 class="card-title">Dr. Kiio L.M</h5>
    <p class="card-text" style="padding-right: 10px;"><strong> A dedicated dental professional with over 13 years of experience in dentistry</strong>; passionate about educating patients on the prevention and treatment of dental disease.</p>
    <p>As a dental practitioner in <strong>Family Health Dental Clinic, one of the most reliable and affordable dental clinics in Nairobi,</strong> Dr. Kiio is one of the dentists you can fully rely on to deliver excellent dental services.</p>
    <br>
    <a style="margin-bottom:10px;" href="https://familyhealthdentalclinic.com/our-team" class="btn btn-primary">Read More</a>
    <br>
  </div>
</div>
<br>

<div class="card" style="width: 18rem;">
  <img loading="lazy" style="width: 80%;" src="https://pbs.twimg.com/media/FOc8hfcX0Acuyz7?format=png&name=small" class="card-img-top" alt="Dr. Njane">
  <div class="card-body">
  <br>
    <h5 class="card-title">Dr. Njane</h5>
    <p class="card-text"><strong>A registered dental surgeon</strong> by the <a style="font-weight:bold; color: blue; text-decoration: underline;" href="https://kmpdc.go.ke/">Kenya Medical Practitioners and Dentists Council</a>. He is also <strong> a registered member of the </strong> <a style="font-weight:bold; color: blue; text-decoration: underline;" href="https://kda.or.ke/">Kenya Dental Association</a>.</p>
    <p>Dr, Njane's qualifications, constant learning and skill development make him an ideal dental practitioner to continue serving patients in <strong>one of the most reliable and trusted dental clinics in Nairobi, Family Health Dental Clinic.</strong> </p>
    <br>
    <a href="https://familyhealthdentalclinic.com/our-team" class="btn btn-primary">Read More</a>
  </div>
</div>
</div>
<br>
<div class="row">
<div class="card" style="width: 18rem;">
  <img loading="lazy" style="width: 80%;" src="https://pbs.twimg.com/media/FOc81HWX0AI6dsi?format=png&name=small" class="card-img-top" alt="Dr. Mutai">
  <div class="card-body" style="padding-right: 10px;">
  <br>
    <h5 class="card-title">Dr. Mutai</h5>
    <p class="card-text" style="padding-right: 10px;"><strong>Dental Surgeon</strong></p>
    <br>
    
  </div>
  </div>

  <br>
 
  <div class="card" style="width: 18rem;">
  <img loading="lazy" style="width: 80%;" src="https://pbs.twimg.com/media/FOc9Wx7XsA4Cbpb?format=jpg&name=small" alt="Dr. Mwathi">
  <div class="card-body" style="padding-right: 10px;">
  <br>
    <h5 class="card-title">Dr. Mwathi</h5>
    <p class="card-text" style="padding-right: 10px;"><strong>Dental Surgeon</strong></p>
    <br>
  </div>
  </div>
  </div>
  <div class="card" style="width: 18rem;">
  <img loading="lazy" style="width: 80%;" src="https://pbs.twimg.com/media/FOc9uccX0AQ_wc3?format=jpg&name=small" class="card-img-top" alt="Dr. Gachie">
  <div class="card-body">
  <br>
    <h5 class="card-title">Dr. Gachie</h5>
    <p class="card-text"><strong>Dental Surgeon</strong></p>
    <br>
    
  </div>
  </div>
  
  

         </div>
      </div>
    </div>
  </div>
 
                            <div>
                            <h2>What Our Patients Are Saying:</h2>
                            <br>
                            <p><strong>We have 70+ reviews on <a style="color:blue; text-decoration:underline;" href="https://www.google.com/search?gs_ssp=eJwFwbsNgCAQANDYmjiBDY01d8QDZAS3IHwEg1REYXvfmxd-ccR4i28o_8JkNuioRUQkqbWQigIZ6EQgNZKDXbrDen-u0T65DJaCLS0xH2qzhbmSa3Y_wz0Y3w&q=family+health+dental+clinic&oq=family&aqs=chrome.1.69i60j46i39i175i199j69i57j46i67i199i465j69i60l4.4780j0j7&sourceid=chrome&ie=UTF-8#lrd=0x182f1156882675e5:0x5506815c046c9add,1,,,">Google</a> with an average rating of 4.9/5.</strong>. This proves that <strong> we are a reliable and trustworthy dental clinic in Nairobi</strong> that has your best interest at heart.  Here is what some of our patients are saying: </p>
                            <br>
                            <p><strong>Betty Mutwiri</strong></p>
                            <p>We loved our visit today. Exceptional ambiance, consideration to have a kids play section was a plus for me. The dentists wow was very patient and professional. I would recommend this to parents and guardians.</p>
                            <br>
                            <p><strong>Ian Mbugua</strong></p>
                            <p>Very friendly staff who kept me updated on every detail. From those at the reception to the dentist. Very comfortable space and state of the art equipment. Everything done at the comfort of that 'dreaded' dentist's chair. A surprisingly good experience. Kudos!</p>
                            <br>
                            <p><strong>Zain</strong></p>
                            <p>Very awesome service from the receptionist and Dr.Gachie did a very good job..never felt better  </p>
                        </div>
                        <div style="text-align:"center"">  
                        
                        </div>
                        </div>
                    
                    </div>
                    <div class="col-md-6 mt-6 ">
                        <?php
                            $specialist_query = $this->site_model->get_active_items('Posters');
                              $main_services = '';
                              if($specialist_query->num_rows() > 0)
                              {
                                $x=0;
                                foreach($specialist_query->result() as $row)
                                {
                                  $about_title = $row->post_title;
                                  $post_id = $row->post_id;
                                  $blog_category_name = $row->blog_category_name;
                                  $blog_category_id = $row->blog_category_id;
                                  $post_title = $row->post_title;
                                  $web_name = $this->site_model->create_web_name($post_title);
                                  $post_status = $row->post_status;
                                  $post_views = $row->post_views;
                                  $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
                                  $created_by = $row->created_by;
                                  $modified_by = $row->modified_by;
                                  $post_target = $row->post_target;
                                  $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
                                  $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
                                  $description = $row->post_content;
                                  $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
                                  $created = $row->created;
                                  $day = date('j',strtotime($created));
                                  $month = date('M',strtotime($created));
                                  $year = date('Y',strtotime($created));
                                  $created_on = date('jS M Y',strtotime($row->created));
                                  $x++;
                                  if($x < 9)
                                  {
                                    $x = '0'.$x;
                                  }

                                  if($x== 1)
                                  {
                                    $expandable="true";
                                    $show = 'show';
                                  }
                                  else
                                  {
                                    $expandable = 'false';
                                     $show = '';
                                  }
                                  $main_services .= '
                                                      <div class="col-md">
                                                        <div class="icn-text icn-text-wmax">
                                                            
                                                            <div>
                                                                <img loading="lazy" src="'.$image_specialist.'" class="img-fluid" alt="">
                                                               
                                                            </div>
                                                        </div>
                                                    </div>';
                            }
                           }?>
                        <div class="row js-icn-carousel icn-carousel flex-column flex-sm-row text-center text-sm-left" data-slick='{"slidesToShow": 1, "responsive":[{"breakpoint": 1024,"settings":{"slidesToShow": 2}}]}'>
                            <?php echo $main_services?>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <!--//section welcome-->
        <!--section services-->

        <?php

            $branches_rs = $this->site_model->get_all_branches();

            $branches_list = '';
            $branch_dropdownList = '';
            if($branches_rs->num_rows() > 0)
            {
                foreach ($branches_rs->result() as $key => $value) {
                    # code...
                    $branch_name = $value->branch_name;
                    $branch_email = $value->branch_email;
                    $branch_phone = $value->branch_phone;
                    $branch_address = $value->branch_address;
                    // $branch_postal_code = $value->branch_postal_code;
                    $branch_location = $value->branch_location;
                    $branch_building = $value->branch_building;
                    $branch_floor = $value->branch_floor;
                    $location_link = $value->location_link;
                    $branch_code = $value->branch_code;
                    $branch_id = $value->branch_id;

                    

                    $branches_list .= '
                                    <li style="margin-bottom:10px !important;">
                                        <i class="fa fa-map-marker"></i> <b>'.$branch_name.'</b><br />
                                        <p> '.$branch_building.' '.$branch_floor.' '.$branch_location.'</p>
                                        <p> '.$branch_phone.'</p>
                                    </li>';

                    $branch_dropdownList .= '<option value="'.$branch_code.'#'.$branch_id.'#'.$branch_email.'">'.$branch_name.'</option>';
                }
            }
        ?>

        <div class="section bg-grey">
            <div class="container">
                <div class="title-wrap text-center">
                    <!-- <div class="h-sub theme-color">See the difference</div> -->
                    <h2 class="h1" id="book-appointment">BOOK APPOINTMENT WITH US.</h2>
                    <div class="h-decor"></div>
                </div>
                <div class="row">
                    <div class="col-md">
                        <div class="row">
                            <div class="col-lg-2">
                                &nbsp;
                            </div>
                            <div class="col-lg-8">
                                <form class="mt-15" id="bookingFormId" method="post" novalidate>
                                    <div class="successform">
                                        <p>Your message was sent successfully!</p>
                                    </div>
                                    <div class="errorform">
                                        <p>Something went wrong, try refreshing and submitting the form again.</p>
                                    </div>
                                    <div class="input-group">
                                         <span class="fa fa-user"></span>
                                        <input type="text" name="name" class="form-control" autocomplete="off" placeholder="Your Name*" />
                                    </div>
                                    <div class="row row-xs-space mt-1">
                                        <div class="col-sm-6">
                                            <div class="input-group">
                                                <span class="fa fa-email"></span>
                                                <input type="text" name="email" class="form-control" autocomplete="off" placeholder="Your Email*" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6 mt-1 mt-sm-0">
                                            <div class="input-group">
                                                 <span class="fa fa-user"></span>
                                                <input type="text" name="phone" class="form-control" autocomplete="off" placeholder="Your Phone" />
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div class="selectWrapper input-group mt-1">
                                        <span>
                                            <i class="fa fa-list"></i>
                                        </span>
                                        <select name="branch" class="form-control">
                                            <option selected="selected" disabled="disabled">Branch*</option>
                                            <?php echo $branch_dropdownList; ?>
                                        </select>
                                    </div>
                                    <div class="input-group flex-nowrap mt-1">
                                         <span class="fa fa-calendar"></span>
                                        <div class="datepicker-wrap">
                                            <input name="date" type="text" class="form-control datetimepicker" placeholder="Date" readonly>
                                        </div>
                                    </div>
                                    <div class="mt-1"></div>
                                    <textarea name="bookingmessage" class="form-control" placeholder="Your comment"></textarea>
                                    <div class="text-center mt-2">
                                        <button type="submit" class="btn btn-sm btn-hover-fill">Book now</button>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-2">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
              
            </div>
        </div>
     
        
        <?php echo $this->load->view("site/our_partners", '');?>
        
        <!--<div class="container">-->
        <!--<div class="title-wrap text-center">-->
        <!--    <h2 class="h1">Our Partners</h2>-->
        <!--    <div class="h-decor"></div>-->
        <!--</div>-->
        
      
        
        
        
       