<?php
        $specialist_query = $this->site_model->get_active_items('Our Doctors');
          $doctors_list = '';
          if($specialist_query->num_rows() > 0)
          {
            $x=0;
            foreach($specialist_query->result() as $row)
            {
              $about_title = $row->post_title;
              $post_id = $row->post_id;
              $blog_category_name = $row->blog_category_name;
              $blog_category_id = $row->blog_category_id;
              $post_title = $row->post_title;
              $web_name = $this->site_model->create_web_name($post_title);
              $post_status = $row->post_status;
              $post_views = $row->post_views;
              $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
              $created_by = $row->created_by;
              $modified_by = $row->modified_by;
              $post_target = $row->post_target;
              $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
              $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
              $description = $row->post_content;
              $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
              $created = $row->created;
              $day = date('j',strtotime($created));
              $month = date('M',strtotime($created));
              $year = date('Y',strtotime($created));
              $created_on = date('jS M Y',strtotime($row->created));
              $x++;
              if($x < 9)
              {
                $x = '0'.$x;
              }
              
              $doctors_list .= ' <div class="col-sm-6 col-md-4">
                                      <div class="doctor-box text-center">
                                        <div class="doctor-box-photo">
                                          <a href=""><img src="'.$image_specialist.'" alt="" class="img-fluid"></a>
                                        </div>
                                        <h5 class="doctor-box-name"><a href="#">'.$post_title.'</a></h5>
                                        <div class="doctor-box-position">'.$post_target.'</div>
                                       
                                        
                                      </div>
                                    </div>';
              
              ?>
            
               
             
            <?php
        }
       }

        $gallery_rs = $this->site_model->get_branch_gallery_list('Our Team');
         // var_dump($gallery_rs);die();
        $gallery = '';
        if($gallery_rs->num_rows() > 0)
        {
            foreach ($gallery_rs->result() as $key => $value) {
              # code...
              $gallery_image_name = base_url().'assets/gallery/'.$value->gallery_image_name;
              $gallery_image_thumb = $value->gallery_image_thumb;

              $gallery .= ' <div class="col-md-3"><span class="gallery-popover-link" data-full="'.$gallery_image_name.'"><img src="'.$gallery_image_name.'" alt="" class="img-fluid"></span></div>';


            }

             // var_dump($gallery_image_name);die();
        }


        $blog_category_id = $this->site_model->get_category_id('Directors');

        $category_items = $this->site_model->get_active_post_content_by_category($blog_category_id);
        
        $main_services = '';
        if($category_items->num_rows() > 0)
        {
          
         
          foreach ($category_items->result() as $key => $row) {
            # code...

            $post_title = $row->post_title;

             $post_id = $row->post_id;
            $blog_category_name = $row->blog_category_name;
            $blog_category_id = $row->blog_category_id;
            $post_title = $row->post_title;
            $web_name = $this->site_model->create_web_name($post_title);
            $post_status = $row->post_status;
            $post_views = $row->post_views;
            $image_specialist = base_url().'assets/images/posts/'.$row->post_image;
            $created_by = $row->created_by;
            $modified_by = $row->modified_by;
            $post_target = $row->post_target;
            $comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
            $categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
            $description = $row->post_content;
            $mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
            $created = $row->created;
            $day = date('j',strtotime($created));
            $month = date('M',strtotime($created));
            $year = date('Y',strtotime($created));
            $created_on = date('jS M Y',strtotime($row->created));
              
            $web_name = $this->site_model->create_web_name($post_title);

            $main_services .= '

                          <div class="container mt-6">
                            <div class="row">
                              <div class="col-md">
                                <div class="doctor-page-photo text-center">
                                 <img src="'.$image_specialist.'" class="img-fluid" alt="">
                                                          
                                </div>
                              <div class="mt-3 mt-md-5"></div>
                              </div>
                              <div class="col-lg-8 mt-4 mt-lg-0">
                                <div class="doctor-info mb-3 mb-lg-4">
                                  <div class="doctor-info-name">
                                    <h3>'.$post_title.'</h3>
                                    <h6>'.$post_target.'</h6>
                                  </div>
                                  
                                </div>

                                '.$description.'
                               </div>
                            </div>
                          </div>
                          ';

          }
         
         
        }
        else
        {
          // put that category here
        }

  



        $directors_gallery_rs = $this->site_model->get_branch_gallery_list('Directors');
         // var_dump($directors_gallery_rs);die();
        $directors_gallery = '';
        if($directors_gallery_rs->num_rows() > 0)
        {
            foreach ($directors_gallery_rs->result() as $key => $value) {
              # code...
              $gallery_image_name = base_url().'assets/gallery/'.$value->gallery_image_name;
              $gallery_image_thumb = $value->gallery_image_thumb;

              $directors_gallery .= '
              ';





            }

             // var_dump($gallery_image_name);die();
        }
        $doctors_gallery_rs = $this->site_model->get_branch_gallery_list('Doctors');
         // var_dump($directors_gallery_rs);die();
        $doctors_gallery = '';
        if($doctors_gallery_rs->num_rows() > 0)
        {
            foreach ($doctors_gallery_rs->result() as $key => $value) {
              # code...
              $gallery_image_name = base_url().'assets/gallery/'.$value->gallery_image_name;
              $gallery_image_thumb = $value->gallery_image_thumb;

              $doctors_gallery .= ' <div class="col-sm-6 col-md-4">
            <div class="doctor-box text-center">
              <div class="doctor-box-photo">
                <a href=""><img src="'.$gallery_image_name.'" alt="" class="img-fluid"></a>
              </div>
              <h5 class="doctor-box-name"><a href="doctor-page.html">Dr. Name</a></h5>
              <div class="doctor-box-position">Dental Surgeon.</div>
             
              
            </div>
          </div>';


            }




            
          

             // var_dump($gallery_image_name);die();
        }
     ?>
<?php echo $this->load->view("site/quick_links", '');?>  
<div class="page-content">
    <!--section-->
    <div class="section mt-0">
      <div class="breadcrumbs-wrap">
        <div class="container">
          <div class="breadcrumbs">
            <a href="<?php echo site_url().'home'?>">Home</a>
            <a href="#">Our Team</a>
            <span>Our Team</span>
          </div>
        </div>
      </div>
    </div>
    <!--//section-->
    <!--section-->
    <div class="section page-content-first">
      <div class="container mt-6">
         <div class="title-wrap text-center">
          <h2 class="h1">Our Team</h2>
          <div class="h-decor"></div>
        </div>
        <div class="row">
          <div class="col-md-2">
          </div>
          <?php echo $main_services?>
          
          <div class="col-md-2">
          </div>
          
        </div>
      </div>
    </div>
    <!--section-->
    <div class="section page-content-first">
          

              <?php echo $directors_gallery?>

    
    </div>


            


    <!--//section-->
     <div class="section">
      <div class="container">
        <div class="title-wrap text-center">
          <h2 class="h1">Our Doctors.</h2>
          <div class="h-decor"></div>
        </div>
        <div class="row specialist-carousel js-specialist-carousel">
         
               <?php echo $doctors_list?>

         </div>
      </div>
    </div>
  </div>
  






      
        <!--<div class="row specialist-carousel ">
            <div class="row row-sm-space pt-2">
             <div class="sm-gallery-row row no-gutters mx-lg-15">

                   
                  <?php echo $gallery?>
              </div>
          </div>
        </div>
      </div>
    </div>-->
   
    <!--//section-->
    
 