<?php


$contacts = $this->site_model->get_contacts();

  if(count($contacts) > 0)
  {
    $email = $contacts['email'];
    $phone = $contacts['phone'];
    $facebook = $contacts['facebook'];
    $twitter = $contacts['twitter'];
    $linkedin = $contacts['linkedin'];
    $instagram = $contacts['instagram'];
    $logo = $contacts['logo'];
    $company_name = $contacts['company_name'];
    $about = $contacts['about'];
    $address = $contacts['address'];
    $city = $contacts['city'];
    $post_code = $contacts['post_code'];
    $building = $contacts['building'];
    $floor = $contacts['floor'];
    $location = $contacts['location'];
    $working_weekend = $contacts['working_weekend'];
    $working_weekday = $contacts['working_weekday'];
    
    if(!empty($email))
    {
      $mail = '<div class="top-number"><p><i class="fa fa-envelope-o"></i> '.$email.'</p></div>';
    }
    
    // if(!empty($facebook))
    // {
    //  $facebook = '<li><a class="social_icon facebook" href="'.$facebook.'" target="_blank"></a></li>';
    // }
    
    // if(!empty($twitter))
    // {
    //  $twitter = '<li><a class="social_icon twitter" href="'.$twitter.'" target="_blank"></li>';
    // }
    
    // if(!empty($linkedin))
    // {
    //  $linkedin = '<li><a class="social_icon googleplus" href="'.$linkedin.'" target="_blank"></li>';
    // }
  }
  else
  {
    $email = '';
    $facebook = '';
    $twitter = '';
    $linkedin = '';
    $logo = '';
    $instagram = '';
    $company_name = '';
  }

		$result = '';
		
		//if users exist display them
	
		if ($query->num_rows() > 0)
		{	
			//get all administrators
			$administrators = $this->users_model->get_all_administrators();
			if ($administrators->num_rows() > 0)
			{
				$admins = $administrators->result();
			}
			
			else
			{
				$admins = NULL;
			}
			$count = 1;
			foreach ($query->result() as $row)
			{
				$post_id = $row->post_id;
				$blog_category_name = $row->blog_category_name;
				$blog_category_id = $row->blog_category_id;
				$post_title = $row->post_title;
				$web_name = $this->site_model->create_web_name($post_title);
				$post_status = $row->post_status;
				$post_views = $row->post_views;
				$slug = $row->slug;
				$image = base_url().'assets/images/posts/'.$row->post_image;
				$created_by = $row->created_by;
				$modified_by = $row->modified_by;
				$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
				$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
				$description = $row->post_content;
				$mini_desc = strip_tags(implode(' ', array_slice(explode(' ', $description), 0, 50)));
				$created = $row->created;
				$day = date('j',strtotime($created));
				$month = date('M Y',strtotime($created));
				$created_on = date('jS M Y H:i a',strtotime($row->created));
				
				$categories = '';
				
				//get all administrators
					// $administrators = $this->users_model->get_all_administrators();
					// if ($administrators->num_rows() > 0)
					// {
					// 	$admins = $administrators->result();
						
					// 	if($admins != NULL)
					// 	{
					// 		foreach($admins as $adm)
					// 		{
					// 			$user_id = $adm->user_id;
								
					// 			if($user_id == $created_by)
					// 			{
					// 				$created_by = $adm->first_name;
					// 			}
					// 		}
					// 	}
					// }
					
					// else
					// {
					// 	$admins = NULL;
					// }
				
					foreach($categories_query->result() as $res)
					{
						// $count++;
						$category_name = $res->blog_category_name;
						$category_id = $res->blog_category_id;
						
						if($count == $categories_query->num_rows())
						{
							$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>';
						}
						
						else
						{
							$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>, ';
						}
					}
					$comments_query = $this->blog_model->get_post_comments($post_id);
					//comments
					$comments = 'No Comments';
					$total_comments = $comments_query->num_rows();
					if($total_comments == 1)
					{
						$title = 'comment';
					}
					else
					{
						$title = 'comments';
					}
					
					if($comments_query->num_rows() > 0)
					{
						$comments = '';
						foreach ($comments_query->result() as $row)
						{
							$post_comment_user = $row->post_comment_user;
							$post_comment_description = $row->post_comment_description;
							$date = date('jS M Y H:i a',strtotime($row->comment_created));
							
							$comments .= 
							'
								<div class="user_comment">
									<h5>'.$post_comment_user.' - '.$date.'</h5>
									<p>'.$post_comment_description.'</p>
								</div>
							';
						}
					}
				// var_dump($count % 3);die();
				
				$link= "";
				if ($count % 2 == 0) {

						$link = 'bg-grey';
			     }
				$result .= '<div class="blog-post '.$link.'">

								<div class="post-image">
									<a href="'.site_url().'blog/view-single/'.$slug.'"><img src="'.$image.'" alt="" style="object-fit: cover; !important;max-height:400px !important;"></a>
								</div>
								<div class="blog-post-info">
									<div class="post-date">17<span>JAN</span></div>
									<div>
										<h2 class="post-title"><a href="'.site_url().'blog/view-single/'.$slug.'">'.$post_title.'</a></h2>
										<div class="post-meta">
											<div class="post-meta-author">by admin</div>
											
										</div>
									</div>
								</div>
								<div class="post-teaser" style="min-height:109px !important;">'.$mini_desc.' [�K]</div>
								<div class="mt-2"><a href="'.site_url().'blog/view-single/'.$slug.'" class="btn btn-sm btn-hover-fill">Read more</span> <i class="fa fa-arrow-right"></i></a></div>
							</div>

							
		           			 ';
		            $count++;



		        }
			}
			else
			{
				$result = "";
			}
           
          ?>     
 <!-- Content -->

<div class="page-content">
		<!--section-->
		<div class="section mt-0">
			<div class="breadcrumbs-wrap">
				<div class="container">
					<div class="breadcrumbs">
						<a href="<?php echo site_url().'home'?>">Home</a>
						<span>Blogs</span>
					</div>
				</div>
			</div>
		</div>
		<!--//section-->
		<!--section-->
		<div class="section page-content-first">
			
			<div class="container">
				<div class="blog-isotope">
					<?php echo $result;?>
					
				</div>
				<div class="clearfix"></div>
				
			</div>
		</div>
	</div>
	<br></br>
		<?php echo $this->load->view("site/our_partners", '');?>  
    	<?php //echo $this->load->view("site/tag_line", '');?>  
	<!--//quick links-->
