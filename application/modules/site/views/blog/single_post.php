<?php

$post_id = $row->post_id;
$blog_category_name = $row->blog_category_name;
$blog_category_id = $row->blog_category_id;
$post_title = $row->post_title;
$intro = $row->post_target;
$post_status = $row->post_status;
$post_views = $row->post_views;
$image = base_url().'assets/images/posts/'.$row->post_image;
$created_by = $row->created_by;
$modified_by = $row->modified_by;
$post_meta = $row->post_meta;
$comments = $this->users_model->count_items('post_comment', 'post_id = '.$post_id);
$categories_query = $this->blog_model->get_all_post_categories($blog_category_id);
$body = $row->post_content;
$mini_desc = implode(' ', array_slice(explode(' ', $body), 0, 50));
$created = $row->created;
$day = date('j',strtotime($created));
$month = date('M Y',strtotime($created));
$transdate = date('jS M Y H:i a',strtotime($row->created));

$categories = '';
$count = 0;
//get all administrators
	// $administrators = $this->users_model->get_all_administrators();
	// if ($administrators->num_rows() > 0)
	// {
	// 	$admins = $administrators->result();
		
	// 	if($admins != NULL)
	// 	{
	// 		foreach($admins as $adm)
	// 		{
	// 			$user_id = $adm->user_id;
				
	// 			if($user_id == $created_by)
	// 			{
	// 				$created_by = $adm->first_name;
	// 			}
	// 		}
	// 	}
	// }
	
	// else
	// {
	// 	$admins = NULL;
	// 
$gallery_rs = $this->site_model->get_post_gallery($post_id);

$gallery = '';
if($gallery_rs->num_rows() > 0)
{
	foreach ($gallery_rs->result() as $key => $value) {
	  # code...
	  $post_gallery_image_name = base_url().'assets/images/posts/'.$value->post_gallery_image_name;
	  $post_gallery_image_thumb = $value->post_gallery_image_thumb;

	  $gallery .= ' <div class="col-xl4 col-lg-4 col-md-4 col-sm-12 col-12">
	                    <div class="blog_about">
	                        <div class="blog_img blog_post_img">
	                            <figure>
	                                <img src="'.$post_gallery_image_name.'" alt="img" class="img-responsive">
	                            </figure>
	                        </div>
	                    </div>
	                </div>';
	}
}

	foreach($categories_query->result() as $res)
	{
		$count++;
		$category_name = $res->blog_category_name;
		$category_id = $res->blog_category_id;
		
		if($count == $categories_query->num_rows())
		{
			$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>';
		}
		
		else
		{
			$categories .= '<a href="'.site_url().'blog/category/'.$category_id.'" title="View all posts in '.$category_name.'" rel="category tag">'.$category_name.'</a>, ';
		}
	}
	$comments_query = $this->blog_model->get_post_comments($post_id);
	//comments
	$comments = 'No Comments';
	$total_comments = $comments_query->num_rows();
	if($total_comments == 1)
	{
		$title = 'comment';
	}
	else
	{
		$title = 'comments';
	}
	
	if($comments_query->num_rows() > 0)
	{
		$comments = '';
		foreach ($comments_query->result() as $row)
		{
			$post_comment_user = $row->post_comment_user;
			$post_comment_description = $row->post_comment_description;
			$date = date('jS M Y H:i a',strtotime($row->comment_created));
			
			$comments .= 
			'
				<div class="user_comment">
					<h5>'.$post_comment_user.' - '.$date.'</h5>
					<p>'.$post_comment_description.'</p>
				</div>
			';
		}
	}
	

$items = '';
if(!empty($post_meta))
{
	$meta_explode = explode(',',$post_meta);

	$count = count($meta_explode);

	$items = '<ul class="tags-list">';

	for ($i=0; $i < $count; $i++) { 
		// code...

		$items .='<li><a href="#">'.$meta_explode[$i].'</a></li>';
	}
	$items .= '</ul>';

}

?>
<div class="page-content">
		<!--section-->
		<div class="section mt-0">
			<div class="breadcrumbs-wrap">
				<div class="container">
					<div class="breadcrumbs">
						<a href="<?php echo site_url().'home'?>">Home</a>
						<a href="<?php echo site_url().'whats-new'?>">Blog</a>
						<span><?php $post_title?></span>
					</div>
				</div>
			</div>
		</div>
		<!--//section-->
		<!--section-->
		<div class="section page-content-first">
			<div class="container">
				<div class="row">
					<div class="col-lg-9 aside">
						<div class="blog-post blog-post-single">
							<div class="blog-post-info">
								<div class="post-date"><?php echo $day?><span><?php echo $month?></span></div>
								<div>
									<h2 class="post-title"><a href="<?php echo site_url()?>"><?php echo $post_title?></a></h2>
									<div class="post-meta">
										<div class="post-meta-author">by <i>admin</i></div>
										
									</div>
								</div>
							</div>
							<div class="post-image">
								<img src="<?php echo $image?>" alt="">
							</div>
							<div class="post-text">
								<?php echo $body?>
							</div>
							<ul class="tags-list">
								<?php echo $items;?>
							</ul>
						</div>
						
					</div>
					<div class="col-lg-3 aside-left mt-5 mt-lg-0">
						
						
						
						<div class="side-block">
							<h3 class="side-block-title">Tags</h3>
							<ul class="tags-list">
								<?php echo $items;?>
							</ul>
						</div>
					
						
					</div>
				</div>
			</div>
		</div>
		<!--//section-->
	</div>