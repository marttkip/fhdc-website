<?php session_start();
/*
	This module loads the head, header, footer &/or Social media sections.
*/
	error_reporting(0);
class Site extends CI_Controller 
{	
	var $slideshow_location;
	var $service_location;
	var $gallery_location;
	
	function __construct() 
	{
		parent:: __construct();
		
		$this->load->model('site_model');
		$this->load->model('admin/blog_model');
		$this->load->model('admin/gallery_model');
		$this->load->model('admin/users_model');
		$this->load->model('admin/email_model');
		$this->load->model('site/departments_model');
		
		$this->slideshow_location = base_url().'assets/slideshow/';
		$this->service_location = base_url().'assets/service/';
		$this->gallery_location = base_url().'assets/gallery/';
  	}
	
	public function index()
	{
		$contacts = $this->site_model->get_contacts();
		$data['contacts'] = $contacts;
		$data['gallery_location'] = $this->gallery_location;
		// $data['adverts'] = $this->site_model->get_active_adverts();
		// $data['gallery_images'] = $this->site_model->get_active_gallery();
		// $data['testimonials'] = $this->site_model->get_testimonials();
		// $data['items'] = $this->site_model->get_front_end_items();
		$data['slides'] = $this->site_model->get_slides();
		$data['services'] = $this->site_model->get_all_services();
		// $data['corporates'] = $this->site_model->get_corporates();
		// $data['resource'] = $this->site_model->get_resource(3);
		$data['latest_posts'] = $this->blog_model->get_recent_posts(4);
		// $data['trainings'] = $this->training_model->get_recent_trainings(5);
		// $data['seminars'] = $this->event_model->get_recent_events(1, 4);
		// $data['events'] = $this->event_model->get_recent_events(2, 4);
		// $data['conferences'] = $this->event_model->get_recent_events(3, 4);
		// $data['training_location'] = $this->training_location;
		// $data['resource_location'] = $this->resource_location;
		$data['slideshow_location'] = $this->slideshow_location;
		$data['faqs'] = $this->site_model->get_faqs();
		$data['title'] = $this->site_model->display_page_title();
		$data['service_location'] = $this->service_location;
		$v_data['meta_tags'] = 'Dental Clinic in Nairobi, Affordable Dental Clinic in Nairobi, Dental Clinic in Nairobi That Accepts NHIF, Dental Clinic in Nairobi That Accepts Insurance, Dentist, Dentist in Nairobi, Dentists in Nairobi, Dental Hospitals in Nairobi, Dentist in Mall, Dental Clinic in Mall, Dentist in TMall, Dental Clinic in TMall, Dental Clinic in Langata, Dentist in Langata, Dentist in Buruburu, Dental Clinic in Buruburu, Dental Appointment, Dental Consultation, Dental Services,  Dental Examination, Tooth Extraction in Kenya, Fillings in Kenya, Emergency Dental Care in Kenya, Gum Disease Treatment in Kenya, Root Canal Treatment in Kenya, Dental Implants in Kenya,Teeth Splinting in Kenya, Dentures in Kenya, Teeth Whitening in Kenya, Braces/Orthodontics in Kenya';
		$v_data['mini_desc'] = 'An affordable dental clinic in Nairobi offering Specialized Dental Care. We accept insurance and NHIF, plus flexible payment plans. Book a dental appointment today.';
		
		$v_data['title'] = 'Affordable Dental Clinic in Nairobi - Family Health Dental Clinic 0718501426';
		$v_data['content'] = $this->load->view("home", $data, TRUE);
		$v_data['meta_tags'] = 'Dental Clinic in Nairobi, Dental Clinics in Nairobi, Affordable Dental Clinic in Nairobi, Affordable Dental Clinics in Nairobi, The Best Dental Clinic in Nairobi, The Best Dental Clinics in Nairobi, Dental Clinic in Nairobi That Accepts NHIF, Dental Clinics in Nairobi That Accepts Insurance, Dentist, Dentist in Nairobi, Dentists in Nairobi, Dental Hospital in Nairobi, Dental Hospitals in Nairobi, Dentist in Mall, Dental Clinic in Mall, Dentist in TMall, Dental Clinic in TMall, Dental Clinic in Langata, Dentist in Langata, Dentist in Buruburu, Dental Clinic in Buruburu, Dental Appointment, Dental Consultation, Dental Services,  Dental Examination, Tooth Extraction in Kenya, Fillings in Kenya, Emergency Dental Care in Kenya, Gum Disease Treatment in Kenya, Root Canal Treatment in Kenya, Dental Implants in Kenya,Teeth Splinting in Kenya, Dentures in Kenya, Teeth Whitening in Kenya, Braces/Orthodontics in Kenya';
		$v_data['mini_desc'] = 'An affordable dental clinic in Nairobi offering Specialized Dental Care. We accept insurance and NHIF, plus flexible payment plans. Book a dental appointment today.';
		
		
		$this->load->view("includes/templates/general", $v_data);
	}
	
	function home_page()
	{
		$contacts = $this->site_model->get_contacts();
		$data['contacts'] = $contacts;
		$data['gallery_location'] = $this->gallery_location;
		// $data['adverts'] = $this->site_model->get_active_adverts();
		// $data['gallery_images'] = $this->site_model->get_active_gallery();
		// $data['testimonials'] = $this->site_model->get_testimonials();
		// $data['items'] = $this->site_model->get_front_end_items();
		$data['slides'] = $this->site_model->get_slides();
		$data['services'] = $this->site_model->get_all_services();
		// $data['corporates'] = $this->site_model->get_corporates();
		// $data['resource'] = $this->site_model->get_resource(3);
		$data['latest_posts'] = $this->blog_model->get_recent_posts(4);
		// $data['trainings'] = $this->training_model->get_recent_trainings(5);
		// $data['seminars'] = $this->event_model->get_recent_events(1, 4);
		// $data['events'] = $this->event_model->get_recent_events(2, 4);
		// $data['conferences'] = $this->event_model->get_recent_events(3, 4);
		// $data['training_location'] = $this->training_location;
		// $data['resource_location'] = $this->resource_location;
		$data['slideshow_location'] = $this->slideshow_location;
		$data['faqs'] = $this->site_model->get_faqs();
		$data['title'] = $this->site_model->display_page_title();
		$data['service_location'] = $this->service_location;
		
	$v_data['meta_tags'] = 'Dental Clinic in Nairobi, Affordable Dental Clinic in Nairobi, Dental Clinic in Nairobi That Accepts NHIF, Dental Clinic in Nairobi That Accepts Insurance, Dentist, Dentist in Nairobi, Dentists in Nairobi, Dental Hospitals in Nairobi, Dentist in Mall, Dental Clinic in Mall, Dentist in TMall, Dental Clinic in TMall, Dental Clinic in Langata, Dentist in Langata, Dentist in Buruburu, Dental Clinic in Buruburu, Dental Appointment, Dental Consultation, Dental Services,  Dental Examination, Tooth Extraction in Kenya, Fillings in Kenya, Emergency Dental Care in Kenya, Gum Disease Treatment in Kenya, Root Canal Treatment in Kenya, Dental Implants in Kenya,Teeth Splinting in Kenya, Dentures in Kenya, Teeth Whitening in Kenya, Braces/Orthodontics in Kenya';
	$v_data['mini_desc'] = 'An affordable dental clinic in Nairobi offering Specialized Dental Care. We accept insurance and NHIF, plus flexible payment plans. Book a dental appointment today.';
		
		$v_data['title'] = 'Affordable Dental Clinic in Nairobi- Family Health Dental Clinic';
		$v_data['content'] = $this->load->view("home", $data, TRUE);
		
		$this->load->view("includes/templates/general", $v_data);
	}
	
	public function about()
	{
		$data['title'] = 'About Family Health Dental Clinic: The Dental Clinic of Choice in Nairobi';
		$v_data['title'] = 'About Family Health Dental Clinic: The Dental Clinic of Choice in Nairobi';
		$data['company_details'] = $this->site_model->get_contacts();
		
		
		
		$v_data['content'] = $this->load->view('about_us/about_us', $data, true);
		
		$v_data['meta_tags'] = 'Tooth Extraction in Kenya, Fillings in Kenya, Emergency Dental Care in Kenya, Gum Disease Treatment in Kenya, Root Canal Treatment in Kenya, Dental Implants in Kenya,Teeth Splinting in Kenya, Dentures in Kenya, Teeth Whitening in Kenya, Braces/Orthodontics in Kenya';
		$v_data['mini_desc'] = 'We are a family-oriented group of Dental Clinics located in Nairobi, specifically on the fourth floor of TMall, Langata, and Buruburu. We focus on offering specialized dental services and procedures, including Tooth Extraction, Fillings, Emergency Dental Care, Full Mouth Scaling, Gum Disease Treatment, Root Canal Treatment, Dental Implants, Crowns and Bridges, Teeth Splinting, Dentures, Teeth Whitening, Braces/Retainers/Orthodontics';
		$this->load->view("includes/templates/general", $v_data);
	}

	public function privacy_policy()
	{
		$data['title'] = 'Privacy Policy';
		$v_data['title'] = 'Privacy Policy';
		$v_data['content'] = $this->load->view('our_team', $data, true);
			$v_data['meta_tags'] = 'Personally Identifiable Information, Non-Personally Identifiable Information, Information about You from Other Sources, How We Protect and Retain Your Information';
		$v_data['mini_desc'] = 'This Privacy Policy covers only information and data collected or processed through the Website and not any other information or data collected or processed by third parties who provide products and services on the Website (“Service Providers”) or affiliated organizations (“Affiliates”), third party web pages, or websites, products, or services to which we link that do not display a direct link to this Privacy Policy.';
		$data['company_details'] = $this->site_model->get_contacts();
		$v_data['content'] = $this->load->view('privacy', $data, true);
		
		$this->load->view("includes/templates/general", $v_data);
	}
	
	public function services($department_web_name = NULL)
	{
  		$table = "service, department";
		$where = "service.service_status = 1 AND service.department_id = department.department_id";
		
		if($department_web_name != NULL)
		{
			$department_name = $this->site_model->decode_web_name($department_web_name);
			$where .= ' AND department.department_name = \''.$department_name.'\'';
			$data['services'] = $this->site_model->get_services($table, $where, NULL);
		}
		
		else
		{
			$data['services'] = $this->site_model->get_services($table, $where, 12);
		}
		$data['service_location'] = $this->service_location;
		
		$data['title'] = 'Dental Services';
		$v_data['title'] = 'Dental Services';
		$v_data['class'] = '';
		
		$v_data['content'] = $this->load->view('our_team', $data, true);
		
		
		$v_data['content'] = $this->load->view("services", $data, TRUE);
	
		
		$this->load->view("includes/templates/general", $v_data);
	}
	
	public function view_service($web_name)
	{
		$service_name = $this->site_model->decode_web_name($web_name);
		
		
		$query = $this->site_model->get_active_content_items($service_name);

		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$post_title = $row->post_title;
			}
			$data['title'] = $post_title;
			$v_data['title'] = $post_title;
			$description = strip_tags($row->post_content);
			$v_data['query'] = $query;
			$data['content'] = $this->load->view('single_service', $v_data, true	);
			$data['meta_tags'] = $post_meta;
			$data['mini_desc'] = implode(' ', array_slice(explode(' ', $description), 0, 50));
		}
		
		else
		{
			$data['title'] = 'Services';
			$v_data['title'] = 'Services';
			$v_data['query'] = '';
			$data['content'] = 'Service not found';
		}
		
		
		$this->load->view("includes/templates/general", $data);
	}
	
	public function contact_us()
	{
	    $data['title'] = 'Contact us';
		$v_data['title'] = 'Contact us';
		$v_data['meta_tags'] = 'Get In Touch With Family Health Dental Clinic';
		$v_data['mini_desc'] = 'For general questions, please send us a message and we’ll get right back to you. You can also call us directly to speak with a member of our service team or insurance expert.';

		$v_data['sender_name_error'] = '';
		$v_data['sender_email_error'] = '';
		$v_data['sender_phone_error'] = '';
		$v_data['message_error'] = '';
		
		$v_data['sender_name'] = '';
		$v_data['sender_email'] = '';
		$v_data['sender_phone'] = '';
		$v_data['message'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');
		$this->form_validation->set_rules('sender_name', 'Your Name', 'required');
		$this->form_validation->set_rules('sender_email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('subject', 'Subject', 'xss_clean');
		$this->form_validation->set_rules('message', 'Message', 'required');
		
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$this->load->model('site/email_model');
			
			$contacts = $this->site_model->get_contacts();
			$message['contacts'] = $contacts;
			if(count($contacts) > 0)
			{
				$email = $contacts['email'];
				$facebook = $contacts['facebook'];
				$oemail = $contacts['oemail'];
				$logo = $contacts['logo'];
				$company_name = $contacts['company_name'];
				$phone = $contacts['phone'];
				$address = $contacts['address'];
				$post_code = $contacts['post_code'];
				$city = $contacts['city'];
				$building = $contacts['building'];
				$floor = $contacts['floor'];
				$location = $contacts['location'];
				
				$working_weekday = $contacts['working_weekday'];
				$working_weekend = $contacts['working_weekend'];
			}
			//Notify admin
			$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
			$message['subject'] =  $this->input->post('subject');
			$message['text'] = '<p>A help message was sent on '.$date.' saying:</p> 
					<p>'.$this->input->post('message').'</p>
					<p>Their contact details are:</p>
					<p>
						Name: '.$this->input->post('sender_name').'<br/>
						Email: '.$this->input->post('sender_email').'<br/>
					</p>';
			$message['text'] = $this->load->view('compose_mail', $message, TRUE);
			
			$sender['email'] = $this->input->post('sender_email');
			$sender['name'] = $this->input->post('sender_name');
			$receiver['email'] = $this->input->post('branch');
			//$receiver['email'] = 'kvinkipruto@gmail.com';

			//$receiver['email'] = 'snyaribo@secvate.com';
			//$receiver['email'] = 'shaddylangton@gmail.com';
			$receiver['name'] = $company_name;
		
			$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
			
			$this->session->set_userdata('success_message', 'Your message has been sent successfully. We shall get back to you as soon as possible');

		}
		else
		{
			$validation_errors = validation_errors();
			
			//repopulate form data if validation errors are present
			if(!empty($validation_errors))
			{
				//create errors
				$v_data['sender_name_error'] = form_error('sender_name');
				$v_data['sender_email_error'] = form_error('sender_email');
				$v_data['sender_phone_error'] = form_error('sender_phone');
				$v_data['message_error'] = form_error('message');
				
				//repopulate fields
				$v_data['sender_name'] = set_value('sender_name');
				$v_data['sender_email'] = set_value('sender_email');
				$v_data['sender_phone'] = set_value('sender_phone');
				$v_data['message'] = set_value('message');
			}
		}
		
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		$v_data['items'] = $this->site_model->get_front_end_items();
		
		$data['title'] = $v_data['title'] = $this->site_model->display_page_title();
		$data['contacts'] = $contacts;
		$data['content'] = $this->load->view('contact', $v_data, true);
		
		$this->load->view("includes/templates/general", $data);
	}
    
	public function gallery() 
	{
		$contacts = $this->site_model->get_contacts();
		$v_data['contacts'] = $contacts;
		
		$data['title'] = $this->site_model->display_page_title();
		$v_data['gallery_location'] = $this->gallery_location;
		$v_data['title'] = $data['title'];
		$data['contacts'] = $contacts;
		$data['content'] = $this->load->view("gallery", $v_data, TRUE);
		
		$this->load->view("includes/templates/general", $data);
	}
	
	public function departments() 
	{
		$where = 'department_status = 1';
		$segment = 2;
		$base_url = base_url().'departments';
		
		$table = 'department';
		//pagination
		$this->load->library('pagination');
		$config['base_url'] = $base_url;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<div class="wp-pagenavi">';
		$config['full_tag_close'] = '</div>';
		
		$config['next_link'] = 'Next';
		
		$config['prev_link'] = 'Prev';
		
		$config['cur_tag_open'] = '<span class="current">';
		$config['cur_tag_close'] = '</span>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->departments_model->get_all_departments($table, $where, $config["per_page"], $page);
		
		if ($query->num_rows() > 0)
		{
			$v_data['query'] = $query;
			$v_data['page'] = $page;
			$v_data['title'] = 'Departments';
			$data['content'] = $this->load->view('departments/department_list', $v_data, true);
		}
		
		else
		{
			$data['content'] = '<p>There are no departments posted yet</p>';
		}
		$data['title'] = 'Departments';
		$this->load->view("includes/templates/general", $data);
	}


	/*
	*
	*	about
	*
	*/
	public function about_content($page_item) 
	{	
		$contacts = $this->site_model->get_contacts();
		// $v_data['adverts'] = $this->site_model->get_active_adverts();
		$v_data['contacts'] = $contacts;
		$v_data['page_item'] = $page_item;
		$v_data['items'] = $this->site_model->get_front_end_items();
		$data['title'] = $this->site_model->display_page_title();
		$v_data['title'] = $data['title'];
		$data['contacts'] = $contacts;
		
		$category_name = $this->site_model->decode_web_name($page_item);
		// var_dump($category_name);die();
		$v_data['company_details'] = $this->site_model->get_contacts();
		if($category_name == "our history")
		{
			$v_data['query'] = $this->site_model->get_active_items($category_name, NULL, 'post.created', 'DESC');
			$data['content'] = $this->load->view("about_us/our_history", $v_data, TRUE);
		}
		else if($category_name == "policy and governance")
		{
			$v_data['query'] = $this->site_model->get_active_items($category_name, NULL, 'post.created', 'DESC');
			$data['content'] = $this->load->view("about_us/policy_governance", $v_data, TRUE);
		}
		else
		{
			$v_data['query'] = $this->site_model->get_active_items($category_name);
			$data['content'] = $this->load->view("about/organization_structure", $v_data, TRUE);
		}
		
	
		

		$this->load->view("site/includes/templates/general", $data);
	}


	public function view_article($web_name=null)
	{


		if(empty($web_name))
		{
			$data['title'] = 'Our Causes';
			$v_data['title'] = 'Our Causes';
			$data['content'] = $this->load->view('causes', $v_data, true);

		}
		else
		{
			$service_name = $this->site_model->decode_web_name($web_name);
		
		
			$query = $this->site_model->get_active_content_items($service_name);

			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$post_title = $row->post_title;
				}
				$data['title'] = $post_title;
				$v_data['title'] = $post_title;
				$v_data['query'] = $query;
				$data['content'] = $this->load->view('single_service', $v_data, true);
			}
			
			else
			{
				$data['title'] = 'Services';
				$v_data['title'] = 'Services';
				$v_data['query'] = '';
				$data['content'] = 'Service not found';
			}
		}
		
		
		$this->load->view("includes/templates/general", $data);
	}

	public function view_service_category($web_name)
	{

		// find if the slug is available 

		$this->db->where('slug = "'.$web_name.'" AND post.post_status = 1 AND blog_category.blog_category_id = post.blog_category_id');
		$this->db->limit(1);
		$query = $this->db->get('post,blog_category');

		if($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
				{
					$post_title = $row->post_title;
					$post_meta = $row->post_meta;
					$blog_category_id = $row->blog_category_id;
					$description = strip_tags($row->post_content);
					$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
				}
				// var_dump($query);die();
				$data['title'] = $post_title;
				$v_data['title'] = $post_title;
					$v_data['meta_tags'] = $post_meta;
				$v_data['mini_desc'] = $post_meta;
				$v_data['query'] = $query;
				$v_data['blog_category_id'] = $blog_category_id;
				$data['content'] = $this->load->view('single_service_category', $v_data, true);
		}
		else
		{




			$service_name = $this->site_model->decode_web_name($web_name);
			
			$blog_category_id = $this->site_model->get_blog_category_id($service_name);
			$query = $this->site_model->get_active_content_items($service_name);

			if ($query->num_rows() > 0)
			{
				foreach ($query->result() as $row)
				{
					$post_title = $row->post_title;
					$post_meta = $row->post_meta;
					$description = strip_tags($row->post_content);
					$mini_desc = implode(' ', array_slice(explode(' ', $description), 0, 50));
				}
				$data['title'] = $post_title;
				$v_data['title'] = $post_title;
					$v_data['meta_tags'] = $post_meta;
			$v_data['mini_desc'] = $post_meta;
				$v_data['query'] = $query;
				$v_data['blog_category_id'] = $blog_category_id;
				$data['content'] = $this->load->view('single_service_category', $v_data, true);
			}
			
			else
			{
				$data['title'] = 'Services';
				$v_data['title'] = 'Services';
				$v_data['query'] = '';
				$data['meta_tags'] = NULL;
				$data['mini_desc'] = NULL;
				$data['content'] = 'Service not found';
				$v_data['blog_category_id'] = $blog_category_id;
			}
		}
		
		$this->load->view("includes/templates/general", $data);
	}

	public function branches()
	{
		$data['title'] = 'Our Branches';
		$v_data['title'] = 'Our Branches';
		$data['meta_tags'] = 'Our Branches, TMall Branch in Langata, Buruburu Branch';
		$data['mini_desc'] = 'TMALL Branch on the 4th Floor of TMall, Langata and Buruburu Branch in The Point Mall Suite C90 along Rabai Road, Buruburu.';
		$data['company_details'] = $this->site_model->get_contacts();
		$v_data['content'] = $this->load->view('our_branches', $data, true);
		
		$this->load->view("includes/templates/general", $v_data);
	}

    public function CSR()
	{
		$data['title'] = 'CSR';
		$v_data['title'] = 'CSR';
		$v_data['meta_tags'] = 'Our CSR Activities';
		$v_data['mini_desc'] = 'Our CSR Activities';
		$data['company_details'] = $this->site_model->get_contacts();
		$v_data['content'] = $this->load->view('csr', $data, true);
		$v_data['meta_robots'] = 'noindex, nofollow';
		
		$this->load->view("includes/templates/general", $v_data);
	}

	public function our_team()
	{
		$data['title'] = 'Our Team';
		$v_data['title'] = 'Our Team';
		$data['company_details'] = $this->site_model->get_contacts();
		$v_data['content'] = $this->load->view('our_team', $data, true);
			$v_data['meta_tags'] = 'Tooth Extraction in Kenya, Fillings in Kenya, Emergency Dental Care in Kenya, Gum Disease Treatment in Kenya, Root Canal Treatment in Kenya, Dental Implants in Kenya,Teeth Splinting in Kenya, Dentures in Kenya, Teeth Whitening in Kenya, Braces/Orthodontics in Kenya';
		$v_data['mini_desc'] = 'Dr. Kiio L.M: Dedicated dental professional with over 13 years of experience in dentistry. Listens patiently, delivers quality dental care and has clear and kind communication to clients. Dr. Njane, Dr. Mutai, Dr. Mwathi, Dr. Gachie.';
		
		$this->load->view("includes/templates/general", $v_data);
	}


	public function book_appointment()
	{
		$v_data['sender_name_error'] = '';
		$v_data['sender_email_error'] = '';
		$v_data['sender_phone_error'] = '';
		$v_data['message_error'] = '';
		
		$v_data['sender_name'] = '';
		$v_data['sender_email'] = '';
		$v_data['sender_phone'] = '';
		$v_data['message'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone', 'xss_clean');
		// $this->form_validation->set_rules('time', 'Time', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('branch', 'Branch', 'required');
		// $this->form_validation->set_rules('visit_status', 'Visit Status', 'required');
		


		 
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$this->load->model('site/email_model');
			
			$branch_cc = $this->input->post('branch');

			$branch_cc = explode("#", $branch_cc);

			$branch_email = $branch_cc[2];


			// send to appointments scheduele

			/*$url = 'https://simplex-financials.com/royal-smiles-nakuru/cloud/book_online_appointment';
			//Encode the array into JSON.
			$patient_details['name'] = $this->input->post('name');
			$patient_details['email'] = $this->input->post('email');
			$patient_details['phone'] = $this->input->post('phone');
			$patient_details['time'] = $this->input->post('time');
			$patient_details['date'] = $this->input->post('date');
			$patient_details['visit_status'] = $this->input->post('visit_status');
			$patient_details['branch'] = $this->input->post('branch');
			$patient_details['remarks'] = $this->input->post('remarks');
			//The JSON data.

			$data_string = json_encode($patient_details);

			// var_dump($data_string);die();

			try{                                                                                                         

				$ch = curl_init($url);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Content-Length: ' . strlen($data_string))
				);
				$result = curl_exec($ch);

				// var_dump($result);die();

				curl_close($ch);
			}
			catch(Exception $e)
			{
				$response = "something went wrong";
				echo json_encode($response.' '.$e);
			}*/
			//Notify admin
			$company_details = $this->site_model->get_contacts();

			$company_email = $company_details['email'];
			$company_name = $company_details['company_name'];


			$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
			$message['subject'] =  'Appointment Request';
			$message['text'] = '<p>Appointment Request '.$date.':</p> 
					<p>'.$this->input->post('message').'</p>
					<p>Their contact details are:</p>
					<p>
						Name: '.$this->input->post('name').'<br/>
						Email: '.$this->input->post('email').'<br/>
						Phone: '.$this->input->post('phone').'<br/>
						Request Date: '.$this->input->post('date').'<br/>
						
					</p>';
			$message['text'] = $this->load->view('compose_mail', $message, TRUE);
			
			$sender['email'] = $this->input->post('email');
			$sender['name'] = $this->input->post('name');
			// $receiver['email'] = $email;
			$receiver['email'] = $branch_email;
			// $receiver['email'] = "marttkip@gmail.com";

			// $receiver['email'] = 'infodann2010@gmail.com';
			$receiver['name'] = $company_name;


		
			$this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
			
			$this->session->set_userdata('success_message', 'Your message has been sent successfully. We shall get back to you as soon as possible');
			$response['message'] = 'success';
			$response['result'] = 'You have successfully booked an appointment';

		}
		else
		{
			$validation_errors = validation_errors();

			// var_dump($validation_errors);die();
			
			//repopulate form data if validation errors are present
			$this->session->set_userdata('error_message', $validation_errors);
			$response['message'] = 'fail';
			$response['result'] = $validation_errors;

		}
		echo json_encode($response);
		
		// $contacts = $this->site_model->get_contacts();
		// $v_data['contacts'] = $contacts;
		// $v_data['items'] = $this->site_model->get_front_end_items();
		
		// $data['title'] = $v_data['title'] = $this->site_model->display_page_title();
		// $data['contacts'] = $contacts;
		// $data['content'] = $this->load->view('book_appointment', $v_data, true);
		
		// $this->load->view("includes/templates/general", $data);
	}

	public function quick_appointment()
	{
		$v_data['sender_name_error'] = '';
		$v_data['sender_email_error'] = '';
		$v_data['sender_phone_error'] = '';
		$v_data['message_error'] = '';
		
		$v_data['sender_name'] = '';
		$v_data['sender_email'] = '';
		$v_data['sender_phone'] = '';
		$v_data['message'] = '';
		
		//form validation rules
		$this->form_validation->set_error_delimiters('', '');

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone', 'xss_clean');
		$this->form_validation->set_rules('time', 'Time', 'required');
		$this->form_validation->set_rules('date', 'Date', 'required');
		$this->form_validation->set_rules('branch', 'Branch', 'required');
		// $this->form_validation->set_rules('visit_status', 'Visit Status', 'required');
		


		 
		//if form has been submitted
		if ($this->form_validation->run())
		{
			$this->load->model('site/email_model');
			
			$branch_cc = $this->input->post('branch');

			$branch_cc = explode("#", $branch_cc);

			$branch_email = $branch_cc[2];

			//Notify admin
			$date = date('jS M Y H:i a',strtotime(date('Y-m-d H:i:s')));
			$message['subject'] =  'Appointment Request';
			$message['text'] = '<p>Appointment Request '.$date.':</p> 
					<p>'.$this->input->post('message').'</p>
					<p>Their contact details are:</p>
					<p>
						Name: '.$this->input->post('name').'<br/>
						Email: '.$this->input->post('email').'<br/>
						Phone: '.$this->input->post('phone').'<br/>
						Branch: '.$this->input->post('branch').'<br/>
						Request Date: '.$this->input->post('date').'<br/>
						Time: '.$this->input->post('time').'<br/>
					</p>';
			$message['text'] = $this->load->view('compose_mail', $message, TRUE);
			
			$sender['email'] = $this->input->post('email');
			$sender['name'] = $this->input->post('name');
			$receiver['email'] = $email;
			//$receiver['email'] = $branch_email;
			$receiver['email'] = "marttkip@gmail.com";

			// $receiver['email'] = 'infodann2010@gmail.com';
			$receiver['name'] = $company_name;


		
			$response = $this->email_model->send_sendgrid_mail_no_attachment($receiver, $sender, $message);
			
			$this->session->set_userdata('success_message', 'Your message has been sent successfully. We shall get back to you as soon as possible');
			

		}
		else
		{
			$validation_errors = validation_errors();

			// var_dump($validation_errors);die();
			
			//repopulate form data if validation errors are present
			$this->session->set_userdata('error_message', $validation_errors);

		}
		redirect('home');
	}
}