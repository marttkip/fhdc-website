<?php

if($branches->num_rows() > 0)
{
	$row = $branches->result();
	$branch_id = $row[0]->branch_id;
	$branch_name = $row[0]->branch_name;
	$branch_image_name = $row[0]->branch_image_name;
	$branch_address = $row[0]->branch_address;
	$branch_post_code = $row[0]->branch_post_code;
	$branch_city = $row[0]->branch_city;
	$branch_phone = $row[0]->branch_phone;
	$branch_email = $row[0]->branch_email;
	$branch_location = $row[0]->branch_location;
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- For mobile content -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- IE Support -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Bootstrap -->
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/vendor/bootstrap/css/bootstrap.css" media="print"/>
        <link rel="stylesheet" href="<?php echo base_url()."assets/themes/porto-admin/1.4.1/";?>assets/stylesheets/theme-custom.css" media="print">
		<style type="text/css">
            .receipt_spacing{letter-spacing:0px; font-size: 12px;}
            .center-align{margin:0 auto; text-align:center;}
            
            .receipt_bottom_border{border-bottom: #888888 medium solid;}
            .row .col-md-12 table {
                border:solid #000 !important;
                border-width:1px 0 0 1px !important;
                font-size:10px;
            }
            .row .col-md-12 th, .row .col-md-12 td {
                border:solid #000 !important;
                border-width:0 1px 1px 0 !important;
            }
            
            .row .col-md-12 .title-item{float:left;width: 130px; font-weight:bold; text-align:right; padding-right: 20px;}
            .title-img{float:left; padding-left:30px;}
            img.logo{max-height:70px; margin:0 auto;}
            .table {margin-bottom: 0;}
        </style>
    </head>
    <body class="receipt_spacing">
    	    
        <div class="row receipt_bottom_border">
        	<div class="col-md-12">
            	<?php echo $text;?>
                <p>Thank You</p>
                <p><?php echo $branch_name;?></p>
            </div>
        </div>
        
        <div class="row">
        	<div class="col-md-12">
                <table class="table" style="width:100%">
                    <tr>
                        <th class="align-right" style="width:50%; text-align:left;">
                            <?php echo $branch_name;?><br/>
                            <?php echo $branch_address;?> <?php echo $branch_post_code;?> <?php echo $branch_city;?><br/>
                            E-mail: <?php echo $branch_email;?><br/>
                            Tel : <?php echo $branch_phone;?><br/>
                            <?php echo $branch_location;?>
                        </th>
                        <th style="width:50%">
                            <img src="<?php echo base_url().'assets/logo/'.$branch_image_name;?>" alt="<?php echo $branch_name;?>" class="img-responsive logo" style="float:right;"/>
                        </th>
                    </tr>
                </table>
            </div>
        </div>
    </body>
</html>
