<?php   if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Applications extends MX_Controller 
{
	function __construct()
	{
		parent:: __construct();
		
		$this->load->model('file_model');
		$this->load->model('admin_model');
		$this->load->model('auth/auth_model');
		$this->load->model('site/site_model');
		$this->load->model('reports_model');
		$this->load->model('hr/personnel_model');
		$this->load->model('admin/applications_model');
		$this->load->model('sections_model');
		$this->load->model('admin/branches_model');
		$this->load->model('admin/users_model');
		// $this->load->model('admin/tutorial_model');
		
		if(!$this->auth_model->check_login())
		{
			redirect('admin');
		}
	}


	public function opportunities() 
	{
		$where = 'applicant_category = 0 AND applications.application_type = post.post_id';
		$table = 'applications, post';

			
		$segment = 3;
		$order = 'applications.applicant_status';
		$order_method = 'ASC';
		$this->load->library('pagination');
		$config['base_url'] = site_url().'applications/opportunities';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->applications_model->get_all_applicants($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Opportunity Applicants';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('applications/applicants', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
	public function conservancies() 
	{
		$where = 'applicant_category = 1 ';
		$table = 'applications';

			
		$segment = 3;
		$order = 'applications.applicant_status';
		$order_method = 'ASC';
		$this->load->library('pagination');
		$config['base_url'] = site_url().'applications/conservancies';
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->applications_model->get_all_conservancies($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = 'Conservancies Applicants';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('applications/conservancies', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}

	/*
	*
	*	Activate an existing category
	*	@param int $category_id
	*
	*/
	public function activate_applicant($applicant_id)
	{
		$this->applications_model->activate_applicant($applicant_id);
		$this->session->set_userdata('success_message', 'applicant activated successfully');
		redirect('applications/opportunities');
	}
    
	/*
	*
	*	Deactivate an existing applicant
	*	@param int $applicant_id
	*
	*/
	public function deactivate_applicant($applicant_id)
	{
		$this->applications_model->deactivate_applicant($applicant_id);
		$this->session->set_userdata('success_message', 'Category disabled successfully');
		redirect('applications/opportunities');
	}

	/*
	*
	*	Activate an existing category
	*	@param int $category_id
	*
	*/
	public function activate_conservancy($applicant_id)
	{
		$this->applications_model->activate_applicant($applicant_id);
		$this->session->set_userdata('success_message', 'applicant activated successfully');
		redirect('applications/conservancies');
	}
    
	/*
	*
	*	Deactivate an existing applicant
	*	@param int $applicant_id
	*
	*/
	public function deactivate_conservancy($applicant_id)
	{
		$this->applications_model->deactivate_applicant($applicant_id);
		$this->session->set_userdata('success_message', 'Category disabled successfully');
		redirect('applications/conservancies');
	}


	public function applicants_view($item)
	{

		if($item == "schools")
		{
			$category_id = 2;
		}
		else if($item == "clubs")
		{
			$category_id = 3;
		}
		else if($item == "corporates")
		{
			$category_id = 4;
		}
		else
		{
			$category_id = 1;
		}
		$where = 'applicant_category = '.$category_id;
		$table = 'applications';

			
		$segment = 3;
		$order = 'applications.applicant_status';
		$order_method = 'ASC';
		$this->load->library('pagination');
		$config['base_url'] = site_url().'applications/'.$item;
		$config['total_rows'] = $this->users_model->count_items($table, $where);
		$config['uri_segment'] = $segment;
		$config['per_page'] = 20;
		$config['num_links'] = 5;
		
		$config['full_tag_open'] = '<ul class="pagination pull-right">';
		$config['full_tag_close'] = '</ul>';
		
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		
		$config['next_tag_open'] = '<li>';
		$config['next_link'] = 'Next';
		$config['next_tag_close'] = '</span>';
		
		$config['prev_tag_open'] = '<li>';
		$config['prev_link'] = 'Prev';
		$config['prev_tag_close'] = '</li>';
		
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$this->pagination->initialize($config);
		
		$page = ($this->uri->segment($segment)) ? $this->uri->segment($segment) : 0;
        $v_data["links"] = $this->pagination->create_links();
		$query = $this->applications_model->get_all_conservancies($table, $where, $config["per_page"], $page, $order, $order_method);
		
		//change of order method 
		if($order_method == 'DESC')
		{
			$order_method = 'ASC';
		}
		
		else
		{
			$order_method = 'DESC';
		}
		
		$data['title'] = $item.' applicants';
		$v_data['title'] = $data['title'];
		
		$v_data['order'] = $order;
		$v_data['order_method'] = $order_method;
		$v_data['query'] = $query;
		$v_data['page'] = $page;
		$data['content'] = $this->load->view('applications/others', $v_data, true);
		
		$this->load->view('admin/templates/general_page', $data);
	}
}
?>