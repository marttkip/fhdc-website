<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Sponsors & Partners</h2>
    </header>
    <div class="panel-body">
        <!-- Jasny -->
        <link href="<?php echo base_url();?>assets/jasny/jasny-bootstrap.css" rel="stylesheet">		
        <script type="text/javascript" src="<?php echo base_url();?>assets/jasny/jasny-bootstrap.js"></script> 
          <div class="padd">
				<?php
				$attributes = array('role' => 'form', 'class'=>'form-horizontal');
		
				echo form_open_multipart('admin/events/add_event_partner/'.$event_id.'/'.$page, $attributes);
				
				?>
                <div class="row">
                	<div class="col-md-8">
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="gallery_name">Name</label>
                            <div class="col-lg-8">
                            	<input type="text" class="form-control" name="event_partners_name" placeholder="Name" value="<?php echo set_value("event_partners_name");?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="gallery_name">Website URL</label>
                            <div class="col-lg-8">
                            	<input type="text" class="form-control" name="event_partners_location" placeholder="Website URL" value="<?php echo set_value("event_partners_location");?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-4 control-label">Type</label>
                            <div class="col-lg-4">
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios1" type="radio" checked value="1" name="event_partners_type">
                                        Partner
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" value="2" name="event_partners_type">
                                        Sponsor
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="optionsRadios2" type="radio" value="3" name="event_partners_type">
                                        Speaker
                                    </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label class="col-lg-4 control-label" for="gallery_name">Description</label>
                            <div class="col-lg-8">
                            	<textarea class="form-control" name="event_partners_description" placeholder="Description"><?php echo set_value("event_partners_description");?></textarea>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label" for="image">Partner Image</label>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 100%;">
                                    <img src="http://placehold.it/300?text=Partner+Image" class="img-responsive"/>
                                </div>
                                <div>
                                    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="event_partners_image"></span>
                                    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div>
                            </div>
                        </div>
                	</div>
                </div>
				
				<div class="form-group center-align">
					<input type="submit" value="Add Partner" class="login_btn btn btn-success">
				</div>
				<?php
					echo form_close();
				?>
		</div>
        
        <div class="row">
        <?php 
		if(isset($event_partners))
		{
			if($event_partners->num_rows() > 0)
			{
				foreach($event_partners->result() as $res)
				{
					$event_partners_id = $res->event_partners_id;
					$event_partners_name = $res->event_partners_name;
					$event_partners_image = $res->event_partners_image_name;
					$event_partners_link = $res->event_partners_link;
					$event_partners_type = $res->event_partners_type;
					
					if($event_partners_type == 1)
					{
						$event_partners_type = 'Partner';
					}
					
					else if($event_partners_type == 2)
					{
						$event_partners_type = 'Sponsor';
					}
					
					else if($event_partners_type == 3)
					{
						$event_partners_type = 'Speaker';
					}
					
					else
					{
						$event_partners_type = '';
					}
					?>
					<div class="col-md-3">
                        <div class="thumbnail">
                            <img class="img-responsive" src="<?php echo $this->event_location.$event_partners_image;?>" alt="<?php echo $event_partners_name;?>">
                            <div class="caption">
                                <h3><?php echo $event_partners_name;?></h3>
                                <p><?php echo $event_partners_type;?></p>
                                <p>
                                	<a href="<?php echo $event_partners_link;?>" target="_blank" class="btn btn-primary">Website</a>
                                	<a href="<?php echo site_url().'admin/events/edit_event_partners/'.$event_id.'/'.$event_partners_id.'/'.$page;?>" class="btn btn-success">Edit</a>
                                    <a href="<?php echo site_url().'admin/events/delete_event_partners/'.$event_id.'/'.$event_partners_id.'/'.$page;?>" class="btn btn-danger" onClick="return confirm('Delete <?php echo $event_partners_name;?>?'">Delete</a>
                                </p>
                            </div>
                        </div>
					</div>
					<?php
				}
			}
		}
		?>
                
        </div>
    </div>
</section>

                        